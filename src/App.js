import React, { Component, Fragment } from 'react';
import './scss/main.css'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import SideNavbar from './components/layout/sidebar/SideNavbar';
import Loader from './components/layout/Loader';
import DashBoard from './components/dashboard/DashBoard';
import PageWrapper from './components/auxiliary/PageWrapper';
import Header from './components/header/Header';
import Breadcrumb from './components/breadcrumb/Breadcrumb';
import SectionBodyWrapper from './components/auxiliary/SectionBodyWrapper';
import SignIn from './components/auth/SignIn';
import Register from './components/auth/Register'
import ForgotPassword from './components/auth/ForgotPassword';
import { ChangePassword } from './components/users/ChangePassword';
import UserProfile from './components/users/UserProfile';
import UserProfileUpdate from './components/users/UserProfileUpdate';
import ReferAFriend from './components/users/ReferAFriend';
import ReviewAdd from './components/users/ReviewAdd';
import Programs from './components/programs/Programs';
import ProgramDetails from './components/programs/ProgramDetails';
import Module from './components/programs/Module';
import TranscriptReadMore from './components/programs/TranscriptReadMore';
import ModuleDetails from './components/programs/ModuleDetails';
import ItemDetails from './components/programs/ItemDetails';
import CategoryDetails from './components/programs/CategoryDetails';
import ActionItems from './components/programs/ActionItems';
import FaqList from './components/support/FaqList';
import FaqDetails from './components/support/FaqDetails';
import FaqSearch from './components/support/FaqSearch';
import EmailSupport from './components/support/EmailSupport';
import Community from './components/support/Community';
import LinkList from './components/support/Links';

import DashboardNew from './components/dashboard/DashboardNew';
import LinkIframe from './components/support/LinkIframe';
import NotificationList from './components/notification/NotificationList';
import EventList from './components/events/EventList';
import EventAdd from './components/events/EventAdd';
import EventDetails from './components/events/EventDetails';
import EventEdit from './components/events/EventEdit';
// import EventAdd from './components/events/EventAdd';

// import Users from './components/users/Users';

export class App extends Component {

  state = {
    islogin: localStorage.getItem('memberSession')
  }

  render() {


    const { islogin } = this.state
    !islogin ? (document.body.style.paddingTop = 0) : (document.body.style.paddingTop = '78px')
    return (
      <Router>

        <Route exact path='/auth/register' component={Register} />
        <Route exact path='/auth/forgot-password' component={ForgotPassword} />
        <Route exact path='/auth/signin' component={SignIn} />


        {!islogin ? (
          <Redirect to='/auth/signin' />
        ) : (
            <Fragment>
              <SideNavbar />
              <PageWrapper classes='header-sticky'>
                <SectionBodyWrapper id='page_top'>
                  <Header />
                </SectionBodyWrapper>
                <SectionBodyWrapper>
                  <Breadcrumb />
                </SectionBodyWrapper>
              </PageWrapper>


              <Switch>
                <PageWrapper>
                  <SectionBodyWrapper classes='mt-4 content-wrapper'>
                    <Route exact path='/dashboard' component={DashBoard} />
                    <Route exact path='/dashboard/new' component={DashboardNew} />
                    <Route exact path='/review' component={ReviewAdd} />
                    <Route exact path='/refer-a-friend' component={ReferAFriend} />
                    <Route exact path='/profile' component={UserProfile} />
                    <Route exact path='/profile/update' component={UserProfileUpdate} />
                    <Route exact path='/password/change' component={ChangePassword} />
                    <Route exact path='/programs' component={Programs} />
                    {/* <Route exact path='/program/details/:program_id' component={ProgramDetails} /> */}
                    <Route exact path='/program/module/' component={Module} />
                    <Route exact path='/program/module/transcript/:module_id' component={TranscriptReadMore} />
                    <Route exact path='/program/category/module/details/:module_id' component={ModuleDetails} />
                    <Route exact path='/program/category/item/details/:item_id' component={ItemDetails} />
                    <Route exact path='/program/details/:program_id' component={ProgramDetails} />
                    <Route exact path='/program/category/details/:category_id' component={CategoryDetails} />
                    
                    <Route exact path='/program/category/actionitems/:program_id/:category_id' component={ActionItems} />

                    <Route exact path='/faqs' component={FaqList} />
                    <Route exact path='/faq/details/:faq_id' component={FaqDetails} />
                    <Route exact path='/faq/search/:query' component={FaqSearch} />
                    <Route exact path='/faq/search' component={FaqSearch} />
                    <Route exact path='/email-support' component={EmailSupport} />
                    <Route exact path='/links' component={LinkList} />
                    <Route exact path='/link/open/:link_id' component={LinkIframe} />
                    <Route exact path='/event/list/:date' component={EventList} />
                    <Route exact path='/event/add/:date' component={EventAdd} />
                    <Route exact path='/event/details/:date/:event_id' component={EventDetails} />
                    <Route exact path='/event/details/:event_id' component={EventDetails} />
                    <Route exact path='/event/edit/:date/:event_id' component={EventEdit} />
                    {/* <Route exact path='/community' component={Community} /> */}
                    
                    <Route exact path='/notifications' component={NotificationList} />
                    <Route exact path='/notifications/:notification_id' component={NotificationList} />

                    {/* <Route exact path='/profile/:member_id' component={UserProfile} /> */}
                  </SectionBodyWrapper>
                </PageWrapper>
              </Switch>
            </Fragment>
          )}




      </Router>
    )
  }
}

export default App

