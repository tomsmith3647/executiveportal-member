import axios from 'axios'
import { API_URL } from './config'


export function plus(a, b) {
  return a + b;
}


export function getUserDetails(member_id='') {

    const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
    if (memberSession.user_id) {
        axios({
            url: API_URL + "member/detail/" + member_id,
            method: 'get',
            data: {},
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
        .then(response => {

             return response.data;
        })
        .catch(err => {
            //error
            console.log(err);
        });
    }
}
