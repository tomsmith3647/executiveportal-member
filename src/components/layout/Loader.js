import React from 'react';
import $ from 'jquery';

const Loader = () => {

    setTimeout(function () {
        $('.page-loader-wrapper').fadeOut();
    }, 50);


    return (
        // Page Loader
        <div className="page-loader-wrapper">
            <div className="loader">
            </div>
        </div>
    );
}

export default Loader;
