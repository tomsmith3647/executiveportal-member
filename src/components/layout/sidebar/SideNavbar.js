
import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'


import { API_URL } from '../../../config/config'
import axios from 'axios'
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';

// Variable
const $ = window.$

export class SideNavbar extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        showAlert: false,
        showAlertMessage: '',
        memberPermission: [],
        programs: [],
        categories: [],
        modules: [],
        links:[],
    }

    subtract = (x, y) => {
        return (x - y)
    }
    componentDidMount() {


       $('[data-toggle="tooltip"]').tooltip();

        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                memberPermission: memberSession.permissions,
                role_id: memberSession.role_id
            })

            //get Member program List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/list/menu/" + memberSession.role_id,
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ image_base_url: response.data.api_url });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ programs: response.data.programs });
                        $("#menu").metisMenu({
                            toggle: true // disable the auto collapse. Default: true.
                        });
                        //this.setState({ categories: response.data.programs.categories });        
                        //this.setState({ modules: response.data.programs.categories.modules });        


                    }
                    else {
                        //fail
                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                    }

                    //Positing Side Nav Icon with Menu Items

                    //Flag
                    let found = 0
                    var iconOffSet = 0
                    let sideMenuIcons = $('.offset')
                    let NavLinkIdNew = ''

                    setTimeout(() => {
                        $("#menu").metisMenu({
                            toggle: true, // disable the auto collapse. Default: true.

                        }).on('show.metisMenu', function (event) {

                            // do something…
                            // if (found == 0) {
                            //     iconOffSet = $(event.target).parent('li').find('ul')[0].offsetHeight
                            //     found = 1
                            // } else if (found == 1) {
                            //     iconOffSet += $(event.target).parent('li').find('ul')[0].offsetHeight
                            // }

                            // let NavLinkId = $(event.target).parent('li').attr('id')



                            // for (let i = 0; i < sideMenuIcons.length; i++) {
                            //     if (sideMenuIcons[i].id) {
                            //         if (NavLinkId == sideMenuIcons[i].id) {
                            //             document.getElementById(sideMenuIcons[i].id).style.marginBottom = `${iconOffSet}px`
                            //             NavLinkIdNew = $(event.target).parent('li').attr('id')
                            //         }
                            //         else if (sideMenuIcons[i].id !== '') {
                            //             document.getElementById(sideMenuIcons[i].id).style.marginBottom = '0px'
                            //         }
                            //     }

                            // }

                            if (document.body.classList.contains('offcanvas-active')) {
                                document.body.classList.remove('offcanvas-active')
                            }

                        }).on('hide.metisMenu', function (event) {

                            // var s = JSON.stringify($(event.target.offsetHeight));
                            // var d = JSON.parse(s);

                            // if (d && iconOffSet) {

                            //     iconOffSet -= d[0]

                            //     let NavLinkId = $(event.target).parent('li').attr('id')

                            //     setTimeout(() => {

                            //         for (let i = 0; i < sideMenuIcons.length; i++) {
                            //             if (sideMenuIcons[i].id) {
                            //                 if (NavLinkId == NavLinkIdNew) {

                            //                     if (NavLinkId == sideMenuIcons[i].id) {
                            //                         document.getElementById(sideMenuIcons[i].id).style.marginBottom = `${iconOffSet}px`
                            //                     } else if (sideMenuIcons[i].id !== '') {
                            //                         document.getElementById(sideMenuIcons[i].id).style.marginBottom = '0px'
                            //                     }

                            //                 } else {

                            //                     if (NavLinkIdNew == sideMenuIcons[i].id) {
                            //                         document.getElementById(sideMenuIcons[i].id).style.marginBottom = `${iconOffSet}px`
                            //                     } else if (sideMenuIcons[i].id !== '') {
                            //                         document.getElementById(sideMenuIcons[i].id).style.marginBottom = '0px'
                            //                     }

                            //                 }
                            //             }


                            //         }
                            //         NavLinkIdNew = ''
                            //     }, 500);

                            // }


                        });
                        // $('.fa.fa-trophy').tooltip({trigger:'manual'}).tooltip('show');

                    }, 1000);
                })
                .catch(err => {
                    console.log(err);
                });


                this.setState({ showLoader: true });
                axios({
                    url: API_URL + "member/link/list",
                    method: 'get',
                    headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ links: response.data.links });                                                       
                    }
                    else{
                        //success                        
                        this.setState({ links: [] });                                                       
                    }
                    
                })
                .catch(err => {
                    console.log(err);
                });
        }

    }


    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
        setTimeout(() => {
            $("#menu").metisMenu({
                toggle: false // disable the auto collapse. Default: true.
            });
        }, 3000);

        // if ($('.isOpen').hasClass('is')) {
        //     console.log($('.isOpen').height())
        // }


    }

    handleToggle = () => {
        $('body').toggleClass('offcanvas-active');
    }

    render() {

        const getPermissionOject = (value) => {
            const permissiondata = this.state.memberPermission.filter(function (e) {
                return e.permission_id == value;
            });

            if (permissiondata) {
                return permissiondata[0];
            }
        }



        //boom - 38080107
        const boomdata = getPermissionOject('38080107');
        var boom_r = 0;
        var boom_w = 0;
        var boom_u = 0;
        if (boomdata) {
            boom_r = boomdata.r;
            boom_w = boomdata.w;
            boom_u = boomdata.u;

        }

        //Love - 22334025
        const lovedata = getPermissionOject('22334025');
        var love_r = 0;
        var love_w = 0;
        var love_u = 0;
        if (lovedata) {
            love_r = lovedata.r;
            love_w = lovedata.w;
            love_u = lovedata.u;
        }

        //Refer A Friend -35982552
        const referAFriendData = getPermissionOject('35982552');
        var referAFriend_r = 0;
        var referAFriend_w = 0;
        var referAFriend_u = 0;
        if (referAFriendData) {
            referAFriend_r = referAFriendData.r;
            referAFriend_w = referAFriendData.w;
            referAFriend_u = referAFriendData.u;
        }

        //Notification - 70459612
        const notificationData = getPermissionOject('70459612');
        var notification_r = 0;
        var notification_w = 0;
        var notification_u = 0;
        if (referAFriendData) {
            notification_r = notificationData.r;
            notification_w = notificationData.w;
            notification_u = notificationData.u;
        }

        //User Dashboard // 55640434
        // const userDashboardData = getPermissionOject('55640434');
        var userdashboard_r = 1;
        var userdashboard_w = 1;
        var userdashboard_u = 1;
        // if (referAFriendData) {
        //     userdashboard_r = userDashboardData.r;
        //     userdashboard_w = userDashboardData.w;
        //     userdashboard_u = userDashboardData.u;
        // }


        //User Advanced onboarding // 27534076 
        const advancedOnboardingData = getPermissionOject('27534076');
        var advancedOnboarding_r = 0;
        var advancedOnboarding_w = 0;
        var advancedOnboarding_u = 0;
        if (advancedOnboardingData) {
            advancedOnboarding_r = advancedOnboardingData.r;
            advancedOnboarding_w = advancedOnboardingData.w;
            advancedOnboarding_u = advancedOnboardingData.u;
        }


        //Program // 17345795
        const programData = "";
        //const programData = getPermissionOject('17345795');
        var program_r = 1;
        var program_w = 1;
        var program_u = 1;
        if (programData) {
            program_r = programData.r;
            program_w = programData.w;
            program_u = programData.u;
        }

        //Link // 80555725
        const linkData = getPermissionOject('80555725');
        var link_r = 0;
        var link_w = 0;
        var link_u = 0;
        if (referAFriendData) {
            link_r = linkData.r;
            link_w = linkData.w;
            link_u = linkData.u;
        }

        //Support // 56519713
        const supportData = getPermissionOject('56519713');
        var support_r = 0;
        var support_w = 0;
        var support_u = 0;
        if (referAFriendData) {
            support_r = supportData.r;
            support_w = supportData.w;
            support_u = supportData.u;
        }

        //FAQ // 68389135
        const faqData = getPermissionOject('68389135');
        var faq_r = 0;
        var faq_w = 0;
        var faq_u = 0;
        if (referAFriendData) {
            faq_r = faqData.r;
            faq_w = faqData.w;
            faq_u = faqData.u;
        }

        //Email Support // 32508229
        const emailSupportData = getPermissionOject('32508229');
        var emailSupport_r = 0;
        var emailSupport_w = 0;
        var emailSupport_u = 0;
        if (referAFriendData) {
            emailSupport_r = emailSupportData.r;
            emailSupport_w = emailSupportData.w;
            emailSupport_u = emailSupportData.u;
        }


        return (

            <Fragment>
                {/* Start Main top header */}
                {/* <div id="header_top" className="header_top d-none  d-sm-block">
                    <div className="container">
                        <div className="hleft">
                            <NavLink className="header-brand" to="/"><img src="/assets/images/brand-icon.png" alt="brand-icon" /></NavLink>
                            <div className="dropdown">
                                {userdashboard_r ? <NavLink to="/dashboard" className="nav-link icon app_inbox sideMenuIcons" id=''>
                                    <i className="fa fa-tachometer" data-toggle="tooltip" data-placement="right" title="Dashboard"></i>
                                </NavLink> : ''}
                                {program_r ? 
                                (
                                <div>
                                {this.state.programs.map((program, pindex) => (

                                    <a href={`/program/details/${program.program_id}`} id={`${program.program_id}`} className="nav-link icon app_file xs-hide sideMenuIcons offset">
                                        {program.program_icon ? 
                                            (<div className='program-icon'><img src={this.state.image_base_url+program.program_icon}  /></div>) : (<i className="fa fa-trophy" data-toggle="tooltip" data-placement="right" title="Program"></i>) }
                                        
                                    
                                    </a>
                                ))
                                } 
                                </div>
                                ) : ''}

                                {/* {program_r ? <NavLink to="/program" id='testing' className="nav-link icon app_file xs-hide sideMenuIcons "><i className="fa fa-trophy" data-toggle="tooltip" data-placement="right" title="Program"></i></NavLink> : ''} */}
                {/* {link_r ? <NavLink to="/links" className="nav-link icon xs-hide sideMenuIcons" id=''><i className="fa fa-link" data-toggle="tooltip" data-placement="right" title="Links"></i></NavLink> : ''}

                                {support_r ? <NavLink to="/support" className="nav-link icon xs-hide sideMenuIcons" id=''><i className="fa fa-handshake-o" data-toggle="tooltip" data-placement="right" title="Support"></i></NavLink> : ''}

                            </div>
                        </div>
                        <div className="hright">
                            <a href="javascript:void(0)" className="nav-link icon menu_toggle" onClick={this.handleToggle}><i className="fe fe-align-center"></i></a>
                        </div>
                    </div>
                </div> */}
                {/* Start Rightbar setting panel  */}




                {/* Start Main leftbar navigation */}
                <div id="left-sidebar" className="sidebar">
                    <div className="brand-name brandColor">
                        <img src="/assets/images/brand-full-logo.png" alt="brand-name" />
                    </div>
                    <nav className="sidebar-nav">
                        <ul className="metismenu" id="menu">
                            {
                                userdashboard_r ?
                                    <li className="">
                                        <NavLink to="/dashboard" className='sideMenuLinks'>
                                            <i className="fa fa-tachometer" data-toggle="tooltip" data-placement="right" title="Dashboard"></i>
                                            <span>Dashboard</span>
                                        </NavLink>
                                    </li>
                                    : ''
                            }

                            {program_r ?
                                (
                                    <Fragment>
                                        {this.state.programs.map((program, pindex) => (

                                            <li className="" id={program.program_id}>
                                                {program.program_title ? (<a href={`/program/details/${program.program_id}`} className='sideMenuLinks' aria-expanded="false">
                                                    {program.program_icon ?
                                                        (<span className='program-icon'><img src={this.state.image_base_url + program.program_icon} data-toggle="tooltip" data-placement="right" title={program.program_title} /></span>) : (<i className="fa fa-trophy" data-toggle="tooltip" data-placement="right" title={program.program_title}></i>)}
                                                    <span>{program.program_title}</span>

                                                    {program.category_exist ? <span className="fa arrow"></span> : ''}

                                                </a>) : ''}

                                                {program.category_exist ? (<ul aria-expanded="false" className='isOpen'>
                                                    {program.categories.map((category, cindex) => (
                                                        <li id={program.program_id}>
                                                            <a href={`/program/category/details/${category.category_id}`} aria-expanded="false">
                                                                {category.category_complete_status == '1' ? (<span className="sidebar-nav-item-icon fa fa-check-circle text-success"></span>) : (<span className="sidebar-nav-item-icon fa fa-check-circle"></span>)}  {category.category_title}

                                                                {category.module_exist ? <span className="fa arrow"></span> : ''}

                                                            </a>
                                                            {category.module_exist ? (<ul aria-expanded="false">
                                                                {category.modules.map((module, mindex) => (
                                                                    <li>
                                                                        <span>
                                                                            <a href={`/program/category/module/details/${module.module_id}`} aria-expanded="false">
                                                                                {module.complete_status == '1' ? (<span className="sidebar-nav-item-icon fa fa-check-circle text-success"></span>)
                                                                                    : (<span className="sidebar-nav-item-icon fa fa-check-circle"></span>)}
                                                                                <span>{module.module_title}</span>
                                                                            </a>
                                                                        </span>
                                                                    </li>
                                                                ))}
                                                            </ul>) : ''}
                                                        </li>
                                                    ))}
                                                </ul>) : ''}
                                            </li>
                                        ))
                                        } </Fragment>) : ''}

                            {/* {program_r ?

                                

                                <li className="">
                                    <NavLink to="/program" className='sideMenuLinks' activeClassName='active'>
                                        <span>Program</span>
                                        <span className="fa arrow"></span>
                                    </NavLink>
                                    <ul aria-expanded="false">
                                        <li>
                                            <NavLink to="#">
                                                <span className="sidebar-nav-item-icon fa fa-circle text-danger"></span> Category 1
                                                </NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="#">
                                                <span className="sidebar-nav-item-icon fa fa-circle text-danger"></span> Category 2
                                                </NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="#">
                                                <span className="sidebar-nav-item-icon fa fa-circle text-success"></span> Category 3
                                                </NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="#/issues">
                                                <span className="sidebar-nav-item-icon fa fa-circle text-success"></span> Category 4
                                                    <span className="fa arrow"></span>
                                            </NavLink>
                                            <ul aria-expanded="false">
                                                <li>
                                                    <NavLink to="/program/module/">
                                                        <span className="sidebar-nav-item-icon fa fa-circle text-danger"></span> Module 1
                                                        </NavLink>
                                                </li>
                                                <li>
                                                    <NavLink to="#">
                                                        <span className="sidebar-nav-item-icon fa fa-circle text-success"></span> Module 2
                                                        </NavLink>
                                                </li>
                                                <li>
                                                    <NavLink to="#/issues">
                                                        <span className="sidebar-nav-item-icon fa fa-circle text-danger"></span> Module 3
                                                        </NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                : ''
                            } */}

                            {
                                link_r ?
                                    <li className="" >
                                        <NavLink to="/link" className='sideMenuLinks' activeClassName='active'>
                                            <i className="fa fa-link" data-toggle="tooltip" data-placement="right" title="Links"></i>
                                            <span>Links</span>
                                            {this.state.links.length > 0 ? (<span className="fa arrow"></span>) : ''}
                                        </NavLink>
                                        <ul aria-expanded="false">
                                            
                                            {this.state.links.map((link, lindex) => (                                                 
                                                <li>
                                                     {link.open_type == 0 ? (                                                       
                                                            <a  href={`${link.link_url}`} target="_blank" className="" title="Details">{link.link_title}</a>
                                                        ) : (
                                                            <a  href={`${"/link/open/"+link.link_id}`} className="" title="Details">{link.link_title}</a>
                                                        )
                                                        }
                                                    {/* <a href={`${link.link_url}`} target="_blank">
                                                        <span className="sidebar-nav-item-icon"></span> {link.link_title}
                                                    </a> */}
                                                </li>
                                            ))}

                                            {this.state.links.length >= 1 && this.state.links.length <= 5 && (<li><a  href="/links" className="" title="View More">View More</a></li>)}
                                                 
                                            </ul>
                                    </li>
                                    : ''
                            }


                            {/* {
                                link_r ?
                                    <li className="" >
                                        <NavLink to="/links" className='sideMenuLinks' activeClassName='active'>
                                            <i className="fa fa-link" data-toggle="tooltip" data-placement="right" title="Links"></i>
                                            <span>Links</span>
                                            
                                        </NavLink>
                                         
                                    </li>
                                    : ''
                            } */}

                            {
                                support_r ?
                                    <li className="" >
                                        <NavLink to="/support" className='sideMenuLinks' activeClassName='active'>
                                            <i className="fa fa-handshake-o" data-toggle="tooltip" data-placement="right" title="Support"></i>
                                            <span className="fa arrow"></span>
                                            <span>Support </span>
                                        </NavLink>
                                        <ul aria-expanded="false">
                                            {faq_r ? <li>
                                                <NavLink to="/faqs" className='faq'>
                                                    <span className="sidebar-nav-item-icon"></span> Help Center
                                                </NavLink>
                                            </li> : ''

                                            }

                                            <li>
                                                {/* <NavLink to="/community" className='community'>
                                                    <span className="sidebar-nav-item-icon "></span> Community
                                                </NavLink> */}
                                                <a  target="_blank" href="http://www.facebook.com/groups/506241683458656" className='community'>
                                                    <span className="sidebar-nav-item-icon "></span> Community
                                                </a>
                                                
                                            </li>

                                            {emailSupport_r ? <li>
                                                <NavLink to="/email-support" className='email'>
                                                    <span className="sidebar-nav-item-icon "></span> Email Support
                                                </NavLink>
                                            </li> : ''}


                                        </ul>
                                    </li>
                                    : ''
                            }

                            {/* <li className="">
                                <NavLink to="/notifications" className='sideMenuLinks'>
                                    <i className="fa fa-bell" data-toggle="tooltip" data-placement="right" title="notifications"></i>
                                    <span>Notifications</span>
                                </NavLink>
                            </li> */}
                        </ul>
                    </nav>
                    <div className="hright">
                        <a href="javascript:void(0)" className="nav-link icon menu_toggle" onClick={this.handleToggle}><i className="fe fe-align-center"></i></a>
                    </div>
                </div>

                {/* Start project content area */}
            </Fragment>

        )
    }
}

export default SideNavbar
