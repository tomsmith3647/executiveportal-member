import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import { NavLink } from 'reactstrap';
// Variable
const $ = window.$

export class ForgotPassword extends Component {

    state = {
        email: '',
        password: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false
    };

    componentDidMount() 
    {
        
    }


    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleForgotPassword = (e) => {

        e.preventDefault()
        const { email } = this.state
        if (!email) {
            //error
        }
        else {
            axios({
                url: API_URL + "member/forgot/password",
                method: 'post',
                data: {
                    email: email,                   
                },
                headers: {}
            })
                .then(response => {
                    const data = response.data;
                    const error_message = data.error_message;
                    if (data.error_code == '200') {
                        this.setState({showAlertIcon : 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/auth/signin' });                                         
                    }
                    else {                        
                        this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });                        
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        }
    }

    sweetalertok = () => { 
        this.setState({ showAlert: false });
        if(this.state.showAlertActionURL)
        {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }        
    }

    render() {
        return (
            <Fragment>
                <SweetAlert
                   type={this.state.showAlertIcon}                    
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
            
                <div className="auth option2">
                    <div className="auth_left">
                        <div className="card">

                            <div className="card-body">
                                <div className="text-center">
                                    
                                    <div className="card-title">Forgot password</div>
                                </div>
                                <p className="text-muted">Enter your email address and your password will be reset and emailed to you.</p>

                                <form onSubmit={this.handleForgotPassword}>
                                    <div className="form-group">
                                    <input type="email" name="email" onChange={this.handleChange} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required />                                
                                    </div>
                                    <div className="text-center">                                        
                                        <button type="submit" className="btn btn-brand btn-block" title="" >Send me new password</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default ForgotPassword
