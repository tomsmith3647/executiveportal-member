import React, { Component } from 'react'

export class TranscriptReadMore extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
        return (
            <div className="transcript-full white-container">
                <div className="module-transcript-header">
                    <h6 className='module-transcript-title text-uppercase fw-600'>transcript</h6>
                    <a href="#" className='download ml-auto fw-700'>MP3 <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                </div>
                <div className="module-transcript-wrapper">
                    <div className="module-transcript-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus deleniti voluptatem asperiores illum rerum id tempora soluta vitae sint repudiandae possimus autem sequi necessitatibus rem eius, eveniet neque consectetur placeat exercitationem aliquam mollitia voluptatum excepturi.
                        Pariatur eligendi voluptatibus ab repellendus tempore molestias neque omnis, porro accusamus rerum ea corporis cumque eveniet explicabo temporibus itaque maxime sint nulla voluptate assumenda dolore quas exercitationem quibusdam sequi? Laudantium saepe tenetur aspernatur nisi perferendis natus itaque dicta iure explicabo dolore voluptatem nesciunt doloribus omnis doloremque, reprehenderit
                        exercitationem esse eligendi? Doloremque inventore provident eius iste quam, cum, commodi quae quisquam consequatur dolorem laudantium sit facilis. Facilis consequuntur reiciendis exercitationem dicta nobis natus delectus debitis laudantium tenetur iste aliquam, eligendi unde magni
                        rerum quidem! Aperiam necessitatibus deserunt libero. Esse quae voluptatum veritatis vitae cupiditate, magnam ab dolor optio omnis reprehenderit magni possimus voluptate, quasi fugiat voluptatem recusandae natus laboriosam, similique facilis sequi ratione alias porro repellat saepe? Cum laborum fugiat, dolorum illum voluptate consequuntur enim magnam, consequatur inventore dignissimos sapiente magni numquam reiciendis vel explicabo saepe.
                        Odit nihil corrupti consequuntur ea neque eum est. Odit nam porro sapiente illum quod corrupti assumenda amet, accusantium similique sint! Optio iure esse rem, non assumenda, recusandae distinctio expedita,
                        obcaecati quidem molestiae fugiat maxime omnis pariatur? Quas ab molestias nobis dolorem quis quasi quisquam eius voluptatibus fugit reprehenderit officiis esse voluptates, ratione dolor corporis cum eum ducimus in, ut illo corrupti. Adipisci fugiat voluptates quisquam molestiae consequuntur labore corporis eos! Nihil architecto dicta quo esse blanditiis? Reprehenderit non dicta esse dolorum facere vero incidunt odit debitis commodi dolores eum repellat ipsa a sunt minima unde quibusdam doloremque,
                        modi necessitatibus quaerat deserunt laudantium. Alias porro numquam, enim possimus quidem sint earum dignissimos! Incidunt repellendus dolor earum aperiam, unde illum cumque architecto. Repellat quos unde esse, temporibus corrupti qui pariatur exercitationem dolorem dolores dolorum, inventore accusamus vitae veniam, cum veritatis suscipit! Delectus, in iste porro quis, itaque accusamus sunt quibusdam facere iure a voluptatum. Ab consectetur earum vero provident fuga nam nesciunt neque! Eius officia libero unde tempora magnam optio perspiciatis iste,
                        nemo vitae iusto soluta eum commodi numquam aut repellat! Eos placeat voluptatibus unde dolore asperiores quas quidem illo repudiandae ex, quae expedita nesciunt fuga voluptate atque autem aut deserunt soluta qui voluptatem est sapiente, quam laboriosam culpa. Commodi pariatur rerum libero, ducimus voluptate nemo in laboriosam blanditiis praesentium quod deserunt adipisci ea illum fugiat beatae iste reiciendis.
                        Tempore natus illo pariatur ea dolorum autem ut culpa temporibus impedit cum nostrum quasi enim eius facere eos aperiam quae totam nisi, assumenda, vitae earum, officiis beatae sit! Earum architecto quis adipisci eius distinctio voluptate vel deserunt laudantium possimus accusantium minus magni officia esse, qui, quidem nisi amet porro veniam molestiae minima! Tenetur harum repellendus cupiditate! Voluptatum consequatur,
                        distinctio labore odio maiores debitis dolor unde quisquam excepturi autem incidunt velit, et cupiditate deleniti quia voluptatibus! Iste, ratione accusamus?
                        Non excepturi facere maiores animi dolore perspiciatis quibusdam illum quis recusandae cupiditate veniam, similique dolor. Porro illum non quisquam iusto vitae nostrum nisi, amet provident quidem, animi error atque corrupti suscipit vel aliquam ut reprehenderit perspiciatis doloremque voluptas in aut libero distinctio expedita quasi! Consequatur,
                        quos? Ipsam nesciunt laudantium quaerat molestiae omnis, totam quo maxime quos saepe doloremque ut consequuntur natus obcaecati odit quis eligendi voluptate autem quod ab iste eum dolores officiis fugiat inventore! Saepe necessitatibus, tenetur facere consequuntur eveniet molestias vitae fuga! Aliquam quo mollitia, voluptas ipsam dolores officia quos officiis veniam alias earum rem molestiae
                        voluptates, minima id, voluptate exercitationem illo. Omnis fugit iure nostrum temporibus debitis quo ducimus sit et consequatur at id,
                        perferendis nam necessitatibus ipsam architecto numquam voluptas! Reiciendis?
                    </div>

                </div>

            </div>
        )
    }
}

export default TranscriptReadMore
