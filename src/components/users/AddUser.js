import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
// Variable
const $ = window.$

export class AddUser extends Component {

    state = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        linkeinProfile: '',
        address: '',
        city: '',
        state: '',
        country: '',
        businessWebsite: '',
        businessDescription: '',
        showAlert: false,
        showAlertMessage: '',
        member: {},
        member_permissions: [],
        valid_permission: [],
        selected_permission: {}
    }

    componentDidMount() {
        $('.dropify').dropify();

        this.table('salesTable')
        this.table('tableReview')
        this.table('tableReferral')


        //get user detail from memeber id
        
    }

    table = (id) => {
        var table = $(`#${id}`).DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    },
                    render: function (datad) {
                        //return '<input type="checkbox" name="selectcompanyid">';
                        return '<label><input type="checkbox" name="test[]"></label>';
                    }
                }
            ],
            'select': {
                'style': 'multi',
                'selector': 'td:first-child'
            },
            'order': [[1, 'asc']]
        });
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }


    AddUser = () => {

        console.log(this.state.valid_permission);
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                 
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
             
            
               
                axios({
                    url: API_URL + "admin/member/register/",
                    method: 'post',
                    data: {
                        first_name: this.state.firstName,
                        last_name: this.state.lastName,
                        linkedin_profile: this.state.linkeinProfile,
                        business_website: this.state.businessWebsite,
                        business_description: this.state.businessDescription,
                        Address: this.state.address,
                        city: this.state.city,
                        email: this.state.email,
                         
                    },
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {

                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        if (error_code == '200') {
                            //success                        
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                            window.location.href = '/users'
                        }
                        else {
                            //fail
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
             

        }
        //Get user permissions

    }
    render() {
        return (

            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    title="Alert"
                    text={this.state.showAlertMessage}
                    onConfirm={() => this.setState({ showAlert: false })}
                />
                <div className='user-profile'>
                    <div className="row">
                        <div className="col-sm-3 mx-auto">
                            {this.props.match.params.user_id}
                            <input type="file" data-height="200" className="dropify" data-default-file="/assets/images/gallery/1.jpg" />
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <ul className="nav nav-tabs" role="tablist">
                                <li className="nav-item"><a className="nav-link active" data-toggle="tab" href="#profile" aria-expanded="true">Add User</a></li>
                                 
                            </ul>
                            <div className="tab-content">
                                <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">
                                    <form className='form-horizontal' onSubmit={this.handleSubmit}>
                                         
                                        <div className="card-body">

                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">First Name </label>
                                                <div className="col-md-7">
                                                    <input type="text" className="form-control" value={this.state.member.first_name} name='firstName' onChange={this.handleChange} required />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Last Name </label>
                                                <div className="col-md-7">
                                                    <input type="text" className="form-control" value={this.state.member.last_name} name='lastName' onChange={this.handleChange} required />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Email address </label>
                                                <div className="col-md-7">
                                                    <input type="email" className="form-control" value={this.state.member.email} name='email' onChange={this.handleChange} required />
                                                </div>
                                            </div>
                                            {/* <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Password </label>
                                                <div className="col-md-7">
                                                    <input type="password" className="form-control" name='password' onChange={this.handleChange} />
                                                </div>
                                            </div> */}
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">LinkedIn Profile </label>
                                                <div className="col-md-7">
                                                    <input type="text" className="form-control" value={this.state.member.linkedin_profile} name='linkeinProfile' onChange={this.handleChange} />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Address </label>
                                                <div className="col-md-7">
                                                    <textarea name="address" id="address" className="form-control" value={this.state.member.Address} name='address' onChange={this.handleChange}></textarea>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">City </label>
                                                <div className="col-md-7">
                                                    <select className="form-control custom-select" name='city' value={this.state.member.city} onChange={this.handleChange}>
                                                        <option value="" hidden selected>Please Choose</option>
                                                        <option value="Los Angeles">Los Angeles</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {/* <div className="form-group row">
                                                <label className="col-md-3 col-form-label">State / Province </label>
                                                <div className="col-md-7">
                                                    <select className="form-control custom-select" name='state' onChange={this.handleChange}>
                                                        <option value="" hidden selected>Please Choose</option>
                                                        <option value="state">State</option>
                                                    </select>
                                                </div>
                                            </div> */}
                                            {/* <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Country</label>
                                                <div className="col-md-7">
                                                    <select className="form-control custom-select" name='country' onChange={this.handleChange}>
                                                        <option value="" hidden selected>Please Choose</option>
                                                        <option value="Country">Country</option>
                                                    </select>
                                                </div>
                                            </div> */}
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Business Website</label>
                                                <div className="col-md-7">
                                                    <input type="text" className="form-control" value={this.state.member.business_website} name='businessWebsite' onChange={this.handleChange} />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Business Description </label>
                                                <div className="col-md-7">
                                                    <textarea name="address" id="businessDescription" className="form-control" value={this.state.member.business_description} name='businessDescription' onChange={this.handleChange}></textarea>
                                                </div>
                                            </div>



                                        </div>
                                         
                                        <div className="card-footer text-right">
                                        <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
                                        </div>

                                    </form>

                                </div>
                                                                                                                                  
                            </div>
                        </div>
                    </div>


                </div>


            </Fragment>





        )
    }
}

export default AddUser
