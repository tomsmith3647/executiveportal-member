import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import { NavLink } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';
// Variable
const $ = window.$

export class ReferAFriend extends Component {
    17
    state = {
        email: '',
        member: {},
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showLoader: false,
    }

    componentDidMount() {

        this.setState({ showAlert: false })

        //get user detail from memeber id

    }



    handleChange = (e) => {
        const contact = this.state.member;
        contact[e.target.name] = e.target.value;
        this.setState({ member: contact });
    }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }


    ReferAFriend = (e) => {
        e.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (userSession.user_id) {



            if (!this.state.member.first_name.trim()) {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid first name" });
                return false;
            }
            else if (this.state.member.first_name.trim().length < 2) {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid first name" });
                return false;
            }
            if (!this.state.member.last_name.trim()) {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid first name" });
                return false;
            }
            else if (this.state.member.last_name.trim().length < 2) {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid last name" });
                return false;
            } //ValidateEmail
            else if (!this.ValidateEmail(this.state.member.email)) {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid email address" });
                return false;
            }

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/referfriend",
                method: 'post',
                data: this.state.member,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/refer-a-friend' });

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions

    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    ValidateEmail = (mail) => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return true
        }

        return false
    }

    render() {

        var icn = "success";
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>



                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />



                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Refer A Friend</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Refer A Friend</li>
                        </ol>
                    </div>
                </div>
                <br />
                <div className='user-profile'>
                    <div className="row">
                        <div className="col">
                            <div className="tab-content mt-3">
                                <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">
                                    <form className='form-horizontal' onSubmit={this.ReferAFriend}>


                                        <div className="card">
                                            <div className="card-body">

                                                <div className="form-group row">
                                                    <label className='col-md-3 col-form-label'>First Name <span className="text-danger">*</span></label>
                                                    <div className="col-md-7">
                                                        <input className="form-control" type="text" value={this.state.member.first_name} name='first_name' onChange={this.handleChange} required />
                                                    </div>
                                                </div>



                                                <div className="form-group row">
                                                    <label className='col-md-3 col-form-label'>Last Name <span className="text-danger">*</span></label>
                                                    <div className="col-md-7">
                                                        <input className="form-control" value={this.state.member.last_name} type="text" name='last_name' onChange={this.handleChange} required />
                                                    </div>
                                                </div>


                                                <div class="form-group row">
                                                    <label className='col-md-3 col-form-label' >Email <span class="text-danger">*</span></label>
                                                    <div class="col-md-7">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="icon-envelope"></i></span>
                                                            </div>
                                                            <input type="email" className="form-control" value={this.state.member.email} name='email' onChange={this.handleChange} required />
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <div className="card-footer text-right">
                                                <button type="submit" className="btn btn-brand">Invite</button>
                                            </div>

                                        </div>





                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>


                </div>



            </Fragment>





        )
    }
}

export default ReferAFriend
