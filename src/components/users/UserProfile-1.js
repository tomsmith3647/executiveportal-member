import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
import { NavLink } from 'react-router-dom'
// Variable
const $ = window.$

export class UserProfile extends Component {

    state = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        linkeinProfile: '',
        address: '',
        city: '',
        state: '',
        country: '',
        businessWebsite: '',
        businessDescription: '',
        showAlert: false,
        showAlertMessage: '',
        member: {},
        member_permissions: [],
        valid_permission: [],
        selected_permission: {}
    }

    componentDidMount() {
        $('.dropify').dropify();

        this.table('salesTable')
        this.table('tableReview')
        this.table('tableReferral')


        //get user detail from memeber id
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            const member_id = this.props.match.params.member_id;
            if (member_id) {
                //Get user profile details           
                axios({
                    url: API_URL + "admin/member/detail/" + member_id,
                    method: 'get',
                    headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        if (error_code == '200') {
                            //success
                            this.setState({ member: response.data.member });
                        }
                        else {
                            //fail
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });


                //Get user permissions
                axios({
                    url: API_URL + "admin/member/permissions/" + member_id,
                    method: 'get',
                    headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                })
                    .then(response => {

                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        if (error_code == '200') {
                            //success
                            this.setState({ member_permissions: response.data.member_permissions });
                        }
                        else {
                            //fail
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
            }
            else {
                this.setState({ showAlert: true, showAlertMessage: "Member id empty!" });
            }


        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }
    }

    table = (id) => {
        var table = $(`#${id}`).DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    },
                    render: function (datad) {
                        //return '<input type="checkbox" name="selectcompanyid">';
                        return '<label><input type="checkbox" name="test[]"></label>';
                    }
                }
            ],
            'select': {
                'style': 'multi',
                'selector': 'td:first-child'
            },
            'order': [[1, 'asc']]
        });
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }


    handlePermission = (e) => {
        //PerMission

        const isChecked = e.target.checked ? 1 : 0;
        if (e.target.checked) {
            

            // this.setState({
            //     ...this.state,
            //     valid_permission: [...this.state.valid_permission, test]
            // })

            if(this.state.valid_permission.length > 0)
            {
                if(this.state.valid_permission)
                {
                    var rrrr = [];
                    this.state.valid_permission.map(item => {
                    
                        if(item.role == e.target.name) 
                        { 
                            item.status = 1;
                            rrrr.push(item);
                            this.setState({ valid_permission: rrrr })
                        }
                        else
                        {
                            const test = {
                                'role': e.target.name,
                                'status': isChecked
                            }
                            var joined = this.state.valid_permission.concat(test);
                            this.setState({ valid_permission: joined })
                        }                                                                    
                    })
                    
                }
            }
            else
            {
                const test = {
                    'role': e.target.name,
                    'status': isChecked
                }
                var joined = this.state.valid_permission.concat(test);
                this.setState({ valid_permission: joined })
            }
            

            

        }
        else {


            // const test = {
            //     'role': e.target.name,
            //     'status': isChecked
            // }

    
            // var joined = this.state.valid_permission.concat(test);
            // this.setState({ valid_permission: joined })


            if(this.state.valid_permission.length > 0)
            {
                if(this.state.valid_permission)
                {
                    var eee = [];
                    this.state.valid_permission.map(item => {
                    
                        if(item.role == e.target.name) 
                        { 
                            item.status = 0;
                            eee.push(item);
                            this.setState({ valid_permission: eee })
                        }
                        else
                        {
                            const test = {
                                'role': e.target.name,
                                'status': isChecked
                            }
                            var joined = this.state.valid_permission.concat(test);
                            this.setState({ valid_permission: joined })
                        }                                                                    
                    })
                    
                }
            }
            else
            {
                const test = {
                    'role': e.target.name,
                    'status': isChecked
                }
                var joined = this.state.valid_permission.concat(test);
                this.setState({ valid_permission: joined })
            }

            
            
            
            //  const changeDesc = (key) => {
            //     for (var i in this.state.valid_permission) {
            //         if (this.state.valid_permission[i].role == key) {
            //             this.state.valid_permission[i].status = 0;
            //             break; //Stop this loop, we found it!
            //         }
            //     }
            // }

             
            // const selectedPermission = {
            //     'role': e.target.name,
            //     'status': isChecked
            // }
 
            // this.setState({
            //     ...this.state,
            //     valid_permission: [...this.state.valid_permission, selectedPermission]
            // })
 
        }
        console.log(this.state.valid_permission);
    }


    updateUserPermissoin = () => {

        console.log(this.state.valid_permission);
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            const member_id = this.props.match.params.member_id;
            if (member_id) {
                const selected_permissions = this.state.valid_permission;
                axios({
                    url: API_URL + "admin/member/permission/edit/",
                    method: 'post',
                    data: {
                        member_id: member_id,
                        permissions: selected_permissions
                    },
                    headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                })
                    .then(response => {

                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        if (error_code == '200') {
                            //success                        
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                            window.location.reload();
                        }
                        else {
                            //fail
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
            }
            else {
                this.setState({ showAlert: true, showAlertMessage: "Member id empty!" });
            }

        }
        //Get user permissions

    }
    render() {
        return (

            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    title="Alert"
                    text={this.state.showAlertMessage}
                    onConfirm={() => this.setState({ showAlert: false })}
                />

                

<div className=""></div>
                    


                <div className='user-profile'>
                    <div className="row">
                        <div className="col-sm-3 mx-auto">
                            {this.props.match.params.user_id}
                            <input type="file" data-height="200" className="dropify" data-default-file="/assets/images/gallery/1.jpg" />
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <ul className="nav nav-tabs" role="tablist">
                                <li className="nav-item"><a className="nav-link active" data-toggle="tab" href="#profile" aria-expanded="true">Profile</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#permission" aria-expanded="true">Permission</a></li>
                                {/* <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#sales" aria-expanded="false">Sales</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#reviews" aria-expanded="false">Reviews</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#referrals" aria-expanded="false">Referrals</a></li> */}
                            </ul>
                            <div className="tab-content">
                                <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">
                                    <form className='form-horizontal' onSubmit={this.handleSubmit}>
                                        <p>User Profile </p>
                                        <div className="card-body">

                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">First Name </label>
                                                <div className="col-md-7">
                                                    <input type="text" className="form-control" value={this.state.member.first_name} name='firstName' onChange={this.handleChange} />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Last Name </label>
                                                <div className="col-md-7">
                                                    <input type="text" className="form-control" value={this.state.member.last_name} name='lastName' onChange={this.handleChange} />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Email address </label>
                                                <div className="col-md-7">
                                                    <input type="text" className="form-control" value={this.state.member.email} name='email' onChange={this.handleChange} />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Password </label>
                                                <div className="col-md-7">
                                                    <input type="password" className="form-control" name='password' onChange={this.handleChange} />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">LinkedIn Profile </label>
                                                <div className="col-md-7">
                                                    <input type="text" className="form-control" value={this.state.member.linkedin_profile} name='linkeinProfile' onChange={this.handleChange} />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Address </label>
                                                <div className="col-md-7">
                                                    <textarea name="address" id="address" className="form-control" value={this.state.member.Address} name='address' onChange={this.handleChange}></textarea>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">City </label>
                                                <div className="col-md-7">
                                                    <select className="form-control custom-select" name='city' value={this.state.member.city} onChange={this.handleChange}>
                                                        <option value="" hidden selected>Please Choose</option>
                                                        <option value="USA">USA</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {/* <div className="form-group row">
                                                <label className="col-md-3 col-form-label">State / Province </label>
                                                <div className="col-md-7">
                                                    <select className="form-control custom-select" name='state' onChange={this.handleChange}>
                                                        <option value="" hidden selected>Please Choose</option>
                                                        <option value="state">State</option>
                                                    </select>
                                                </div>
                                            </div> */}
                                            {/* <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Country</label>
                                                <div className="col-md-7">
                                                    <select className="form-control custom-select" name='country' onChange={this.handleChange}>
                                                        <option value="" hidden selected>Please Choose</option>
                                                        <option value="Country">Country</option>
                                                    </select>
                                                </div>
                                            </div> */}
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Business Website</label>
                                                <div className="col-md-7">
                                                    <input type="text" className="form-control" value={this.state.member.business_website} name='businessWebsite' onChange={this.handleChange} />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Business Description </label>
                                                <div className="col-md-7">
                                                    <textarea name="address" id="businessDescription" className="form-control" value={this.state.member.business_description} name='businessDescription' onChange={this.handleChange}></textarea>
                                                </div>
                                            </div>



                                        </div>

                                        <div className="card-footer text-right">
                                            <button type="submit" className="btn btn-primary">Update Profile</button>
                                        </div>

                                    </form>

                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="permission" aria-expanded="false">

                                    <div className="row">
                                        <div className="col-sm-12">
                                            <p>User Permissions </p>
                                            <div className="card" style={{ padding: '20px 20px' }}>
                                                <form>
                                                    <div className="form-group row">
                                                        <div className="table-responsive">
                                                            <table className="table table-hover table-vcenter text-nowrap    table-striped  border-style spacing5">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Role Title</th>
                                                                        <th class='text-center'>Read</th>
                                                                        <th class='text-center'>Write</th>
                                                                        <th class='text-center'>Update</th>
                                                                        {/* <th class='text-center'>Status</th>                                         */}
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {
                                                                        this.state.member_permissions.map((memberPer, index) => (
                                                                            <tr key={index}>
                                                                                <td>{index + 1}</td>
                                                                                <td>{memberPer.role_detail.role_title}</td>
                                                                                <td class='text-center'>
                                                                                    <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" class="custom-control-input" name={memberPer.role_id + '_r'} value={memberPer.r} onChange={this.handlePermission} defaultChecked={memberPer.r == 1 ? true : ''} />               <span class="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>

                                                                                <td class='text-center'>
                                                                                    <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" class="custom-control-input" name={memberPer.role_id + '_w'} value={memberPer.w} onChange={this.handlePermission} defaultChecked={memberPer.w == 1 ? true : ''} />               <span class="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>

                                                                                <td class='text-center'>
                                                                                    <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" class="custom-control-input" name={memberPer.role_id + '_u'} value={memberPer.u} onChange={this.handlePermission} defaultChecked={memberPer.u == 1 ? true : ''} />               <span class="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>




                                                                                {/* <td class='text-center'>
                                                                            { memberPer.status==1 ? ('Active') : ('Deactivate') }
                                                                            </td> */}
                                                                            </tr>
                                                                        ))
                                                                    }

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>




                                                    {/* <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="checkPermission" value="option1" onChange={this.handlePermission} />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            View / Submit BOOM
                                                     </label>
                                                    </div>

                                                    <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="example-checkbox2" value="option2" onChange={this.handlePermission} />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            View Love
                                                    </label>
                                                    </div>

                                                    <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="example-checkbox3" value="option3" />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            Refer A Friend
                                                    </label>
                                                    </div> */}

                                                </form>
                                            </div>
                                        </div>
                                        {/* <div className="col-sm-6">
                                            <p> Admin </p>
                                            <div className="card" style={{ padding: '15px 20px' }}>
                                                <form>
                                                    <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="checkPermission" value="option1" />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            View Admin
                                                     </label>
                                                    </div>

                                                    <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="example-checkbox2" value="option2" />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            Manage Programs
                                                    </label>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="example-checkbox3" value="option3" />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            Manage FAQ
                                                    </label>
                                                    </div>


                                                </form>
                                            </div>
                                        </div> */}
                                    </div>

                                    <div className="card-footer text-right">
                                        <button type="submit" onClick={this.updateUserPermissoin} className="btn btn-primary">Update</button>
                                    </div>

                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="sales" aria-expanded="false">
                                    <p> Sales </p>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id='salesTable' class="table table-hover js-basic-example dataTable table-striped table_custom border-style spacing5">
                                                    <thead>
                                                        <tr>
                                                            <th><input type="checkbox" name="select_all" value="1" id="example-select-all" /></th>
                                                            <th>Date</th>
                                                            <th>Amount</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>November 12, 2019</td>
                                                            <td>$5,000</td>
                                                            <td>
                                                                <button type="button" className="btn btn-icon btn-sm" title="Edit"><i className="fa fa-edit"></i></button>
                                                                <button type="button" className="btn btn-icon btn-sm js-sweetalert" title="Delete" data-type="confirm"><i className="fa fa-trash-o text-danger"></i></button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>November 13, 2019</td>
                                                            <td>$5,000</td>
                                                            <td>
                                                                <button type="button" className="btn btn-icon btn-sm" title="Edit"><i className="fa fa-edit"></i></button>
                                                                <button type="button" className="btn btn-icon btn-sm js-sweetalert" title="Delete" data-type="confirm"><i className="fa fa-trash-o text-danger"></i></button>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="reviews" aria-expanded="false">
                                    <p>
                                        Reviews
                                    </p>

                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id='tableReview' class="table table-hover js-basic-example dataTable table-striped table_custom border-style spacing5">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Product</th>
                                                            <th>Author</th>
                                                            <th>Review</th>
                                                            <th>Rating</th>
                                                            <th>Date</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>Apple Watch Series 3</td>
                                                            <td>Sophhia Hale</td>
                                                            <td>The Series 4 is a significant step</td>
                                                            <td>****</td>
                                                            <td>12.07.2018 10:00</td>
                                                            <td>Published</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>Apple Watch Series 4</td>
                                                            <td>Sophhia Hale</td>
                                                            <td>The Series 4 is a significant step</td>
                                                            <td>****</td>
                                                            <td>12.07.2018 10:00</td>
                                                            <td>Published</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="referrals" aria-expanded="false">
                                    <p>
                                        Referrals
                                    </p>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id='tableReferral' class="table table-hover js-basic-example dataTable table-striped table_custom border-style spacing5">
                                                    <thead>
                                                        <tr>
                                                            <th>Accepted</th>
                                                            <th>Date</th>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Earned</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>11/09/2019</td>
                                                            <td>BOB Smith</td>
                                                            <td>smith.bob@gmail.com</td>
                                                            <td>$5,000</td>
                                                            <td>
                                                                <button type="button" className="btn btn-icon btn-sm" title="Edit"><i className="fa fa-edit"></i></button>
                                                                <button type="button" className="btn btn-icon btn-sm js-sweetalert" title="Delete" data-type="confirm"><i className="fa fa-trash-o text-danger"></i></button>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="card-footer text-right">
                                        <button type="submit" className="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>


            </Fragment>





        )
    }
}

export default UserProfile
