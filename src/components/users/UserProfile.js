import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import { NavLink } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';

// Variable
const $ = window.$

export class UserProfile extends Component {


    constructor(props) {
        super(props);

        this.state = { member: [] };

        this.state = {
            member: [{ first_name: "", last_name: "", profile_image: '' }]
        };


        this.state = {
            selectedFile: null
        }


        this.state = {

            baseApiURL: '',
            showLoader: false,
            firstName: '',
            lastName: '',
            email: '',

            password: '',
            linkeinProfile: '',
            address: '',
            city: '',
            state: '',
            country: '',
            businessWebsite: '',
            businessDescription: '',
            showAlert: false,
            showAlertMessage: '',
            profile_image: '',
            member: {},
            member: { status: 0 },
            member_permissions: [],
            valid_permission: [],
            selected_permission: {},
            countries: [],
            states: [],

        }



    }




    componentDidMount() {


        //get user detail from memeber id
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            const member_id = memberSession.user_id;
            if (member_id) {
                //Get user profile details    
                this.setState({ showLoader: true });

                axios({
                    url: API_URL + "member/profile/" + member_id,
                    method: 'get',
                    headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        const ApiURL = response.data.api_url;
                        this.setState({ baseApiURL: ApiURL });
                        this.setState({ showLoader: false });
                        if (error_code == '200') {
                            //success
                            delete response.data.member.modified_date;
                            delete response.data.member.created_date;
                            this.setState({ member: response.data.member });
                            $('.dropify').dropify();
                            const default_country_code = this.state.member.country_code;
                            if (default_country_code) {
                                this.setState({ showLoader: true });
                                //get state List
                                axios({
                                    url: API_URL + "state/list/" + default_country_code,
                                    method: 'get',
                                })
                                    .then(response => {
                                        const error_code = response.data.error_code;
                                        const error_message = response.data.error_message;

                                        this.setState({ showLoader: false });
                                        if (error_code == '200') {
                                            //success                        
                                            this.setState({ states: response.data.states });
                                        }
                                        else {
                                            //fail
                                            //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                        }
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });
                            }
                        }
                        else {
                            //fail
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
            }
            else {
                this.setState({ showAlert: true, showAlertMessage: "Member id empty!" });
            }
        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }


    }




    render() {
        var buttonfile = "";
        // if(this.state.member.profile_image) {
        //     buttonfile = <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-min-width="400" data-default-file={this.state.baseApiURL+this.state.member.profile_image} />
        //   } else {
        //     buttonfile = <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file="/assets/images/gallery/1.jpg" />;
        //   }

        const isProfileImage = this.state.member.profile_image;
        var isProfileImageURL = this.state.baseApiURL + this.state.member.profile_image;

        return (

            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>


                {/* <LoadingOverlay
                    
                    active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay> */}


                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Profile</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Profile</li>
                        </ol>

                    </div>
                    <div className='pull-right'>
                        <a href="/profile/update" className="btn btn-brand">Edit</a>
                    </div>
                </div>
               
                <br />
                <div className='user-profile white-container'>


                    <div className="row">
                        <div className="col-md-3 mt-3">
                            {/* <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-min-width="400" data-default-file={this.state.member.profile_image ? 'http://dev5.logicnext.com:7550/storage/9Hb2TS9Yx05CcpJmADkeHPujiyZUfJVm3sJHy.jpg' : '/assets/images/gallery/1.jpg'} /> */}
                            {/* <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file="http://dev5.logicnext.com:7550/storage/9Hb2TS9Yx05CcpJmADkeHPujiyZUfJVm3sJHy.jpg" /> */}
                            {/* <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-min-width="400" data-default-file={this.state.member.profile_image ? 'http://dev5.logicnext.com:7550/storage/9Hb2TS9Yx05CcpJmADkeHPujiyZUfJVm3sJHy.jpg':'/assets/images/gallery/1.jpg'} /> */}
                            <img class="img-responsive" src={this.state.member.profile_image ? this.state.baseApiURL + this.state.member.profile_image : '/assets/images/gallery/1.jpg'} />

                            {/* {this.state.baseApiURL + this.state.member.profile_image} */}
                        </div>
                        <div className="col-md-7">


                            <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">

                                <form className='form-horizontal' onSubmit={this.updateUserProfileUpdate}>
                                    <div className="card-body">

                                        <div className="form-group required row">

                                            <label className="col-md-3 col-form-label">First Name </label>
                                            <div className="col-md-9">
                                                <span className="form-control">{this.state.member.first_name}  &nbsp;</span>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Last Name </label>
                                            <div className="col-md-9">
                                                <span className="form-control">{this.state.member.last_name}  &nbsp;</span>
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">Email Address </label>
                                            <div className="col-md-9">
                                                <span className="form-control">{this.state.member.email}</span>
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">LinkedIn Profile </label>
                                            <div className="col-md-9">
                                                <span className="form-control">
                                                    {this.state.member.linkedin_profile ? <a href={this.state.member.linkedin_profile} title='Linkedin Profile' target="_blank">View Linkedin Profile</a> : ''}
                                                    &nbsp;
                    </span>
                                            </div>

                                        </div>

                                        <br />
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Address </label>
                                            <div className="col-md-9">
                                                <textarea name="address" id="address" className="form-control" readOnly value={this.state.member.address}></textarea>
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">City </label>
                                            <div className="col-md-9">
                                                <span className="form-control">{this.state.member.city}  &nbsp;</span>
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">Country </label>
                                            <div className="col-md-9">
                                                <span className="form-control">{this.state.member.country_name} &nbsp;</span>
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">State / Province </label>
                                            <div className="col-md-9">
                                                <span className="form-control">{this.state.member.state_name} &nbsp;</span>
                                            </div>

                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">Zip Code</label>
                                            <div className="col-md-9">
                                                <span className="form-control">{this.state.member.zip_code}  &nbsp;</span>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Business Website</label>
                                            <div className="col-md-9">
                                                <span className="form-control">{this.state.member.business_website}  &nbsp;</span>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Business Description </label>
                                            <div className="col-md-9">

                                                <textarea name="business_description" id="business_description" className="form-control" readOnly value={this.state.member.business_description}></textarea>
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Status</label>
                                            <div className="col-md-9">

                                                <span className="form-control">
                                                    {this.state.member.status == 1 ? ('Active') : ''}
                                                    {this.state.member.status == 0 ? ('Inactive') : ''}
                                                    &nbsp;
                </span>


                                            </div>
                                        </div>

                                    </div>

                                </form>


                            </div>


                        </div>
                    </div>


                </div>


            </Fragment>





        )
    }
}

export default UserProfile
