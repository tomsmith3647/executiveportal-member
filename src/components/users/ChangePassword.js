import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import { NavLink } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';
// Variable
const $ = window.$

export class ChangePassword extends Component {

    state = {
        password: '',
        confirmpassword: '',
        email: '',
        showAlert: false,
        showAlertMessage: '',
        member: {},
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        old_password: '',


    }

    componentDidMount() {

        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
        }

    }



    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }


    updatePassword = (e) => {
        e.preventDefault();
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })

            if (!this.state.email) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Email is Required' });
            }
            else if (!this.state.password) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please enter password' });
            }
            else if (this.state.password.length < 8) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Password should be minimum 8 characters' });
            }
            else if (!this.state.confirmpassword) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please enter confirm password' });
            }
            else if (this.state.password != this.state.confirmpassword) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Passwords do not match. Please try again' });
            }
            else {
                this.setState({ showLoader: true });
                axios({
                    url: API_URL + "member/password/change",
                    method: 'post',
                    data: {
                        member_id: this.state.user_id,
                        password: this.state.password,
                        old_password: this.state.old_password,
                        email: this.state.email,

                    },
                    headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                })
                    .then(response => {

                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        this.setState({ showLoader: false });
                        if (error_code == '200') {
                            //success                        
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/dashboard' });
                            //window.location.href = '/dashboard'
                        }
                        else {
                            //fail
                            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
            }
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    render() {
        return (

            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Change Password</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Change Password</li>
                        </ol>
                    </div>
                </div>
                <br />
                <div className='user-profile'>


                    <div className="row">
                        <div className="col">

                            <div className="tab-content">
                                <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">
                                    <form className='form-horizontal' onSubmit={this.updatePassword}>
                                        <div className="card">

                                            <div className="card-body">

                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">Email</label>
                                                    <div className="col-md-7">
                                                        <input type="text" className="form-control" readonly="readonly" value={this.state.email} name='email' onChange={this.handleChange} required />
                                                    </div>
                                                </div>
                                                <div className="form-group required row">
                                                    <label className="col-md-3 col-form-label">Old Password </label>
                                                    <div className="col-md-7">
                                                        <input type="password" className="form-control" value={this.state.old_password} name='old_password' onChange={this.handleChange} required />
                                                    </div>
                                                </div>
                                                <div className="form-group required row">
                                                    <label className="col-md-3 col-form-label">New Password </label>
                                                    <div className="col-md-7">
                                                        <input type="password" className="form-control" value={this.state.password} name='password' onChange={this.handleChange} required />
                                                    </div>
                                                </div>
                                                <div className="form-group required row">
                                                    <label className="col-md-3 col-form-label">Confirm New Password </label>
                                                    <div className="col-md-7">
                                                        <input type="password" className="form-control" value={this.state.confirmpassword} name='confirmpassword' onChange={this.handleChange} required />
                                                    </div>
                                                </div>




                                            </div>


                                            <div className="card-footer text-right">
                                                <button type="submit" className="btn btn-brand">Update Password</button>
                                            </div>

                                        </div>


                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>


                </div>


            </Fragment>





        )
    }
}

export default ChangePassword
