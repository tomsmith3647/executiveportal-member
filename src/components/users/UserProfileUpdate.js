import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import { NavLink } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';
// Variable
const $ = window.$

export class UserProfileUpdate extends Component {


    constructor(props) {
        super(props);

        this.state = { member: [] };

        this.state = {
            member: [{ first_name: "", last_name: "", profile_image: '' }]
        };


        this.state = {
            selectedFile: null
        }


        this.state = {

            baseApiURL: '',
            showLoader: false,
            firstName: '',
            lastName: '',
            email: '',

            password: '',
            linkeinProfile: '',
            address: '',
            city: '',
            state: '',
            country: '',
            businessWebsite: '',
            businessDescription: '',
            showAlert: false,
            showAlertMessage: '',
            profile_image: '',
            member: {},
            member: { status: 0 },
            member_permissions: [],
            valid_permission: [],
            selected_permission: {},
            countries: [],
            states: [],
            showUpdateBtn: false,

        }



    }




    componentDidMount() {


        //get user detail from memeber id
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            const member_id = memberSession.user_id;
            if (member_id) {
                //Get user profile details    
                this.setState({ showLoader: true });

                axios({
                    url: API_URL + "member/detail/" + member_id,
                    method: 'get',
                    headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        const ApiURL = response.data.api_url;
                        this.setState({ baseApiURL: ApiURL });
                        this.setState({ showLoader: false });
                        if (error_code == '200') {
                            //success
                            delete response.data.member.modified_date;
                            delete response.data.member.created_date;
                            this.setState({ member: response.data.member });
                            $('.dropify').dropify();
                            const default_country_code = this.state.member.country_code;
                            if (default_country_code) {
                                this.setState({ showLoader: true });
                                //get state List
                                axios({
                                    url: API_URL + "state/list/" + default_country_code,
                                    method: 'get',
                                })
                                    .then(response => {
                                        const error_code = response.data.error_code;
                                        const error_message = response.data.error_message;

                                        this.setState({ showLoader: false });
                                        if (error_code == '200') {
                                            //success                        
                                            this.setState({ states: response.data.states });
                                        }
                                        else {
                                            //fail
                                            //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                        }
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });
                            }
                        }
                        else {
                            //fail
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });



            }
            else {
                this.setState({ showAlert: true, showAlertMessage: "Member id empty!" });
            }



            //get country List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "country/list",
                method: 'get',
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ countries: response.data.countries });
                    }
                    else {
                        //fail
                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }


    }



    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })

    }





    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }



    handleCountryChange = (e) => {

        const country_code = e.target.value
        if (!country_code) {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select country" });
            return false;
        }


        this.setState(
            { member: { ...this.state.member, country_code: country_code } }
        )

        //get state List
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "state/list/" + country_code,
            method: 'get',
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;

                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ states: response.data.states });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });


        console.log(this.state.countries);
    }



    handleImage = (event) => {
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        console.log(event.target.files[0])
        if (!event.target.files[0]) {
            this.setState({ showAlert: true, showAlertMessage: "Please choose profile image" });
        }
        else {
            this.setState({
                selected_profile_image: event.target.files[0],
            })

        }
    }



    handleChangeProfile = (e) => {

        const contact = this.state.member;
        contact[e.target.name] = e.target.value;
        this.setState({ member: contact, showUpdateBtn: true });
    }

    checkValidZipCode = (evt) => {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            evt.preventDefault();
            return false;
        }
        else {
            if (iKeyCode == 46) {
                evt.preventDefault();
                return false;
            }
            else {
                if (evt.target.value.length > 7) {
                    evt.preventDefault();
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    }


    ValidateEmail = (mail) => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return true
        }
        return false
    }

    //update profile
    updateUserProfileUpdate = (e) => {
        e.preventDefault()
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })

            const words = ['linkedin.com'];
            const regex = new RegExp(words.join('|'));


            if (!this.ValidateEmail(this.state.member.email)) {
                e.preventDefault()
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid email address." });
                return false;
            }

            if(!this.state.member.linkedin_profile && !this.state.member.business_website)            
            {
                e.preventDefault()
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please provide your LinkedIn Profile or Business website." });
                return false;
            }

            if(this.state.member.linkedin_profile)
            {
                if (!this.validateUrl(this.state.member.linkedin_profile)) {
                    e.preventDefault()
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid LinkedIn Profile URL." });
                    return false;
                }
                else if (!regex.test(this.state.member.linkedin_profile)) {
                    e.preventDefault()
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid LinkedIn Profile URL." });
                    return false;
                }
            }
            
            if(this.state.member.business_website)
            {
                if (!this.validateUrl(this.state.member.business_website)) {
                    e.preventDefault()
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid Business Website URL." });
                    return false;
                }
            }
            

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/profile/update",
                method: 'post',
                data: this.state.member,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {

                        if (this.state.selected_profile_image) {
                            this.setState({ showLoader: true });
                            const imagedata = new FormData()
                            imagedata.append('profileimage', this.state.selected_profile_image)
                            axios({
                                url: API_URL + "member/profile/image/upload",
                                method: 'post',
                                data: imagedata,
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                                .then(response => {

                                    const error_code = response.data.error_code;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/profile' });
                                        //window.location.href = '/dashboard'
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        }
                        else {
                            //success                                                
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/profile' });
                        }
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }
    //resetPassword
    resetPassword = () => {
        console.log(this.state.member);
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            const member_id = memberSession.user_id;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/password/reset",
                method: 'post',
                data: this.state.member,
                data: {
                    member_id: member_id,
                },
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    this.setState({ showLoader: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success                        
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message });
                        window.location.href = '/dashboard'
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions
    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    validateUrl = (url) => {
        var urlregex = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
        const result = urlregex.test(url);
        if (result) {
            return true;
        }
        else {
            return false;
        }
    }

    checkUrl = (e) => {
        const url = e.target.value;
        if (this.validateUrl(url)) {
            return true;
        }
        else {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid URL." });
            return false;
        }
    }

    render() {
        var buttonfile = "";
        // if(this.state.member.profile_image) {
        //     buttonfile = <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-min-width="400" data-default-file={this.state.baseApiURL+this.state.member.profile_image} />
        //   } else {
        //     buttonfile = <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file="/assets/images/gallery/1.jpg" />;
        //   }

        const isProfileImage = this.state.member.profile_image;
        var isProfileImageURL = this.state.baseApiURL + this.state.member.profile_image;

        return (

            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Profile</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                            <li className="breadcrumb-item"><a href="/profile">Profile</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Edit</li>
                        </ol>
                    </div>
                </div>
                <br />
                <div className='user-profile white-container'>

                    <div className="row">
                        <div className="col-md-3">
                            {/* <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-min-width="400" data-default-file={this.state.member.profile_image ? 'http://dev5.logicnext.com:7550/storage/9Hb2TS9Yx05CcpJmADkeHPujiyZUfJVm3sJHy.jpg' : '/assets/images/gallery/1.jpg'} /> */}
                            {/* <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file="http://dev5.logicnext.com:7550/storage/9Hb2TS9Yx05CcpJmADkeHPujiyZUfJVm3sJHy.jpg" /> */}
                            {/* <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-min-width="400" data-default-file={this.state.member.profile_image ? 'http://dev5.logicnext.com:7550/storage/9Hb2TS9Yx05CcpJmADkeHPujiyZUfJVm3sJHy.jpg':'/assets/images/gallery/1.jpg'} /> */}
                            <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file={this.state.member.profile_image ? this.state.baseApiURL + this.state.member.profile_image : '/assets/images/gallery/1.jpg'} />
                            {/* {this.state.baseApiURL + this.state.member.profile_image} */}

                        </div>

                        <div className="col-md-7">


                            <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">
                                <form className='form-horizontal' onSubmit={this.updateUserProfileUpdate}>

                                    <div className="card-body">

                                        <div className="form-group required row">

                                            <label className="col-md-3 col-form-label">First Name </label>
                                            <div className="col-md-9">
                                                <input type="text" className="form-control" value={this.state.member.first_name} name='first_name' onChange={this.handleChangeProfile} required />
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Last Name </label>
                                            <div className="col-md-9">
                                                <input type="text" className="form-control" value={this.state.member.last_name} name='last_name' onChange={this.handleChangeProfile} />
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">Email Address </label>
                                            <div className="col-md-9">
                                                <input type="text" readOnly className="form-control" value={this.state.member.email} name='email' onChange={this.handleChangeProfile} required />
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">LinkedIn Profile </label>
                                            <div className="col-md-9">
                                                <input type="text" className="form-control" value={this.state.member.linkedin_profile} name='linkedin_profile'  onChange={this.handleChangeProfile}  />


                                            </div>

                                        </div>

                                        <br />
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Address </label>
                                            <div className="col-md-9">
                                                <textarea name="address" id="address" className="form-control" value={this.state.member.address} name='address' onChange={this.handleChangeProfile}></textarea>
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">City </label>
                                            <div className="col-md-9">
                                                <input type="text" className="form-control" value={this.state.member.city} name='city' onChange={this.handleChangeProfile} required />
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">Country </label>
                                            <div className="col-md-9">
                                                <select className="form-control custom-select" name='country_code' onChange={this.handleCountryChange} required >
                                                    <option value="" hidden selected>Please Select Country</option>
                                                    {
                                                        this.state.countries.map((cntry) => (
                                                            cntry.country_code == this.state.member.country_code ? (<option selected value={cntry.country_code}>{cntry.country_name}</option>) : (<option value={cntry.country_code}>{cntry.country_name}</option>)
                                                        ))
                                                    }
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">State / Province </label>
                                            <div className="col-md-9">
                                                <select className="form-control custom-select" name='state_code' onChange={this.handleChangeProfile} required >
                                                    <option value="" hidden selected>Please Select State</option>
                                                    {
                                                        this.state.states.map((state) => (
                                                            state.state_code == this.state.member.state_code ? (<option selected value={state.state_code}>{state.state_name}</option>) : (<option value={state.state_code}>{state.state_name}</option>)
                                                        ))
                                                    }
                                                </select>
                                            </div>
                                        </div>


                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">Zip Code</label>
                                            <div className="col-md-9">
                                                <input type="text" className="form-control" value={this.state.member.zip_code} name='zip_code' onKeyPress={this.checkValidZipCode} onChange={this.handleChangeProfile} required />
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Business Website</label>
                                            <div className="col-md-9">
                                                <input type="text" className="form-control" value={this.state.member.business_website} name='business_website' onChange={this.handleChangeProfile} />
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Business Description </label>
                                            <div className="col-md-9">
                                                <textarea id="businessDescription" className="form-control" value={this.state.member.business_description} name='business_description' onChange={this.handleChangeProfile}></textarea>
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Status</label>
                                            <div className="col-md-9">

                                                <span className="form-control">
                                                    {this.state.member.status == 1 ? ('Active') : ''}
                                                    {this.state.member.status == 0 ? ('Inactive') : ''}
                                                    &nbsp;
                                                    </span>

                                            </div>
                                        </div>

                                    </div>

                                    <div className="card-footer text-right">
                                        {
                                            this.state.showUpdateBtn ? (<button type="submit" className="btn btn-brand">Update Profile</button>) : (<button type="button" disabled className="btn btn-brand">Update Profile</button>)
                                        }
                                    </div>


                                </form>

                            </div>




                        </div>


                    </div>




                </div>


            </Fragment>





        )
    }
}

export default UserProfileUpdate
