import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import Moment from 'react-moment';
import moment from 'moment'
import 'moment-timezone';
import StarRatingComponent from 'react-star-rating-component';
import 'moment/locale/fr';
// Variable
const $ = window.$

export class ReviewAdd extends Component {


    constructor(props) {
        super(props);



        this.state = {

            baseApiURL: '',
            showLoader: false,
            description: '',
            program_id: '',
            rating: '',
            programs: [],
            showAlertIcon: '',
            showAlert: false,
            showConfirmationAlert: false,
            showLoader: false,
            reviews: [],
            member: {},
            star_rating: [1, 2, 3, 4, 5],
            reviewrating: 0,
            title: '',
            note_counter: '500',
            curTime: '',
            rating: 0,

            offset: '',
            timeZone: '',
            memberPermission:[]
        }



    }


    utcToLocal = (utcdateTime, tz) => {
        var zone = moment.tz(tz).format("Z") // Actual zone value e:g +5:30
        var zoneValue = zone.replace(/[^0-9: ]/g, "") // Zone value without + - chars
        var operator = zone && zone.split("") && zone.split("")[0] === "-" ? "-" : "+" // operator for addition subtraction
        var localDateTime
        var hours = zoneValue.split(":")[0]
        var minutes = zoneValue.split(":")[1]
        if (operator === "-") {
            localDateTime = moment(utcdateTime).subtract(hours, "hours").subtract(minutes, "minutes").format("YYYY-MM-DD HH:mm:ss")
        } else if (operator) {
            localDateTime = moment(utcdateTime).add(hours, "hours").add(minutes, "minutes").format("YYYY-MM-DD HH:mm:ss")
        } else {
            localDateTime = "Invalid Timezone Operator"
        }
        return localDateTime
    }


    componentDidMount() {


        var offset = new Date().getTimezoneOffset();
        var timeZone = moment.tz.guess();
        this.setState({
            offset: offset,
            timeZone: timeZone
        })
        //const timeDate = this.utcToLocal("2019-11-14 07:15:37", "Asia/Kolkata")
        const timeDate = moment.utc("2020-02-05 05:40:21").utcOffset(offset).format('YYYY-MM-DD HH:mm:ss ZZ')
        this.setState({
            curTime: timeDate
        })


        // setInterval( () => {
        //     this.setState({
        //       curTime : new Date().toLocaleString()
        //     })
        //   },1000);


        var offset = new Date().getTimezoneOffset();


        // this.setState({
        //     curTime: offset
        // })


        //get user detail from memeber id
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
                memberPermission: memberSession.permissions,
            })


            //get program List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/list",
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ programs: response.data.member_programs });
                    }
                    else {
                        //fail
                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


            this.getReviewList();

        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }


    }


    //review list

    getReviewList = () => {
        //get review List

        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "member/review/list",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ reviews: response.data.reviews });
                    this.setState({ member: response.data.member });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }



    handleChangereview = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })

    }



    //resetPassword
    ReviewAddBtn = (e) => {
        e.preventDefault()
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            const member_id = memberSession.user_id;

            if (!this.state.rating) {
                this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: 'Please select rating.' });
                return false;
            }
            else if (!this.state.description) {
                this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: 'Please enter description.' });
                return false;
            }


            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/review/post",
                method: 'post',
                data: {
                    program_id: this.state.program_id,
                    rating: this.state.rating,
                    title: this.state.title,
                    description: this.state.description,
                },
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success           
                        this.getReviewList();
                        this.setState({ program_id: '', rating: '', title: '', description: '', showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/review' });

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions
    }



    handleSummerNote = (e) => {

        this.setState({ description: e })

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleReviewNote = (e) => {

        console.log(e.target)
        const review_note = e.target.innerText;
        const note_counter = 500 - review_note.length
        this.setState({ note_counter: note_counter })
        if (review_note.length > 500) {
            this.setState({ description: review_note.substr(0, 500) })
            e.preventDefault();
            return false;
        }
        else {
            return true;
        }
    }




    onStarClick = (nextValue, prevValue, name, e) => {

        // const xPos = (e.pageX - e.currentTarget.getBoundingClientRect().left) / e.currentTarget.offsetWidth;

        // if (xPos <= 0.5) {
        //     nextValue -= 0.5;
        // }

        // console.log('name: %s, nextValue: %s, prevValue: %s', name, nextValue, prevValue);
        // console.log(e);
        this.setState({ rating: nextValue });

    }



    render() {

        const getPermissionOject = (value) => {
            const permissiondata = this.state.memberPermission.filter(function (e) {
                return e.permission_id == value;
            });

            if (permissiondata) {
                return permissiondata[0];
            }
        }


 
        //Love - 22334025
        const lovedata = getPermissionOject('22334025');
        var love_r = 0;
        var love_w = 0;
        var love_u = 0;
        if (lovedata) {
            love_r = lovedata.r;
            love_w = lovedata.w;
            love_u = lovedata.u;
        }
 

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };


        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">

                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Review</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Review Post</li>
                                </ol>
                            </div>
                        </div>
                    </div>






                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <form className='form-horizontal' onSubmit={this.ReviewAddBtn}>
                            {love_w ? 
                            (
                            <div className="card">
                                <div className="card-body">

                                    <div className="form-group row">
                                        <label className="col-md-3 col-form-label">Program </label>
                                        <div className="col-md-7">
                                            <select className="form-control custom-select" name='program_id' onChange={this.handleChangereview} required >
                                                <option value="" hidden selected>Please Select Program</option>
                                                {
                                                    this.state.programs.map((cntry) => (
                                                        <option value={cntry.program_id}>{cntry.program_title}</option>
                                                    ))
                                                }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-3 col-form-label">Rating </label>
                                        <div className="col-md-7 brandColor">

                                            <StarRatingComponent
                                                starColor="#ffb400"
                                                emptyStarColor="#ffb400"
                                                name="rating"
                                                value={this.state.rating}
                                                //onStarClick={this.onStarClick}
                                                renderStarIcon={(index, value) => {
                                                    return (
                                                        <span>
                                                            <i className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />
                                                        </span>
                                                    );
                                                }}
                                                // renderStarIconHalf={() => {
                                                //     return (
                                                //         <span>
                                                //             <span style={{ position: 'absolute' }}><i className="fa fa-star-o" /></span>
                                                //             <span><i className="fa fa-star-half-o" /></span>
                                                //         </span>
                                                //     );
                                                // }}
                                                onStarHover={this.onStarClick}
                                            />

                                            {/* <label className="radio-inline lblrating">
                                            <input className="radrating" type="radio" onChange={this.handleChangereview} name="rating" value='1' />
                                            <i class="fa fa-star" aria-hidden="true"></i> 
                                        </label>
                                        <label className="radio-inline lblrating">
                                            <input className="radrating" type="radio" onChange={this.handleChangereview} name="rating" value='2' />
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>                                      
                                        </label>
                                        <label className="radio-inline lblrating">
                                            <input className="radrating" type="radio" onChange={this.handleChangereview} name="rating" value='3' />
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>                                     
                                            </label>
                                        <label className="radio-inline lblrating">
                                            <input className="radrating" type="radio" onChange={this.handleChangereview} name="rating" value='4' />
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>                                     
                                        </label>
                                        <label className="radio-inline lblrating">
                                            <input className="radrating" type="radio" onChange={this.handleChangereview} name="rating" value='5' />
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i> 
                                        </label> */}





                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-3 col-form-label">Title </label>
                                        <div className="col-md-7">
                                            <input type="text" className="form-control" name='title' onChange={this.handleChangereview} required />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-3 col-form-label">Review </label>
                                        <div className="col-md-7">
                                            <ReactSummernote required
                                                onKeyUp={this.handleReviewNote}
                                                onPaste={this.handleReviewNote}


                                                value={this.state.description}

                                                popover='false'
                                                options={{
                                                    height: 150,
                                                    toolbar: [
                                                        ['style', ['style']],
                                                        ['font', ['bold', 'underline', 'clear']],
                                                        ['fontname', ['fontname']],
                                                        ['para', ['ul', 'ol', 'paragraph']],
                                                        ['table', ['table']],
                                                        ['insert', ['link', 'picture', 'video']],
                                                        ['view', ['fullscreen', 'codeview']]
                                                    ]
                                                }}
                                                onChange={this.handleSummerNote}

                                            />
                                            <small className="float-right">Counts: {this.state.note_counter}</small>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-footer text-right">
                                    <button type="submit" className="btn btn-brand">Submit</button>
                                </div>
                            </div>
                            ):""}

                        </form>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-body">
                                {
                                    this.state.reviews.map((review, index) => (
                                        <div className="row" key={index}>

                                            <label className={review.rating == '0.50' ? 'col-md-3 col-form-label text-right' : 'hidden fa fa-star reviewsta'}>
                                                <i class="fa fa-star-half-o star-rating" aria-hidden="true"></i>
                                                <br />
                                                <span>{review.program_title} &nbsp;</span>
                                            </label>

                                            <label className={review.rating == '1.00' ? 'col-md-3 col-form-label text-right' : 'hidden fa fa-star reviewsta'}>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <br />
                                                <span>{review.program_title} &nbsp;</span>
                                            </label>

                                            <label className={review.rating == '1.50' ? 'col-md-3 col-form-label text-right' : 'hidden fa fa-star reviewsta'}>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star-half-o star-rating" aria-hidden="true"></i>
                                                <br />
                                                <span>{review.program_title} &nbsp;</span>
                                            </label>

                                            <label className={review.rating == '2.00' ? 'col-md-3 col-form-label text-right' : 'hidden fa fa-star reviewsta'}>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <br />
                                                <span>{review.program_title} &nbsp;</span>
                                            </label>

                                            <label className={review.rating == '2.50' ? 'col-md-3 col-form-label text-right' : 'hidden fa fa-star reviewsta'}>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star-half-o star-rating" aria-hidden="true"></i>
                                                <br />
                                                <span>{review.program_title} &nbsp;</span>
                                            </label>

                                            <label className={review.rating == '3.00' ? 'col-md-3 col-form-label text-right' : 'hidden fa fa-star reviewsta'}>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <br />
                                                <span>{review.program_title} &nbsp;</span>
                                            </label>

                                            <label className={review.rating == '3.50' ? 'col-md-3 col-form-label text-right' : 'hidden fa fa-star reviewsta'}>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star-half-o star-rating" aria-hidden="true"></i>
                                                <br />
                                                <span>{review.program_title} &nbsp;</span>
                                            </label>

                                            <label className={review.rating == '4.00' ? 'col-md-3 col-form-label text-right' : 'hidden fa fa-star reviewsta'}>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <br />
                                                <span>{review.program_title} &nbsp;</span>

                                            </label>

                                            <label className={review.rating == '4.50' ? 'col-md-3 col-form-label text-right' : 'hidden fa fa-star reviewsta'}>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star-half-o star-rating" aria-hidden="true"></i>
                                                <br />
                                                <span>{review.program_title} &nbsp;</span>

                                            </label>

                                            <label className={review.rating == '5.00' ? 'col-md-3 col-form-label text-right' : 'hidden fa fa-star reviewsta'}>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <i class="fa fa-star star-rating" aria-hidden="true"></i>
                                                <br />
                                                <span>{review.program_title} &nbsp;</span>
                                            </label>






                                            <div className="col-md-7 reviewborder">
                                                <div className="mb-1 cm-review-title">{review.title}</div>
                                                <div dangerouslySetInnerHTML={{ __html: review.description }} />
                                                <span className='reviewfrom float-left'>From: {this.state.member.first_name}</span>
                                                <span className='reviewfrom float-right'>
                                                    {(new Date(this.utcToLocal(review.created_date, this.state.timeZone))).toLocaleDateString('en-US', DATE_OPTIONS)}</span>
                                            </div>
                                        </div>
                                    ))


                                }
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>

        )
    }
}

export default ReviewAdd
