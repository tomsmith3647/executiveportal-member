import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'

import LoadingOverlay from 'react-loading-overlay';
import { PrismCode } from 'react-prism'
import { Player, ControlBar, BigPlayButton, PlaybackRateMenuButton } from 'video-react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
// import ReadMoreReact from  'read-more-react';

import ReadMoreAndLess from 'react-read-more-less';

var $ = window.$
class ModuleDetails extends Component {


    constructor(props, context) {
        super(props, context);

        this.state = {
            topic_complete_status:0,
            user_id: '',
            email: '',
            full_name: '',
            showAlert: false,
            showAlertMessage: '',
            memberPermission: [],
            programs: [],
            categories: [],
            modules: [],
            resources: [],
            topics: [],
            items: [],
            program: {},
            category: {},
            module: {},
            program_id: '',
            category_id: '',
            module_id: '',
            resource_id: '',
            topic_id: '',
            transcript: {},
            baseApiURL: '',
            module_id_previous: '',
            module_id_next: '',
            complete_status: '',
            playerSource: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            inputVideoUrl: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            source: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            program_module: {
                member_id: '',
                program_id: '',
                module_id: '',
                complete_status: '0',
                created_by: ''
            },
            topic_complete_status:'',
        };


        this.play = this.play.bind(this);
        this.pause = this.pause.bind(this);
        this.seek = this.seek.bind(this);



    }
 

 
    componentDidMount() {


        // $(document).bind("contextmenu",function(e){
        //     return false;
        // });

        // Fixed left Module after breadcrumb scrolled
        $(window).scroll(function () {

            const scrollTop = $(window).scrollTop();

            if (scrollTop <= 76.8) {
                $(".module-left").css("marginTop", `-${scrollTop}px`);
            } else {
                $(".module-left").css("marginTop", `-76.8px`);
            }

        });


        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                auth_token: memberSession.auth_token,
                email: memberSession.email,
                full_name: memberSession.full_name,
                memberPermission: memberSession.permissions,
            })

            //get Member Module Details
            this.setState({ showLoader: true });
            const module_id = this.props.match.params.module_id;
            this.setState({ module_id: module_id });
            axios({
                url: API_URL + "member/program/category/module/details/" + module_id,
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    
                    if (error_code == '301') {
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/dashboard'});
                    }
                    else if (error_code == '200') {
                        //success 
                        const ApiURL = response.data.api_url;
                        this.setState({ baseApiURL: ApiURL });
                        this.setState({ module: response.data.module, program_id:response.data.module.program_id, module_id_next: response.data.module_id_next, topic_complete_status: response.data.topic_complete_status, complete_status: response.data.complete_status, module_id_previous: response.data.module_id_previous, program_id: response.data.module.program_id, category_id: response.data.module.category_id });
                        this.saveMemberHistory();
                        //this.load();
                        //Get Module Details
                        if (this.state.program_id && this.state.category_id) {

                            //get Member Module Details
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "member/program/category/details/" + this.state.program_id + '/' + this.state.category_id,
                                method: 'get',
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    //const error_message = response.data.error_message;                       
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ category: response.data.category });
                                    }
                                    else {
                                        //fail
                                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        }


                        //Get Module Lsit
                        if (this.state.program_id && this.state.category_id) {
                            //get Member Module Details
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "member/program/category/module/list/" + this.state.program_id + '/' + this.state.category_id,
                                method: 'get',
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    //const error_message = response.data.error_message;                       
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ modules: response.data.modules });
                                    }
                                    else {
                                        //fail
                                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        }

                        //Get Item Lsit
                        if (this.state.program_id && this.state.category_id) {
                            //get Member Module Details
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "member/program/category/item/list/" + this.state.program_id + '/' + this.state.category_id,
                                method: 'get',
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    //const error_message = response.data.error_message;                       
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ items: response.data.items });
                                    }
                                    else {
                                        //fail
                                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        }

                        //Get Resource Lsit
                        if (this.state.program_id && this.state.module_id) {
                            //get Member Module Details
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "member/program/category/module/resource/list/" + this.state.program_id + '/' + this.state.module_id,
                                method: 'get',
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    //const error_message = response.data.error_message;                       
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ resources: response.data.resources });
                                    }
                                    else {
                                        //fail
                                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        }

                        //Get topic Lsit
                        if (this.state.program_id && this.state.module_id) {
                            //get Member Module Details
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "member/program/category/module/topic/list/" + this.state.program_id + '/' + this.state.module_id,
                                method: 'get',
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    //const error_message = response.data.error_message;                       
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ topics: response.data.topics });
                                    }
                                    else {
                                        //fail
                                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        }

                        //Get transcript Lsit
                        if (this.state.program_id && this.state.module_id) {
                            //get Member Module Details
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "member/program/category/module/transcript/details/" + this.state.program_id + '/' + this.state.module_id,
                                method: 'get',
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    //const error_message = response.data.error_message;                       
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ transcript: response.data.transcript });
                                    }
                                    else {
                                        //fail
                                        // this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        }
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });

                    }
                })
                .catch(err => {
                    console.log(err);
                });

                
        }

    }


    //saveMemberHistory
    saveMemberHistory = () => {
         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
                                  
            const postData = {     
                                
                member_id: memberSession.user_id,                
                url:window.location.pathname,  
                program_id: this.state.program_id,                              
                }
            axios({
                url: API_URL + "member/history",
                method: 'post',
                data: postData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {                        
                    //success                                           
                    //this.setState({ showAlertIcon: 'success', showAlert: false, showAlertMessage: error_message });                        
                }
                else {
                    //fail
                    //this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }         
    }



    //Member module complete

    memberProgramModuleUpdateUnmark = (e) => {

        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })

            const postData = {
                ...this.state.program_module,
                member_id: this.state.user_id,
                program_id: this.state.module.program_id,
                category_id: this.state.module.category_id,
                module_id: this.state.module.module_id,
                complete_status: '0',
                created_by: this.state.user_id,
            };



            if (!postData.module_id) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "invalid member id" });
                return false;
            }
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/module/complete",
                method: 'post',
                data: postData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success   
                        this.getModuleList();     
                        this.getModuleDetails();                                        
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.module_id });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
    }


    memberProgramModuleUpdate = (e) => {

        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })

            const postData = {
                ...this.state.program_module,
                member_id: this.state.user_id,
                program_id: this.state.module.program_id,
                category_id: this.state.module.category_id,
                module_id: this.state.module.module_id,
                complete_status: '1',
                created_by: this.state.user_id,
            };



            if (!postData.module_id) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "invalid member id" });
                return false;
            }
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/module/complete",
                method: 'post',
                data: postData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success 
                        this.getModuleList();       
                        this.getModuleDetails();                                        
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.module_id });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
    }
    //End of member module complete


    updatePlayerInfo() {
        const { inputVideoUrl } = this.state;
        this.setState({
            playerSource: inputVideoUrl
        });
    }


    setMuted(muted) {
        return () => {
            this.player.muted = muted;
        };
    }

    handleStateChange(state) {
        // copy player state to this component's state
        this.setState({
            player: state
        });
    }

    play() {

       this.player.play();
       const { player } = this.player.getState();
      // this.player.seek(34);
       // alert("play = "+ player.currentTime);
 
    }

    pause() {
        this.player.pause();
        const { player } = this.player.getState();
       // alert("push = "+player.currentTime);

        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        const postData = {
             
            module_id: this.state.module_id,
            time: player.currentTime,
            action: "push",
             
        };



        
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "member/program/category/module/video/transaction",
            method: 'post',
            data: postData,
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                                                
                    this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message});
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
    }

    load() {
        this.player.load();
    }

    changeCurrentTime(seconds) {
        return () => {
            const { player } = this.player.getState();
            this.player.seek(player.currentTime + seconds);
        };
    }

    seek(seconds) {
        return () => {
            this.player.seek(seconds);
             
        };
    }

    changePlaybackRateRate(steps) {
        return () => {
            const { player } = this.player.getState();
            this.player.playbackRate = player.playbackRate + steps;
        };
    }

    changeVolume(steps) {
        return () => {
            const { player } = this.player.getState();
            this.player.volume = player.volume + steps;
        };
    }

    changeSource(name) {
        return () => {

            this.player.load();
        };
    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    

    getModuleList = () => {
         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })                                     
            this.setState({ showLoader: true });       
            axios({
                url: API_URL + "member/program/category/module/list/" + this.state.program_id + '/' + this.state.category_id,
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                //const error_message = response.data.error_message;                       
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ modules: response.data.modules });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });
        }                         
    }

     //memberProgramTopicUpdateUnmark
     getModuleDetails = () => {
         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })                                     
            this.setState({ showLoader: true });       
            axios({
                url: API_URL + "member/program/category/module/details/" + this.state.module_id,
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success 
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ module: response.data.module, module_id_next: response.data.module_id_next, topic_complete_status: response.data.topic_complete_status, complete_status: response.data.complete_status, module_id_previous: response.data.module_id_previous, program_id: response.data.module.program_id, category_id: response.data.module.category_id });
                }
            })
            .catch(err => {
                console.log(err);
            });
        }                         
    }

    //memberProgramTopicUpdateUnmark
    memberProgramTopicUpdateUnmark = (e) => {
         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            const topic_id = e.currentTarget.value;
            const postData = {   
                ...this.state.program_item,                 
                member_id: this.state.user_id,
                program_id: this.state.module.program_id,
                category_id: this.state.module.category_id,
                module_id: this.state.module.module_id,
                topic_id: topic_id,
                complete_status: '0',
                created_by: this.state.user_id,                     
                }

           
            if(!postData.topic_id)
            {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "invalid member id" });
                return false;
            }
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/topic/complete",
                method: 'post',
                data: postData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {                        
                    //success       
                    
                    if (this.state.program_id && this.state.module_id) {




                        //get Member Module Details
                        this.setState({ showLoader: true });

                        this.getModuleList();
                        this.getModuleDetails();


                        axios({
                            url: API_URL + "member/program/category/module/topic/list/" + this.state.program_id + '/' + this.state.module_id,
                            method: 'get',
                            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                        })
                            .then(response => {
                                const error_code = response.data.error_code;
                                //const error_message = response.data.error_message;                       
                                this.setState({ showLoader: false });
                                if (error_code == '200') {
                                    //success                        
                                    this.setState({ topics: response.data.topics });
                                }
                                else {
                                    //fail
                                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                }
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    }

                    this.setState({ showAlertIcon: 'success', showAlert: false, showAlertMessage: error_message});                        
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }         
    }
    //End of member module complete



    //Member module complete
    memberProgramTopicUpdate = (e) => {
         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            const topic_id = e.currentTarget.value;
            const postData = {   
                ...this.state.program_item,                 
                member_id: this.state.user_id,
                program_id: this.state.module.program_id,
                category_id: this.state.module.category_id,
                module_id: this.state.module.module_id,
                topic_id: topic_id,
                complete_status: '1',
                created_by: this.state.user_id,                     
                }

           
            if(!postData.topic_id)
            {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "invalid member id" });
                return false;
            }
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/topic/complete",
                method: 'post',
                data: postData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {                        
                    //success       
                    
                    if (this.state.program_id && this.state.module_id) {
                        //get Member Module Details
                        this.setState({ showLoader: true });
                        this.getModuleList();
                        this.getModuleDetails();
                        axios({
                            url: API_URL + "member/program/category/module/topic/list/" + this.state.program_id + '/' + this.state.module_id,
                            method: 'get',
                            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                        })
                            .then(response => {
                                const error_code = response.data.error_code;
                                //const error_message = response.data.error_message;                       
                                this.setState({ showLoader: false });
                                if (error_code == '200') {
                                    //success                        
                                    this.setState({ topics: response.data.topics });
                                }
                                else {
                                    //fail
                                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                }
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    }

                    this.setState({ showAlertIcon: 'success', showAlert: false, showAlertMessage: error_message});                        
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }         
    }
    //End of member module complete




    render() {
        return (
            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Module</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            {/* <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li> */}
                            <li className="breadcrumb-item"><a href={`/program/details/${this.state.category.program_id}`}>Program</a></li>
                            <li className="breadcrumb-item"><a href={`/program/category/details/${this.state.category.category_id}`}>Category</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Module Details</li>
                        </ol>
                    </div>
                </div>
                <br />
                <div className='module-wrapper'>
                    <div className="module-left white-container">
                        <div className="module-category-img">


                            <img src={this.state.category.category_icon ? this.state.baseApiURL + this.state.category.category_icon : '/assets/images/gallery/1.jpg'} alt="category-img" className='img-fluid' />
                        </div>
                        <div className="module-title">
                            <h3 className='fw-700 black' title={this.state.category.category_title}>
                                {this.state.category.category_title}
                            </h3>
                        </div>
                        <div className="module-category-list">
                            <ul>

                                {this.state.modules.map((module, mindex) => (
                                    <li>
                                        <a href={`/program/category/module/details/${module.module_id}`} className={module.complete_status == '1' ? 'completed' : 'Active', module.module_id == this.state.module_id ? 'brandColor' : ''}>
                                            
                                            
                                            {/* <span>{mindex + 1}. </span> */}
                                            <span style={{width: '90%'}}>{module.module_title}</span>

                                            {module.complete_status == '1' ? (<span className="sidebar-nav-item-icon fa fa-check-circle text-success moduleLeftNav"></span>)
                                                                                    : (<span className="sidebar-nav-item-icon fa fa-check-circle moduleLeftNav"></span>)}
                                        </a>
                                    </li>
                                ))}


                            </ul>
                            
                        </div>

                        <div className="module-action">
                            <div className="module-category-list">
                                {/* <ul style={{ border: 'none', margin: '30px 0px 0', paddingBottom: '0' }}>
                                    {this.state.items.map((item, itmindex) => (
                                        <li>
                                            <a href={`/program/category/item/details/${item.item_id}`} className={item.complete_status == '1' ? 'completed' : 'Active'} >
                                                <i className="fa fa-check-circle" aria-hidden="true"></i>
                                               
                                                {item.item_title}
                                            </a>
                                        </li>
                                    ))}
                                </ul> */}

                                <ul>
                                    <li>
                                        <a href={`/program/category/actionitems/${this.state.module.program_id}/${this.state.module.category_id}`} >Action Items</a>
                                    </li>
                                    <li>
                                        <a href="/faqs">Help</a>
                                    </li>
                                </ul>

                            </div>
                            <div>

                            </div>
                        </div>
                    </div>
                    <div className="module-right">
                        <div className="white-container">
                            {/* <div className="module-breadcrumb d-none d-sm-block">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb" style={{ backgroundColor: 'transparent', padding: '0' }}>

                                        <li className="breadcrumb-item"><a href={`/program/details/${this.state.category.program_id}`}>Program</a></li>
                                        <li className="breadcrumb-item"><a href={`/program/category/details/${this.state.category.category_id}`}>Category</a></li>
                                        <li className="breadcrumb-item active" aria-current="page">Module Details</li>
                                    </ol>
                                </nav>
                            </div> */}
                            <div className='module-video-header'>

                                

                                    <h3 className='module-title' style={{display: 'flex'}}>
                                         
                                        {/* {this.state.complete_status == '1' ? (<span className="sidebar-nav-item-icon fa fa-check-circle text-success"></span>)
                                                                                    : (<span className="sidebar-nav-item-icon fa fa-check-circle"></span>)} */}
                                         
                                        {this.state.module.module_title} 
                                       
                                        
                                        
                                    </h3>


                                    
                                        <div className='float-right'>
                                        {this.state.complete_status == '0' ? 
                                            
                                            this.state.topic_complete_status == '1' ?
                                            (<button className='btn btn-brand' onClick={this.memberProgramModuleUpdate}>Mark Complete</button>) :
                                            (<button className='btn btn-blackColor'  disabled title="Please complete topics">Mark Complete</button>)                                                                                
                                            :''
                                        }

                                        {this.state.complete_status == '1' ? (<button className='btn btn-brandColor btn-fixed' title="Mark Incomplete"  onClick={this.memberProgramModuleUpdateUnmark}>Completed</button>) : ''}

                                    

                                   
                                </div>
                                
                                
                            </div>
                            <div className='module-video-player'>
                                 
                                {this.state.module.module_video ? (
                                <Fragment>
                                <Player
                                    poster={this.state.module.module_image ? this.state.baseApiURL + this.state.module.module_image +'/'+ this.state.user_id +'/'+ this.state.auth_token : '/assets/images/gallery/1.jpg'}
                                    ref={player => {
                                        this.player = player;
                                    }}
                                //autoPlay
                                >
                                    <BigPlayButton position="center" />
                                    {/* <source src={this.state.baseApiURL + this.state.module.module_video} /> */}
                                    <source src={this.state.baseApiURL + this.state.module.module_video +'/'+ this.state.user_id +'/'+ this.state.auth_token} />
                                    <ControlBar autoHide={true} />
                                    <ControlBar>
                                        <PlaybackRateMenuButton rates={[5, 2, 1, 0.5, 0.1]} />
                                    </ControlBar>
                                </Player>
                                {/* <Button onClick={this.play} className="mr-3">
                                    play()
                                </Button>
                                <Button onClick={this.pause} className="mr-3">
                                    pause()
                                </Button>
                                <Button onClick={this.seek} className="mr-3">
                                seek()
                                </Button> */}
                                </Fragment>
                                ) : ''}
                            </div>
                            <div className='module-video-footer'>
                                <div className="module-video-buttons">
                                    {this.state.module_id_previous ? (<a href={'/program/category/module/details/' + this.state.module_id_previous}><i className="fa fa-caret-left" aria-hidden="true"></i> Previous</a>) : ''}
                                    {this.state.module_id_next ? (<a href={'/program/category/module/details/' + this.state.module_id_next}>Next <i className="fa fa-caret-right" aria-hidden="true"></i></a>) : ''}
                                </div>
                            </div>
                        </div>

                        <div className="module-lesson">
                            <div className="lesson">
                                <h6 className='title text-uppercase'>About This Module</h6>

                                <div dangerouslySetInnerHTML={{ __html: this.state.module.module_description }} />


                            </div>
                            <div className="resources">
                                <h6 className='title text-uppercase'>resources</h6>
                                <ul className='resources-item'>

                                    {this.state.resources.map((res, resindex) => (
                                        <li>
                                            <a href={`${res.resource_url}`} className={res.resource_type == 'link' ? 'link' : 'file'} target='_blank'>
                                                {res.resource_title}
                                            </a>
                                        </li>
                                    ))}
                                    {this.state.resources.length == 0 ? (<li><span className="no-download text-center">There are no resource in this module.</span></li>) : ''}

                                    {/* <li><a href="#">Alchemic conversion script (key)</a></li>
                                    <li><a href="#">Alchemic conversion script (ppt)</a></li>
                                    <li><a href="#">Message hypothesis</a></li>
                                    <li><a href="#">Niche-offer-result hypothesis</a></li> */}
                                </ul>
                            </div>
                        </div>
                        {this.state.topics.length ?
                            <div className="additional-topics white-container">

                                <h3 className='additional-topics-title text-center'>Additional Topics</h3>

                                <div className="accordion" id="accordionExample">
                                    {this.state.topics.map((top, topindex) => (
                                        <div className="card">
                                            <div className="card-header" id={'heading-' + topindex}>
                                                <a className="btn btn-link d-flex" data-toggle="collapse" data-target={'#collapse-' + topindex} aria-expanded="false">

                                                {top.complete_status == '1' ? (<span className="sidebar-nav-item-icon fa fa-check-circle text-success"></span>)
                                                                                    : (<span className="sidebar-nav-item-icon fa fa-check-circle"></span>)}
                                                    <span className='counter'>                                                        
                                                         {topindex + 1}. 
                                                    </span> 
                                                        
                                                    {top.topic_title}
                                                    
                                                    <span className="ml-auto mr-1 arrow"></span></a>
                                            </div>
                                            <div id={'collapse-' + topindex} className="collapse" aria-labelledby={'heading-' + topindex} data-parent="#accordionExample">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className='float-right p-3'>
                                                                            {/* {top.complete_status =='0' ? (<button className='btn btn-success' value={top.topic_id} onClick={this.memberProgramTopicUpdate}>Mark Complete</button>):''}
                                                                            {top.complete_status =='1' ? (<button className='btn btn-success' value={top.topic_id} disabled>Topic Completed</button>):''} */}

                                                                            {top.complete_status =='0' ? (<button className='btn btn-brand' value={top.topic_id} onClick={this.memberProgramTopicUpdate}>Mark Complete</button>):''}
                                                                            {top.complete_status =='1' ? (<button className='btn btn-brandColor' value={top.topic_id} title="Mark Incomplete" onClick={this.memberProgramTopicUpdateUnmark}>Completed</button>):''}
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div className="card-body row" style={{paddingTop:'0px'}}>

                                                    
                                                    {top.topic_video ?
                                                        <div className='col'>
                                                            <Player
                                                                poster={top.topic_image ? this.state.baseApiURL + top.topic_image +'/'+ this.state.user_id +'/'+ this.state.auth_token : '/assets/images/gallery/1.jpg'}
                                                                ref={player => {
                                                                    this.player = player;
                                                                }}
                                                            //autoPlay
                                                            >
                                                                <BigPlayButton position="center" />
                                                                 
                                                                <source src={this.state.baseApiURL + top.topic_video +'/'+ this.state.user_id +'/'+ this.state.auth_token} />
                                                                <ControlBar autoHide={true} />
                                                            </Player>
                                                        </div>
                                                        : ''}
                                                    <div className='col'>
                                                        <div dangerouslySetInnerHTML={{ __html: top.topic_description }} />
                                                        
                                                    </div>

                                                      

                                                                
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            : ''}

                        {this.state.transcript.transcript_id ?
                            (<div className="module-transcript white-container">
                                <div className="module-transcript-header d-block" >

                                    
                                    <div className="row">
                                    
 
                                        <div className="col-md-7">
                                        <h6 className='module-transcript-title text-uppercase fw-600'>FULL VIDEO TRANSCRIPT</h6>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="float-right">
                                                <span className='fw-700 text-uppercase'>mp3 download</span> &nbsp;
                                                <a href={this.state.transcript.transcript_url} target="_blank" className='download ml-auto'><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                            </div>
                                        </div>

                                        
                                    </div>

                                    
                                    
                                    
                                </div>
                                <div className="row">                            
                                    <div className="col-md-12 module-transcript">
                                    <ReadMoreAndLess
                                                ref={this.ReadMore}
                                                className="read-more-content"
                                                charLimit={250}
                                                readMoreText="Read more"
                                                readLessText="Read less"
                                            >
                                                {this.state.transcript.transcript_description}
                                            </ReadMoreAndLess>

                                    </div>
                                </div>
                                {/* <div className="module-transcript-wrapper">

                                


                                      <div className="module-transcript-body">
                                        <div   dangerouslySetInnerHTML={{ __html: this.state.transcript.transcript_description }} />
                                         
                                            
                                    </div> 

                                </div>
                                <div className="read-more text-center">
                                    <NavLink to={'/program/module/transcript/' + this.state.module_id} className='btn btn-primary'>Read More</NavLink>
                                </div> */}
                            </div>) : ''}

                    </div>
                </div>
            </Fragment>
        )
    }
}

export default ModuleDetails
