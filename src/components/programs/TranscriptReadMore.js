import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

import { PrismCode } from 'react-prism'
import { Player, ControlBar, BigPlayButton } from 'video-react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';


export class TranscriptReadMore extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        showAlert: false,
        showAlertMessage: '',
        memberPermission: [],
        programs:[],
        categories:[],
        modules:[],   
        resources:[],   
        topics:[],   
        program:{},
        category:{},
        module:{},
        program_id: '',         
        category_id: '',         
        module_id: '',         
        resource_id: '',         
        topic_id: '',  
        transcript:{}, 
        
              
    };

    componentDidMount() {
        window.scrollTo(0, 0)
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                memberPermission: memberSession.permissions,
            })

            //get Member Module Details
            this.setState({ showLoader: true });
            const module_id = this.props.match.params.module_id;
            this.setState({ module_id: module_id });
            axios({
                url: API_URL + "member/program/category/module/details/"+module_id,
                method: 'get',    
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }        
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;                       
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success 
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });                       
                    this.setState({module: response.data.module, program_id: response.data.module.program_id, category_id: response.data.module.category_id}); 
                                         
                    //Get transcript Lsit
                    if(this.state.program_id && this.state.module_id)
                    {
                        //get Member Module Details
                        this.setState({ showLoader: true });
                        axios({
                            url: API_URL + "member/program/category/module/transcript/details/"+this.state.program_id+'/'+this.state.module_id,
                            method: 'get',    
                            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }        
                        })
                        .then(response => {
                            const error_code = response.data.error_code;
                            //const error_message = response.data.error_message;                       
                            this.setState({ showLoader: false });
                            if (error_code == '200') {
                                //success                        
                                this.setState({transcript: response.data.transcript});                      
                            }
                            else {
                                //fail
                                this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                            }
                        })
                        .catch(err => {
                            console.log(err);
                        });
                    }
                }
                else {
                    //fail
                    this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });

                }
            })
            .catch(err => {
                console.log(err);
            });
        }

    }

    render() {
        return (

            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Module</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            {/* <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li> */}
                            <li className="breadcrumb-item"><a href={`/program/details/${this.state.transcript.program_id}`}>Program</a></li>
                            <li className="breadcrumb-item"><a href={`/program/category/details/${this.state.transcript.category_id}`}>Category</a></li>
                            <li className="breadcrumb-item"><a href={`/program/category/module/details/${this.state.transcript.module_id}`}>Module Details</a></li>
                            <li className="breadcrumb-item active" aria-current="page">TRANSCRIPT Details</li>
                        </ol>
                    </div>
                </div>
                <br />
                <div className="transcript-full white-container">
                    <div className="module-transcript-header">
                        <h6 className='module-transcript-title text-uppercase fw-600'>transcript</h6>
                        <a target="_blank" href={this.state.transcript.transcript_url} className='download ml-auto fw-700'>MP3 <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                    </div>
                    <div className="module-transcript-wrapper">
                        <div className="module-transcript-body">
                            <div dangerouslySetInnerHTML={{__html: this.state.transcript.transcript_description}} />
                        </div>

                    </div>

                </div>
            </Fragment>
        )
    }
}

export default TranscriptReadMore
