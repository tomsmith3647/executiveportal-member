import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'

class Module extends Component {

    componentDidMount() {

        var $ = window.$
        // get initial position of the element



    }
    render() {
        return (
            <Fragment>
                <div className='module-wrapper'>
                    <div className="module-left white-container">
                        <div className="module-category-img">
                            <img src="/assets/images/gallery/1.jpg" alt="category-img" className='img-fluid' />
                        </div>
                        <div className="module-title">
                            <h3 className='fw-700 black'>
                                Category Three
                            </h3>
                        </div>
                        <div className="module-category-list">
                            <ul>
                                <li>
                                    <a href="#" className='completed'>
                                        <i className="fa fa-check-circle" aria-hidden="true"></i>
                                        <span>1.</span>
                                        Module
                                    </a>
                                </li>
                                <li>
                                    <a href="#" className='completed'>
                                        <i className="fa fa-check-circle" aria-hidden="true"></i>
                                        <span>2.</span>
                                        Module
                                    </a>
                                </li>
                                <li>
                                    <a href="#" className='active' >
                                        <i className="fa fa-check-circle" aria-hidden="true"></i>
                                        <span>3.</span>
                                        Module
                                    </a>
                                </li>
                                <li>
                                    <a href="#" className=''>
                                        <i className="fa fa-check-circle" aria-hidden="true"></i>
                                        <span>4.</span>
                                        Module
                                    </a>
                                </li>
                                <li>
                                    <a href="#" className=''>
                                        <i className="fa fa-check-circle" aria-hidden="true"></i>
                                        <span>5.</span>
                                        Module
                                    </a>
                                </li>

                            </ul>
                        </div>

                        <div className="module-action">
                            <div className="module-category-list">
                                <ul style={{ border: 'none', margin: '30px 0px 0', paddingBottom: '0' }}>
                                    <li>
                                        <a href="#">
                                            <i className="fa fa-check-circle" aria-hidden="true"></i>
                                            Action Items
                                    </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Help
                                    </a>
                                    </li>

                                </ul>
                            </div>

                        </div>

                    </div>
                    <div className="module-right">
                        <div className="white-container">
                            <div className="module-breadcrumb d-none d-sm-block">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb" style={{ backgroundColor: 'transparent', padding: '0' }}>
                                        <li className="breadcrumb-item"><a href="#">Program</a></li>
                                        <li className="breadcrumb-item"><a href="#">Category</a></li>
                                        <li className="breadcrumb-item active" aria-current="page">Module</li>
                                    </ol>
                                </nav>
                            </div>
                            <div className='module-video-header'>
                                <h3 className='module-title'>3. Module</h3>
                                <div className='module-completed'>
                                    <button className='btn btn-success'>Lesson Completed</button>
                                </div>
                            </div>
                            <div className='module-video-player'>

                            </div>
                            <div className='module-video-footer'>
                                <div className="module-video-buttons">
                                    <button><i className="fa fa-caret-left" aria-hidden="true"></i> Previous</button>
                                    <button>Next <i className="fa fa-caret-right" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>

                        <div className="module-lesson">
                            <div className="lesson">
                                <h6 className='title text-uppercase'>about this lesson</h6>
                                <p className='lesson-detail'>
                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Impedit, temporibus? Quas facilis rem exercitationem odio, ex magni nisi laudantium sit est animi ullam doloremque perspiciatis ratione, cupiditate qui inventore mollitia optio, molestiae voluptatibus? Amet vel dolore commodi voluptas architecto porro tempora animi iste, libero eaque accusamus quis deleniti nesciunt non.
                                </p>
                                <p className='lesson-detail'>Here's what we cover:</p>
                                <ul className='cover-topic pl-3'>
                                    <li>Why Crafting you script and making it your own is extremely important</li>
                                    <li>The scientific method: Hypothesis, action, results, feedback</li>
                                    <li>The step by step process for adjusting the script</li>
                                    <li>Recording your sales calls and storing them for feedback</li>
                                    <li>Action items and the iterative process used to evolve your perfect script</li>
                                </ul>
                            </div>
                            <div className="resources">
                                <h6 className='title text-uppercase'>resources</h6>
                                <ul className='resources-item'>
                                    <li><a href="#">Alchemic conversion script (key)</a></li>
                                    <li><a href="#">Alchemic conversion script (ppt)</a></li>
                                    <li><a href="#">Message hypothesis</a></li>
                                    <li><a href="#">Niche-offer-result hypothesis</a></li>
                                </ul>
                            </div>
                        </div>

                        <div className="additional-topics white-container">

                            <h3 className='additional-topics-title text-center'>Addtional Topics</h3>

                            <div className="accordion" id="accordionExample">
                                <div className="card">
                                    <div className="card-header" id="headingOne">
                                        <a className="btn btn-link d-flex" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false">
                                            <span className='counter'> 01 </span> Jump In
                                            <span className="ml-auto mr-1 arrow"></span></a>
                                    </div>
                                    <div id="collapseOne" className="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div className="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header" id="headingTwo">
                                        <a className=" btn btn-link collapsed d-flex align-items-center" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false">
                                            <span className='counter'> 02 </span>  Values and Profits Are Not Enemies
                                            <span className="ml-auto mr-1 arrow"></span>
                                        </a>
                                    </div>
                                    <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div className="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header" id="headingThree">
                                        <a className="btn btn-link collapsed d-flex" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false">
                                            <span className='counter'> 03 </span>  Be Curious<span className="ml-auto arrow mr-1"></span>
                                        </a>
                                    </div>
                                    <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                        <div className="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="module-transcript white-container">
                            <div className="module-transcript-header">
                                <h6 className='module-transcript-title text-uppercase fw-600'>FULL VIDEO TRANSCRIPT / <span className='fw-700'>mp3 download</span></h6>
                                <a href="#" className='download ml-auto'><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                            </div>
                            <div className="module-transcript-wrapper">
                                <div className="module-transcript-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus deleniti voluptatem asperiores illum rerum id tempora soluta vitae sint repudiandae possimus autem sequi necessitatibus rem eius, eveniet neque consectetur placeat exercitationem aliquam mollitia voluptatum excepturi. Pariatur eligendi voluptatibus ab repellendus tempore molestias neque omnis, porro accusamus rerum ea corporis cumque eveniet explicabo temporibus itaque maxime sint nulla voluptate assumenda dolore quas exercitationem quibusdam sequi? Laudantium saepe tenetur aspernatur nisi perferendis natus itaque dicta iure explicabo dolore voluptatem nesciunt doloribus omnis doloremque, reprehenderit exercitationem esse eligendi? Doloremque inventore provident eius iste quam, cum, commodi quae quisquam consequatur dolorem laudantium sit facilis. Facilis consequuntur reiciendis exercitationem dicta nobis natus delectus debitis laudantium tenetur iste aliquam, eligendi unde magni rerum quidem! Aperiam necessitatibus deserunt libero. Esse quae voluptatum veritatis vitae cupiditate, magnam ab dolor optio omnis reprehenderit magni possimus voluptate, quasi fugiat voluptatem recusandae natus laboriosam, similique facilis sequi ratione alias porro repellat saepe? Cum laborum fugiat, dolorum illum voluptate consequuntur enim magnam, consequatur inventore dignissimos sapiente magni numquam reiciendis vel explicabo saepe. Odit nihil corrupti consequuntur ea neque eum est. Odit nam porro sapiente illum quod corrupti assumenda amet, accusantium similique sint! Optio iure esse rem, non assumenda, recusandae distinctio expedita, obcaecati quidem molestiae fugiat maxime omnis pariatur? Quas ab molestias nobis dolorem quis quasi quisquam eius voluptatibus fugit reprehenderit officiis esse voluptates, ratione dolor corporis cum eum ducimus in, ut illo corrupti. Adipisci fugiat voluptates quisquam molestiae consequuntur labore corporis eos! Nihil architecto dicta quo esse blanditiis? Reprehenderit non dicta esse dolorum facere vero incidunt odit debitis commodi dolores eum repellat ipsa a sunt minima unde quibusdam doloremque, modi necessitatibus quaerat deserunt laudantium. Alias porro numquam, enim possimus quidem sint earum dignissimos! Incidunt repellendus dolor earum aperiam, unde illum cumque architecto. Repellat quos unde esse, temporibus corrupti qui pariatur exercitationem dolorem dolores dolorum, inventore accusamus vitae veniam, cum veritatis suscipit! Delectus, in iste porro quis, itaque accusamus sunt quibusdam facere iure a voluptatum. Ab consectetur earum vero provident fuga nam nesciunt neque! Eius officia libero unde tempora magnam optio perspiciatis iste, nemo vitae iusto soluta eum commodi numquam aut repellat! Eos placeat voluptatibus unde dolore asperiores quas quidem illo repudiandae ex, quae expedita nesciunt fuga voluptate atque autem aut deserunt soluta qui voluptatem est sapiente, quam laboriosam culpa. Commodi pariatur rerum libero, ducimus voluptate nemo in laboriosam blanditiis praesentium quod deserunt adipisci ea illum fugiat beatae iste reiciendis. Tempore natus illo pariatur ea dolorum autem ut culpa temporibus impedit cum nostrum quasi enim eius facere eos aperiam quae totam nisi, assumenda, vitae earum, officiis beatae sit! Earum architecto quis adipisci eius distinctio voluptate vel deserunt laudantium possimus accusantium minus magni officia esse, qui, quidem nisi amet porro veniam molestiae minima! Tenetur harum repellendus cupiditate! Voluptatum consequatur, distinctio labore odio maiores debitis dolor unde quisquam excepturi autem incidunt velit, et cupiditate deleniti quia voluptatibus! Iste, ratione accusamus? Non excepturi facere maiores animi dolore perspiciatis quibusdam illum quis recusandae cupiditate veniam, similique dolor. Porro illum non quisquam iusto vitae nostrum nisi, amet provident quidem, animi error atque corrupti suscipit vel aliquam ut reprehenderit perspiciatis doloremque voluptas in aut libero distinctio expedita quasi! Consequatur, quos? Ipsam nesciunt laudantium quaerat molestiae omnis, totam quo maxime quos saepe doloremque ut consequuntur natus obcaecati odit quis eligendi voluptate autem quod ab iste eum dolores officiis fugiat inventore! Saepe necessitatibus, tenetur facere consequuntur eveniet molestias vitae fuga! Aliquam quo mollitia, voluptas ipsam dolores officia quos officiis veniam alias earum rem molestiae voluptates, minima id, voluptate exercitationem illo. Omnis fugit iure nostrum temporibus debitis quo ducimus sit et consequatur at id, perferendis nam necessitatibus ipsam architecto numquam voluptas! Reiciendis?
                                </div>

                            </div>
                            <div className="read-more text-center">
                                <NavLink to='/program/module/transcript' className='btn btn-primary'>Read More</NavLink>
                            </div>
                        </div>

                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Module
