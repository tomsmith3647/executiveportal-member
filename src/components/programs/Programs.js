import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import { NavLink } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';



export class Programs extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            baseApiURL: '',
            showLoader: false,            
            showAlertIcon: '',
            showAlert: false,
            showConfirmationAlert: false,
            showLoader:false,
            categories:[],
            reviews:[],
            booms:[],
            refers:[],
            programs:[],
            program_menu:[],
            total_boom:'0.00',
        }
    }


    componentDidMount() {
          //get user detail from memeber id
          const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
          if (memberSession.user_id) 
          {
              this.setState({
                  user_id: memberSession.user_id,
                  email: memberSession.email,
                  full_name: memberSession.full_name,
                  auth_token: memberSession.auth_token,
              })
        
            //get program List
            // this.setState({ showLoader: true });
            // axios({
            //     url: API_URL + "member/program/list",
            //     method: 'get',    
            //     headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }        
            // })
            // .then(response => {
            //     const error_code = response.data.error_code;
            //     const error_message = response.data.error_message;                       
            //     this.setState({ showLoader: false });
            //     if (error_code == '200') {
            //         //success                        
            //         this.setState({ programs: response.data.member_programs });                
            //     }
            //     else {
            //         //fail
            //         //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
            //     }
            // })
            // .catch(err => {
            //     console.log(err);
            // });


            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/list/menu",
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ programs: response.data.programs });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });
        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }
    }


    render() { 
        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };   
        return (
            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                   type={this.state.showAlertIcon}                    
                   title={this.state.showAlertMessage}
                   onConfirm={this.sweetalertok}
                   onCancel={() => this.setState({ showAlert: false })}
                   show={this.state.showAlert}
                />
                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Programs</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>                            
                            <li className="breadcrumb-item active" aria-current="page">Programs</li>
                        </ol>
                    </div>
                </div>
                <br/>
                <div className="row">                     
                    <div className="col-md-12">
                        <div class="row clearfix row-deck">
                        {
                            this.state.programs.map((program, bindex) => (
                                <div class="col-6 col-md-4 col-xl-3">
                                    <div class="card">
                                        <div class="card-body ribbon">
                                            <div class="ribbon-box green" data-toggle="tooltip" title="Programs">0</div>
                                            <a href={`/program/details/${program.program_id}`} className="my_sort_cut text-muted" title="View Program">
                                                <i class="fa fa-book"></i>
                                                <span>{program.program_title}</span>
                                            </a>                                             
                                        </div>
                                    </div>
                                </div>
                            ))
                        }
                            
                              
                              
                             
                        </div>                                                                                                 
                    </div>
                </div>
            </Fragment>

        )
    }
}


export default Programs
