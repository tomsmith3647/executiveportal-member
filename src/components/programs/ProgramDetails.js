import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'

import LoadingOverlay from 'react-loading-overlay';
import { PrismCode } from 'react-prism'
import { Player, ControlBar, BigPlayButton } from 'video-react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
var $ = window.$
class ProgramDetails extends Component {


    constructor(props, context) {
        super(props, context);

        this.state = {
            user_id: '',
            email: '',
            full_name: '',
            showAlert: false,
            showAlertMessage: '',
            memberPermission: [],
            programs: [],
            categories: [],
            modules: [],
            resources: [],
            topics: [],
            items: [],
            program: {},
            category: {},
            module: {},
            item: {},
            program_id: '',
            category_id: '',
            module_id: '',
            resource_id: '',
            topic_id: '',
            transcript: {},
            baseApiURL: '',
            module_id_previous: '',
            module_id_next: '',
            complete_status: '',
            playerSource: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            inputVideoUrl: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            source: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            program_item: {
                member_id: '',
                program_id: '',
                item_id: '',
                complete_status: '0',
                created_by: ''
            },
        };





    }

    componentDidMount() {


        // Fixed left Module after breadcrumb scrolled
        $(window).scroll(function () {

            const scrollTop = $(window).scrollTop();

            if (scrollTop <= 76.8) {
                $(".module-left").css("marginTop", `-${scrollTop}px`);
            } else {
                $(".module-left").css("marginTop", `-76.8px`);
            }

        });

        


        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                memberPermission: memberSession.permissions,
            })

            //get Member Module Details
            this.setState({ showLoader: true });
            const program_id = this.props.match.params.program_id;
            this.setState({ program_id: program_id });
            axios({
                url: API_URL + "member/program/details/" + program_id,
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success 
                        const ApiURL = response.data.api_url;
                        this.setState({ baseApiURL: ApiURL });
                        this.setState({ program: response.data.program, complete_status: response.data.program_complete });
                        //this.load();
                        //Get Module Details
                        if (this.state.program_id) {

                            this.saveMemberHistory();

                            //get Member Module Details
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "member/program/category/list/" + this.state.program_id,
                                method: 'get',
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    //const error_message = response.data.error_message;                       
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ categories: response.data.categories });
                                    }
                                    else {
                                        //fail
                                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        }

                        
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });

                    }
                })
                .catch(err => {
                    console.log(err);
                });
        }

    }


    //saveMemberHistory
     saveMemberHistory = () => {
         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            const postData = {   
                program_id:this.state.program_id,                            
                member_id: memberSession.user_id,                
                url:window.location.pathname,                         
                }
            axios({
                url: API_URL + "member/history",
                method: 'post',
                data: postData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {                        
                    //success                                           
                    //this.setState({ showAlertIcon: 'success', showAlert: false, showAlertMessage: error_message });                        
                }
                else {
                    //fail
                    //this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }         
    }






    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    render() {
        return (
            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Program</h1>
                        <ol className="breadcrumb page-breadcrumb">

                            <li className="breadcrumb-item active" aria-current="page">Program Details</li>
                        </ol>
                    </div>
                </div>
                <br />
                <div className='module-wrapper'>
                    <div className="module-left white-container">
                        <div className="module-category-img">


                            <img src={this.state.program.program_image ? this.state.baseApiURL + this.state.program.program_image : '/assets/images/gallery/1.jpg'} alt="category-img" className='img-fluid' />
                        </div>
                        <div className="module-title">
                            <h3 className='fw-700 black'>
                                {this.state.program.program_title}
                            </h3>
                        </div>
                        <div className="module-category-list">
                            <ul>

                                {this.state.categories.map((cat, mindex) => (
                                    <li>
                                        <a href={`/program/category/details/${cat.category_id}`} className={cat.complete_status == '1' ? 'completed' : 'Active'}>
                                           
                                            {/* <span>{mindex + 1}. </span> */}
                                            <span style={{width: '90%'}}>{cat.category_title}</span>
                                            <i className="fa fa-check-circle moduleLeftNav" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                ))}


                            </ul>
                             
                        </div>


                    </div>
                    <div className="module-right">
                        <div className="white-container">

                            <div className='module-video-header'>
                                <h3 className='module-title'></h3>
                                <div className='module-completed'>
                                    {this.state.complete_status == '1' ? (<button className='btn btn-success' disabled>Program Completed</button>) : ''}
                                </div>
                            </div>
                            <div className='module-video-player'>
                                <div dangerouslySetInnerHTML={{ __html: this.state.program.program_description }} />
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default ProgramDetails
