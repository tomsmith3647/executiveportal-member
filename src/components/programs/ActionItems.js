import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'

import LoadingOverlay from 'react-loading-overlay';
import { PrismCode } from 'react-prism'
import { Player, ControlBar, BigPlayButton } from 'video-react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
var $ = window.$
class CategoryDetails extends Component {


    constructor(props, context) {
        super(props, context);

        this.state = {
            user_id: '',
            email: '',
            full_name: '',
            showAlert: false,
            showAlertMessage: '',
            memberPermission: [],
            programs: [],
            categories: [],
            modules: [],
            resources: [],
            topics: [],
            items: [],
            program: {},
            category: {},
            module: {},
            item: {},
            program_id: '',
            category_id: '',
            module_id: '',
            resource_id: '',
            topic_id: '',
            transcript: {},
            baseApiURL: '',
            module_id_previous: '',
            module_id_next: '',
            complete_status: '',
            playerSource: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            inputVideoUrl: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            source: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            program_item: {
                member_id: '',
                program_id: '',
                item_id: '',
                complete_status: '0',
                created_by: ''
            },
        };





    }

    componentDidMount() {


        // Fixed left Module after breadcrumb scrolled
        $(window).scroll(function () {

            const scrollTop = $(window).scrollTop();

            if (scrollTop <= 76.8) {
                $(".module-left").css("marginTop", `-${scrollTop}px`);
            } else {
                $(".module-left").css("marginTop", `-76.8px`);
            }

        });


        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                memberPermission: memberSession.permissions,
            })

            //get Member Module Details
            this.setState({ showLoader: true });
            const category_id = this.props.match.params.category_id;
            this.setState({ category_id: category_id });
            axios({
                url: API_URL + "member/program/category/details/" + category_id,
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success 
                        const ApiURL = response.data.api_url;
                        this.setState({ baseApiURL: ApiURL });
                        this.setState({ category: response.data.category, complete_status: response.data.category_complete });
                        //this.load();
                        //Get Module Details
                        if (this.state.category_id && this.state.category.program_id) {

                            this.saveMemberHistory();

                            //get Member Module Details
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "member/program/category/module/list/" + this.state.category.program_id + "/" + this.state.category_id,
                                method: 'get',
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    //const error_message = response.data.error_message;                       
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ modules: response.data.modules });
                                    }
                                    else {
                                        //fail
                                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        }

                        //Get Item Lsit
                        if (this.state.category_id && this.state.category.program_id) {
                            //get Member Module Details
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "member/program/category/item/list/" + this.state.category.program_id + '/' + this.state.category_id,
                                method: 'get',
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    //const error_message = response.data.error_message;                       
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ items: response.data.items });
                                    }
                                    else {
                                        //fail
                                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        }


                        const program_id = this.props.match.params.program_id;
                        const category_id = this.props.match.params.category_id;
                        this.setState({ category_id: category_id, program_id:program_id });

                        if (program_id && category_id) {
                            
                            //get Member Module Details
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "member/program/category/item/list/" + program_id + '/' + category_id,
                                method: 'get',
                                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                            })
                            .then(response => {
                                const error_code = response.data.error_code;
                                const error_message = response.data.error_message;                       
                                this.setState({ showLoader: false });
                                if (error_code == '200') {
                                    //success                        
                                    this.setState({ items: response.data.items });
                                    const ApiURL = response.data.api_url;
                                    this.setState({ baseApiURL: ApiURL });
                                }
                                else {
                                    //fail
                                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                }
                            })
                            .catch(err => {
                                console.log(err);
                            });
                        }
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });

                    }
                })
                .catch(err => {
                    console.log(err);
                });

                
        }

    }


    //saveMemberHistory
    saveMemberHistory = () => {
         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            const program_id = this.props.match.params.program_id;
            const postData = {   
                program_id, program_id,                            
                member_id: memberSession.user_id,                
                url:window.location.pathname,                         
                }
            axios({
                url: API_URL + "member/history",
                method: 'post',
                data: postData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {                        
                    //success                                           
                    //this.setState({ showAlertIcon: 'success', showAlert: false, showAlertMessage: error_message });                        
                }
                else {
                    //fail
                    //this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }         
    }


    
    //handleActionItem
     //Member module complete
     handleActionItem = (e) => {
         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })

            const isChecked = e.target.checked ? 1 : 0;
            
            const postData = {   
                ...this.state.program_item,                 
                member_id: this.state.user_id,
                program_id: this.state.program_id,
                category_id: this.state.category_id,
                item_id: e.target.value,        
                complete_status: isChecked,
                created_by: this.state.user_id,                     
                }

           
            if(!postData.item_id)
            {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "invalid member id" });
                return false;
            }
           // this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/item/complete",
                method: 'post',
                data: postData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {                        
                    //success  
                    
                    this.setState({ showLoader: true });
                    axios({
                        url: API_URL + "member/program/category/item/list/" + this.state.program_id + '/' + this.state.category_id,
                        method: 'get',
                        headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                    })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;                       
                        this.setState({ showLoader: false });
                        if (error_code == '200') {
                            //success                        
                            this.setState({ items: response.data.items });
                            const ApiURL = response.data.api_url;
                            this.setState({ baseApiURL: ApiURL });
                        }
                        else {
                            //fail
                            //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        console.log(err);
                    });


                    this.setState({ showAlertIcon: 'success', showAlert: false, showAlertMessage: error_message });                        
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }         
    }






    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    render() {
        return (
            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Action Items</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            {/* <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li> */}
                            <li className="breadcrumb-item"><a href={`/program/details/${this.state.program_id}`}>Program</a></li>
                            <li className="breadcrumb-item"><a href={`/program/category/details/${this.state.category_id}`}>Category</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Action Items</li>
                        </ol>
                    </div>
                </div>
                <br />


                <div className='module-wrapper'>
                    <div className="module-left white-container">
                        <div className="module-category-img">
                            <img src={this.state.category.category_icon ? this.state.baseApiURL + this.state.category.category_icon : '/assets/images/gallery/1.jpg'} alt="category-img" className='img-fluid' />
                        </div>
                        <div className="module-title">
                            <h3 className='fw-700 black' title={this.state.category.category_title}>
                                {this.state.category.category_title}
                            </h3>
                        </div>
                        <div className="module-category-list">
                            <ul>

                                {this.state.modules.map((module, mindex) => (
                                    <li>
                                        <a href={`/program/category/module/details/${module.module_id}`} className={module.complete_status == '1' ? 'completed' : 'Active'}>
                                            
                                            {/* <span>{mindex + 1}. </span> */}
                                            <span style={{width: '90%'}}>{module.module_title}</span>
                                            {module.complete_status == '1' ? (<span className="sidebar-nav-item-icon fa fa-check-circle text-success moduleLeftNav"></span>)
                                                                                    : (<span className="sidebar-nav-item-icon fa fa-check-circle moduleLeftNav"></span>)}
                                        </a>
                                    </li>
                                ))}


                            </ul>
                        </div>

                        <div className="module-action">
                            <div className="module-category-list">
                                <ul style={{margin: '30px 0px 0', paddingBottom: '0' }}>

                                    {/* {this.state.items.map((item, itmindex) => (
                                        <li>
                                            <a href={`/program/category/item/details/${item.item_id}`} className={module.complete_status == '1' ? 'completed' : 'Active'} >
                                                <i className="fa fa-check-circle" aria-hidden="true"></i>
                                                
                                                {item.item_title}
                                            </a>
                                        </li>
                                    ))} */}

                                    <li>
                                        <a className='Active brandColor' href={`/program/category/actionitems/${this.state.category.program_id}/${this.state.category.category_id}`} >Action Items</a>
                                    </li>

                                    <li>
                                        <a href="/faqs">Help</a>
                                    </li>


                                </ul>
                            </div>
                        </div>


                    </div>
                    <div className="module-right">

                        <div className="white-container">

                        <div className="row">
                <div className="col-sm-12">
                    <p className='page-title mb-3'>Action Items </p>
                    <div className="card" style={{ padding: '20px 20px' }}>
                        <form>
                            <div className="form-group row">
                                <div className="table-responsive">
                                    <table className="table table-hover table-vcenter text-nowrap  table-striped  border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Action Item</th>
                                                    <th className='text-center' >View</th>
                                                    <th className='text-center' >Select</th>
                                                    
                                                {/* <th className='text-center'>Write</th>
                                            <th className='text-center'>Update</th> */}
                                                {/* <th className='text-center'>Status</th>                                         */}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <Fragment>

                                            {this.state.items.map((itm, resindex) => (
                                                <tr >
                                                    <td>{resindex+1}</td>
                                                    <td>{itm.item_title}</td>
                                                    <td className='text-center' >                                                        
                                                    <a href={`${"/program/category/item/details/"+itm.item_id}`}  className="text-info brandColor" title="View Detail">
                                                    <i class="fa fa-eye" aria-hidden="true" style={{fontSize: '25px', color:'#000'}}></i>
                                                            </a>
                                                    </td>
                                                    <td className='text-center' >                                                        
                                                        {/* <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                        
                                                            <input type="checkbox" className="custom-control-input check-box" name="item_id" value={itm.item_id} onChange={this.handleActionItem} checked={itm.complete_status == 1 ? true : ''} defaultChecked={itm.complete_status == 1 ? true : ''}
                                                             />               <span className="custom-control-label">&nbsp;</span>
                                                        </label> */}


                                                        <input type="checkbox" id={'cb'+resindex} value={itm.item_id} onChange={this.handleActionItem} checked={itm.complete_status == 1 ? true : ''} defaultChecked={itm.complete_status == 1 ? true : ''} />
                                                        <label for={'cb'+resindex} class="check-box" title="Check to complete"></label> 
                                                        
                                                    </td>
                                                    
                                                </tr>
                                            ))}

                                                {/* <tr >
                                                    <td>1</td>
                                                    <td>Action Item</td>
                                                    <td className='text-center'>
                                                        <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" className="custom-control-input" name='action' value='action' onChange={this.handleAction} />
                                                            <span className="custom-control-label">&nbsp;</span>
                                                        </label>
                                                    </td>

                                                    <td className='text-center'>
                                                        <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" className="custom-control-input" name={memberPer.permission_id + '_w'} value={memberPer.w} onChange={this.handlePermission} defaultChecked={memberPer.w == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                        </label>
                                                    </td>

                                                    <td className='text-center'>
                                                        <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" className="custom-control-input" name={memberPer.permission_id + '_u'} value={memberPer.u} onChange={this.handlePermission} defaultChecked={memberPer.u == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                        </label>
                                                    </td>  


                                                </tr> */}
                                            </Fragment>




                                        </tbody>
                                    </table>
                                </div>
                            </div>



                        </form>
                        {/* <div className="card-footer text-right">
                            <button type="submit" className="btn btn-brand">Update</button>
                        </div> */}

                    </div>
                </div>

            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default CategoryDetails
