import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class NotificationList extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        notifications: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_notification_id: '',
        
        searchQuery:'',
        notification_id:'',
    }

    componentDidMount() {
        this.setState({ showLoader: false });
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            
            const notification_id = this.props.match.params.notification_id;
            if(notification_id)
            {
                this.setState({notification_id:notification_id});
                
                axios({
                    url: API_URL + "member/notification/read",
                    method: 'post',
                    data: { notification_id: notification_id },
                    headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success        
                        this.getNotificationListAction();                                 
                    }
                    else {
                        //fail
                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
            }
            

            //$('#collapse-4').collapse();
            $("#collapse-4").addClass("show");

            this.getNotificationList();


        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }
    }

    getNotificationList = () => {
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        axios({
            url: API_URL + "member/notification/list",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ notifications: response.data.notifications });

                
            }
            else {
                //fail
                this.setState({showAlertIcon: 'warning', showAlert: false, showAlertMessage: error_message, showProgaramTableMessage: 'There are no notifications to review.' });
            }
        })
        .catch(err => {
            console.log(err);
        });
    }

    getNotificationListAction = () => {
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        
        
        axios({
            url: API_URL + "member/notification/list",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ notifications: response.data.notifications });

                
            }
            else {
                //fail
                this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
            }
        })
        .catch(err => {
            console.log(err);
        });
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleChangeQuery = (e) => {
        const query = e.target.value;
        this.setState({searchQuery:query});
    }  

     
    frmSearchQuery = (e) => {
        e.preventDefault();
        
        const searchQuery = this.state.searchQuery;
        if(searchQuery)
        {
            window.location.href = '/notification/search/?query='+searchQuery;
        }
    }


    //notificationRead
       
     notificationRead = (notid) => (e) => {
        
        const nid = notid ? notid : '';
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        var notification_id = e.currentTarget.dataset.id ;
        if(nid)
        {
            notification_id = nid;
        } 

        alert("READ = "+notification_id);
        axios({
            url: API_URL + "member/notification/read",
            method: 'post',
            data: { notification_id: notification_id },
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success        
                    this.getNotificationListAction();                                 
                }
                else {
                    //fail
                    //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
    }


     

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.notificationDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Notifications</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>                            
                            <li className="breadcrumb-item active" aria-current="page">Notifications</li>
                            
                        </ol>
                    </div>
                </div>
                <br />
                <div className="tab-pane" id="Holiday-all">
                    
                    
                    <div className="card">

                    

                        <div className="card-body">

                        <div className="additional-notifications white-container">

                        

                            <div className="accordion" id="accordionExample">
                                {this.state.notifications.map((notf, catindex) => (
                                    <div className="card">
                                        <div className="card-header" style={{background:'#f9f9f9'}} id={'heading-' + catindex}>
                                            <a className="btn btn-link d-flex" data-toggle="collapse" data-target={'#collapse-' + catindex} aria-expanded="false" onClick={this.notificationRead()} data-id={notf.notification_id}>
                                               {
                                                   notf.read_status == '1' ? (notf.notification.notification_title) : (<strong className='brandColor' style={{fontWeight: 'bold'}}>{notf.notification.notification_title}</strong> ) 
                                               }
                                                
                                                <span className="ml-auto mr-1 arrow"></span></a>
                                        </div>
                                        <div id={'collapse-' + catindex} className={this.state.notification_id == notf.notification_id ? "collapse show" : "collapse"} aria-labelledby={'heading-' + catindex} data-parent="#accordionExample">
                                            <div className="card-body row">
                                                
                                                <div className='col'>
                                                    <div dangerouslySetInnerHTML={{ __html: notf.notification.notification_description }} />
                                                     
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                            </div>


                            
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default NotificationList
