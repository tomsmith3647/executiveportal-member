import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class FaqList extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        faqs: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_Faq_id: '',
        categories:[],
        searchQuery:''
    }

    componentDidMount() {
        this.setState({ showLoader: false });
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "member/faq/category/list",
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ categories: response.data.faq_categories });
 
                        
                    }
                    else {
                        //fail
                        this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleChangeQuery = (e) => {
        const query = e.target.value;
        this.setState({searchQuery:query});
    }  

     
    frmSearchQuery = (e) => {
        e.preventDefault();
        
        const searchQuery = this.state.searchQuery;
        if(searchQuery)
        {
            window.location.href = '/faq/search/?query='+searchQuery;
        }
    }


     

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.FaqDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Help Center</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>                            
                            <li className="breadcrumb-item active" aria-current="page">Help Center</li>
                            
                        </ol>
                    </div>
                </div>
                <br />
                <div className="tab-pane" id="Holiday-all">
                    
                    <div className="card"> <br/>
                    <h3 className='additional-faqs-title text-center text-capitalize'>How can we help you?</h3>
                        <div className="card-header pb-0">
                            
                        </div>
                        <div className="card-body">
                            <div className="form-group row">
                                <div className="col-md-12">

                                <div class="left">
                                    <form className='form-horizontal' method="post" onSubmit={this.frmSearchQuery}>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="What you want to find" value={this.state.searchQuery} name='query'  onChange={this.handleChangeQuery}   required />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary btn-search" type="submit">Search</button>
                                        </div>
                                        <a href="javascript:void(0)" class="d-sm-block d-md-none nav-link icon menu_toggle"><i class="fe fe-align-center"></i></a>
                                    </div>
                                    </form>
                                 

                                </div>

                                     
                                </div>
                            </div>                                
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-body">

                        <div className="additional-faqs white-container">

                        {/* {
                                            this.state.faqs.map((faq, index) => (
                                                <tr>
                                                    <td className='reorder text-left'>{index + 1}</td>
                                                    <td className="text-center text-capitalize">
                                                        <NavLink data-id={faq.faq_id} to={`/faq/details/${faq.faq_id}`} className="btn btn-icon btn-sm text-info brandColor" title="View Detail">
                                                            {faq.faq_id}
                                                        </NavLink>
                                                    </td>
                                                    <td className="text-left text-capitalize">
                                                        {faq.faq_title}
                                                    </td>
                                                    <td className='text-center'>
                                                        {faq.faq_status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Inactive</span>)}
                                                    </td>
                                                     
                                                    <td className='text-center'>{(new Date(faq.created_date)).toLocaleDateString('en-US', DATE_OPTIONS)}</td>
                                                    <td className='text-center'>{(new Date(faq.modified_date)).toLocaleDateString('en-US', DATE_OPTIONS)}</td>

                                                    
                                                     
                                                </tr>
                                            ))
                                        } */}


                            

                            <div className="accordion" id="accordionExample">
                                {this.state.categories.map((cat, catindex) => (
                                    <div className="card">
                                        <div className="card-header" style={{background:'#f9f9f9'}} id={'heading-' + catindex}>
                                            <a className="btn btn-link d-flex" data-toggle="collapse" data-target={'#collapse-' + catindex} aria-expanded="false">
                                               <strong>{cat.category_name}</strong> <strong> {'- ('+cat.faq_count+')'}</strong>
                                                
                                                <span className="ml-auto mr-1 arrow"></span></a>
                                        </div>
                                        <div id={'collapse-' + catindex} className="collapse" aria-labelledby={'heading-' + catindex} data-parent="#accordionExample">
                                            <div className="card-body row">
                                                
                                                <div className='col'>
                                                    <div dangerouslySetInnerHTML={{ __html: cat.category_description }} />
                                                    {cat.faq_list.map((faq, catindex) => (
                                                        <p><a target="_blank" href={'faq/details/'+faq.faq_id}>{faq.faq_title}</a></p>
                                                    ))} 
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                            </div>


                            
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default FaqList
