import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';

const $ = window.$
class EmailSupport extends Component {

    state = {
        name: '',
        notes: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        program: {
            program_description: '',
            program_title: ''
        },
        categories: [],
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_profile_image: '',
        selected_profile_icon:'',
        processBarPercent: 0,
        fileName: 'Choose file',
        email_message:'',
        selected_file:'',
        email_to:'',
        email_subject:'',
        email_message:'',
        fileUploading:false,
        instruction_id:'10101010',
        instruction:{},
  
    }

    componentDidMount() {


        this.setState({
            instruction: {
                ...this.state.instruction,
                description: ''
            }
        })
 

        $('.dropify').dropify();
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            this.getInstructionDetails();
        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }
    }


    handleUloadAttachment = (event) => {
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
 
                const file = event.target.files[0];
                
                this.setState({ fileName: event.target.files[0].name });
                if(file){
                    this.setState({
                        selected_file: file,
                    })
                }
             
        }
    }

    getInstructionDetails = () => {

        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));                                   
        if(this.state.instruction_id)
        {
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/instruction/details/" + this.state.instruction_id,
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                           
                    this.setState({ instruction: response.data.instruction, showAlertActionURL: ''});
                }
                else {
                    //fail
                    this.setState({ showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });            
        }         
    }


    //handleEmailSupport  
    handleEmailSupport = (event) => {
        event.preventDefault();
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })

              
            

            const formData = new FormData()

            formData.append('email_to', this.state.email_to);
            formData.append('email_subject', this.state.email_subject);
            formData.append('email_message', this.state.email_message);
            
            if(this.state.selected_file)
            {
                formData.append('attachment', this.state.selected_file);
            }
            


            var A = this;

            this.setState({fileUploading:true});
            axios({
                url: API_URL + "member/email/support",
                method: 'post',
                data: formData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token },
                onUploadProgress: function (progressEvent) {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                    A.setState({ processBarPercent: percentCompleted })
                    console.log(percentCompleted)
                }
            })
            .then(response => {

                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({fileUploading:false});
                if (error_code == '200') {
                    //success   
                     
                    this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message});
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }
        //Get user permissions

    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
        else
        {
            window.location.reload();
        }
    }

    handleChangeEmail = (e) => {
        this.setState({ email_to: e.target.value});
    }

    handleChangeSubject = (e) => {
        this.setState({ email_subject: e.target.value});
    }

    handleSummerNote = (e) => {
        this.setState({email_message: e})
    }


 

    render() {
        const { crop, croppedImageUrl, src } = this.state;
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Email Support</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    
                                    <li className="breadcrumb-item active" aria-current="page">Email Support</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">

                <div className="col-md-12">
                
                    <h3 className='additional-faqs-title text-center text-capitalize'>We're here to help!</h3> <br/><br/>
                </div>

                <div className="col-xl-4 col-md-12">
                        <div className="card">
                             
                            <div className="card-body">
                                <div dangerouslySetInnerHTML={{ __html: this.state.instruction.description }} />
                                 {/* Use this form to email our Support team. <br/>
                                 The fastest way to get help is to post in the <a target='_blank' href='http://www.facebook.com/groups/506241683458656'>Facebook Group</a> or <a href='/faqs'>Help Center</a>, but if those didn't work for you, you can email support here.
                                 <br/><br/>
                                 Typical response times are within 1 business day. */}
                            </div>
                        </div>


                        


                    </div>


                    <div className="col-xl-7 col-md-12">
                        <form className='form-horizontal' method="post" onSubmit={this.handleEmailSupport}>
                             

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Subject</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.email_subject} name='email_subject' onChange={this.handleChangeSubject} required />
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Message</h3>
                                </div>
                                <div className="card-body">





                                    <ReactSummernote
                                        value=""
                                        required
                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}
                                    />
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Attachment  </h3>
                                </div>
                                <div className="card-body">
                                     
                                <div class="input-group">

                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01" name="video"
                                        aria-describedby="inputGroupFileAddon01" onChange={this.handleUloadAttachment} />
                                    <label class="custom-file-label" for="inputGroupFile01">{this.state.fileName}</label>
                                

                                {this.state.processBarPercent > 0 ?
                                        (
                                            <div className="my-3">
                                                <div className="text-center">{this.state.processBarPercent}%</div>
                                                <Progress value={this.state.processBarPercent} />
                                            </div>
                                        ) : ''}

                                </div>
                                </div>
                                     
                                </div>
                            </div>

                             


                           

                            <div className="card-footer text-right">
                                <button type="submit" className="btn btn-brand">Send</button>
                            </div>
                        </form>


                    </div>



                    
                </div>
            </Fragment>

        )
    }
}

export default EmailSupport
