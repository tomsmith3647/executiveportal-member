import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import queryString from 'query-string'
const $ = window.$
export class FaqSearch extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        faqs: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_Faq_id: '',
        categories:[],
        searchQuery:'',
        faq_list:[],
    }

    componentDidMount() {
        this.setState({ showLoader: false });
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })

            const value= queryString.parse(this.props.location.search);
            const query= value.query;
            
            this.setState({searchQuery:query});
            
            if(query){
            this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "member/faq/search/"+query,
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ faq_list: response.data.faq_list });
 
                        
                    }
                    else {
                        //fail
                        this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            }


        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleChangeQuery = (e) => {
        const query = e.target.value;
        this.setState({searchQuery:query});
    }  

     
    frmSearchQuery = (e) => {
        e.preventDefault();
        
        const searchQuery = this.state.searchQuery;
        if(searchQuery)
        {
            window.location.href = '/faq/search/?query='+searchQuery;
        }
    }

     

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.FaqDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Topic Search</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                            
                            <li className="breadcrumb-item"><NavLink to="/faqs">Help Center</NavLink></li>
                            <li className="breadcrumb-item active" aria-current="page">Topic Search</li>
                        </ol>
                    </div>
                </div>
                <br />
                <div className="tab-pane" id="Holiday-all">
                    
                    <div className="card"> <br/>
                    <h3 className='additional-faqs-title text-center text-capitalize'>How can we help you?</h3>
                        <div className="card-header pb-0">
                            
                        </div>
                        <div className="card-body">
                            <div className="form-group row">
                                <div className="col-md-12">

                                <div class="left">
                                    <form className='form-horizontal' method="post" onSubmit={this.frmSearchQuery}>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="What you want to find" value={this.state.searchQuery} name='query'  onChange={this.handleChangeQuery}   required />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary btn-search" type="submit">Search</button>
                                        </div>
                                        <a href="javascript:void(0)" class="d-sm-block d-md-none nav-link icon menu_toggle"><i class="fe fe-align-center"></i></a>
                                    </div>
                                    </form>
                                 

                                </div>

                                     
                                </div>
                            </div>                                
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-body">

                         

                         


                            

                           
                          
                                    <div className="card">
                                         
                                        
                                            <div className="card-body row">
                                                
                                                <div className='col'>
                                                    <h5>Search Results ({this.state.faq_list.length}):</h5>
                                                    {this.state.faq_list.map((faq, catindex) => (
                                                        <p className="text-capitalize"> - <a target="_blank" href={'/faq/details/'+faq.faq_id}>{faq.faq_title} ({faq.category_name})</a></p>
                                                    ))} 
                                                </div>

                                            </div>
                                         
                                    </div>
                               
                            

                            
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default FaqSearch
