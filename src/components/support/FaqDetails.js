import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class FaqDetails extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        faqs: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_Faq_id: '',
        categories:[],
        faq:{},
    }

    componentDidMount() {
        this.setState({ showLoader: false });
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })
            this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });

            const faq_id = this.props.match.params.faq_id;
            this.setState({ faq_id: faq_id });

            axios({
                url: API_URL + "member/faq/details/"+faq_id,
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ faq: response.data.faq });
 
                        
                    }
                    else {
                        //fail
                        this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleChangeQuery = () => {

    }


     

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.FaqDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Topic Details</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                            <li className="breadcrumb-item"><NavLink to="/faqs">Help Center</NavLink></li>
                            <li className="breadcrumb-item active" aria-current="page">Topic Details</li>
                        </ol>
                    </div>
                </div>
                <br />
                <div className="tab-pane" id="Holiday-all">
                    
                     
                    <div className="card">
                        <div className="card-body">                                                                                                                                      
                            <div className="card">                                                                                                                                                                 
                                <div className="card-body row">                                                
                                    <div className='col'>
                                        <h3 className='text-left'>{this.state.faq.faq_title}</h3> 
                                        <p className='text-left'>{this.state.faq.category_name}</p> 
                                        
                                        <div dangerouslySetInnerHTML={{ __html: this.state.faq.faq_description }} />                                                     
                                        
                                    </div>
                                </div>                                         
                            </div>                                                       
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default FaqDetails
