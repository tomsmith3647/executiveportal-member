import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import IframeComponent from '../support/IframeComponent'
const $ = window.$
export class LinkIframe extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        faqs: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_Faq_id: '',
        links:[],
        searchQuery:'',
        name: 'React',
        link:{},
    }

    componentDidMount() {
            this.setState({ showLoader: false });
            const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
            if (memberSession.user_id) {
                this.setState({
                    user_id: memberSession.user_id,
                    email: memberSession.email,
                    full_name: memberSession.full_name,
                    auth_token: memberSession.auth_token,
                })
                this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
                
                const link_id = this.props.match.params.link_id;
            this.setState({ link_id: link_id });
            this.setState({ link: { link_id: link_id } });

            if(link_id)
            {
                this.setState({ showLoader: true });
                axios({
                    url: API_URL + "member/link/details/" + link_id,
                    method: 'get',
                    headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ link: response.data.link});
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            }

            }
            else {
                window.localStorage.removeItem('memberSession');
                window.location.href = '/auth/signin'
            }
    }
 


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleChangeQuery = (e) => {
        const query = e.target.value;
        this.setState({searchQuery:query});
    }  

     
    frmSearchQuery = (e) => {
        e.preventDefault();
        
        const searchQuery = this.state.searchQuery;
        if(searchQuery)
        {
            window.location.href = '/faq/search/?query='+searchQuery;
        }
    }


 handleCheckWindow =(e)=> {
        const url = e.currentTarget.dataset.id ;
        
        window.open(url, "myWindow", 'width=800,height=600');
        e.preventDefault();
     }

     

     render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Link Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>  
                                    <li className="breadcrumb-item"><NavLink to="/links">Links</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Link Details</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-12 col-md-12">                   
                        <div className="card">                        
                            <div className="card-body">  
                            {/* <iframe style={{width:'100%', minHeight:'580px'}} src={this.state.link.link_url} sandbox>
                                    <p>Your browser does not support iframes.</p>
                                </iframe>  */}
                                <IframeComponent src={this.state.link.link_url} height="1100px" width="100%"/> 
                            </div>
                        </div>                            
                    </div>            
                </div>
            </Fragment>

        )
    }
}

export default LinkIframe