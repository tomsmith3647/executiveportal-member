import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class LinkList extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        faqs: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_Faq_id: '',
        links:[],
        searchQuery:''
    }

    componentDidMount() {
            this.setState({ showLoader: false });
            const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
            if (memberSession.user_id) {
                this.setState({
                    user_id: memberSession.user_id,
                    email: memberSession.email,
                    full_name: memberSession.full_name,
                    auth_token: memberSession.auth_token,
                })
                this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
                
                axios({
                    url: API_URL + "member/link/list/all",
                    method: 'get',
                    headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ links: response.data.links }); 
                        var linkTable = $('#usersTable').DataTable({
                                'rowReorder': true,
                                'columnDefs': [{
                                    'targets': [2], // column index (start from 0)
                                    'orderable': false, // set orderable false for selected columns
                                }]
                            });      
                            linkTable.columns().iterator('column', function (ctx, idx) {
                                $(linkTable.column(idx).header()).append('<span class="sort-icon"/>');
                            });                                                
                    }
                    else{
                        //success                        
                        this.setState({ links: [] });                                                       
                    }
                    
                })
                .catch(err => {
                    console.log(err);
                });

            }
            else {
                window.localStorage.removeItem('memberSession');
                window.location.href = '/auth/signin'
            }
    }

    getMoreLinks = () => {
         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        
        axios({
            url: API_URL + "member/link/list/all",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ links: response.data.links }); 
                                                    
            }
            else{
                //success                        
                this.setState({ links: [] });                                                       
            }            
        })
        .catch(err => {
            console.log(err);
        });        
    }



    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleChangeQuery = (e) => {
        const query = e.target.value;
        this.setState({searchQuery:query});
    }  

     
    frmSearchQuery = (e) => {
        e.preventDefault();
        
        const searchQuery = this.state.searchQuery;
        if(searchQuery)
        {
            window.location.href = '/faq/search/?query='+searchQuery;
        }
    }


 handleCheckWindow =(e)=> {
        const url = e.currentTarget.dataset.id ;
        
        window.open(url, "myWindow", 'width=800,height=600');
        e.preventDefault();
     }

     

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.FaqDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Links</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                            <li className="breadcrumb-item"><NavLink to="/links">Links</NavLink></li>
                            
                        </ol>
                    </div>
                </div>
                <br />
                <div className="tab-pane" id="Holiday-all">
                    
                    <div className="card">  
                         
                        <div className="card-body">
                            <div className="table-responsive">
                                <table id='usersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                    <thead>
                                        <tr>
                                            <th >#</th>
                                             
                                            <th className='text-left' >Title </th>
                                            <th className='text-left' >Description </th>
                                            
                                             

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.links.map((link, index) => (
                                                <tr>
                                                    <td className='reorder text-left'>{index + 1}</td>
                                                    {/* <td className="text-left text-capitalize m-0 p-0">
                                                        <NavLink data-id={link.link_id} to={`/link/details/${link.link_id}`} className="btn btn-icon btn-sm text-info brandColor" title="View Detail">
                                                        {link.link_title}
                                                        </NavLink>
                                                        
                                                    </td> */}

                                                    <td className="text-left text-capitalize m-0 p-0">

                                                    {link.open_type == 0 ? (                                                       
                                                            <a  href={`${link.link_url}`} target="_blank" className="brandColor m-0 p-0" title="Details">{link.link_title}</a>
                                                        ) : (
                                                            <a  href={`${"/link/open/"+link.link_id}`} className="brandColor m-0 p-0" title="Details">{link.link_title}</a>
                                                        )
                                                        }
                                                        
                                                        

                                                        {/* <span title="View Detail" className="btn btn-icon btn-sm text-info brandColor" onClick={this.handleCheckWindow.bind(this)} data-id={link.link_url}>{link.link_title}</span> */}
                                                        
                                                    </td>
                                                     <td className="text-left text-capitalize">
                                                         <div dangerouslySetInnerHTML={{ __html: link.link_description }} />
                                                    </td> 
                                                     
                                                </tr>
                                            ))
                                        }

                                    </tbody>
                                     
                                    {/* {this.state.links.length >= 1 && this.state.links.length <= 5 && (<tfoot><tr><td colspan="8" className="text-center text-capitalize">
                                        <a href="javascript:void(0);"  className="view-more"   title="View More Links"  onClick={this.getMoreLinks}>
                                            View More
                                        </a>
                                    </td></tr></tfoot>)} */}
                                    {this.state.links.length == 0 && (<tfoot><tr><td colspan="8" className="text-center text-capitalize">{this.state.showProgaramTableMessage ? this.state.showProgaramTableMessage : 'No links'}</td></tr></tfoot>)}
                                </table>
                            </div>

                            
                        </div>
                    </div>                  
                </div>
                
            </Fragment>
        )
    }
}

export default LinkList
