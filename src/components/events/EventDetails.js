import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

// import DropdownTreeSelect from 'react-dropdown-tree-select';
// import 'react-dropdown-tree-select/dist/styles.css';

import Container from './DropdownContainer';


import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';

import reactCSS from 'reactcss'
import {SketchPicker, PhotoshopPicker, ChromePicker} from 'react-color';

const $ = window.$
class EventDetails extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        event: {
            event_description: '',
            event_title: '',
            event_video: ''

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
        role_list:[],
        selectedMember:[],
        selected_members : [],
        startDate: new Date(),
        endDate: new Date(),
        currentDate: new Date(),
        selectedDate: '',
        pgcolor:'#F1CA29',
        pgcolorrgb:{},
        displayColorPicker:false,
        event_color:'#F1CA29',
        categories:[],
        event:{},
        event_members:[],
        selected_date:'',
        complete_status:0,
        
    }

    componentDidMount() {

        const rgbcolor = this.convertHex(this.state.pgcolor);
        this.setState({pgcolorrgb: rgbcolor});

        $('.dropify').dropify();
        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            const selected_date = this.props.match.params.date;
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
                selected_date:selected_date,
            })
            //const selectedDate = this.props.match.params.date;
            //this.setState({startDate:new Date(selectedDate), endDate:new Date(selectedDate)});
             
            
            this.getRoleMembers();
            this.getEventCategories();
            this.getEventDetails();
        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }
    }

    getEventDetails = () => {
         
        
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        const event_id = this.props.match.params.event_id;
        axios({
            url: API_URL + "member/event/details/"+event_id,
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ event: response.data.event }); 
                    this.setState({ event_members: response.data.event.event_members }); 
                                                            
                }
                else {
                    this.setState({ showLoader: false, showProgaramTableMessage: error_message });                    
                }
            })
            .catch(err => {
                console.log(err);
            });       
    }

    getEventCategories = () => {
         
        
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        
        axios({
            url: API_URL + "member/event/category/list",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ categories: response.data.categories });                                         
                }
                else {

                    this.setState({ showLoader: false, showProgaramTableMessage: error_message });
                    //fail
                    //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });       
    }

    convertHex = (hex,opacity=100) => {
        hex = hex.replace('#','');
        const r = parseInt(hex.substring(0,2), 16);
        const g = parseInt(hex.substring(2,4), 16);
        const b = parseInt(hex.substring(4,6), 16);
    
        //const result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
        const a = opacity/100;
        const result = {r,g,b,a}
        return result;
    }

    getRoleMembers = () => {         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });        
        axios({
            url: API_URL + "member/rolemember/list",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ role_list: response.data.role_list });                                                     
            }
            else{
                //success                        
                this.setState({ role_list: [] });                                                       
            }            
        })
        .catch(err => {
            console.log(err);
        });        
    }
 

    handleChangeMembers = (currentNode, selectedNodes) => {
        this.setState
        ({
            selectedMember: selectedNodes
        });                          
    }
         
    removeSelectedMember = (myObjects,prop,valu) =>
    {
         return myObjects.filter(function (val) {
          return val[prop] !== valu;
      });
    }

    handleChange = (e) => {
        this.setState({ event: { ...this.state.event, event_title: e.target.value } });
    }
 
    handleSummerNote = (e) => {
        this.setState({
            event: {
                ...this.state.event,
                event_description: e
            }
        })
    }

    dateFormatBackend = (selectedDate) => {
        var dt = new Date(selectedDate) ;

       return `${
            dt.getFullYear().toString().padStart(4, '0')}-${
            (dt.getMonth()+1).toString().padStart(2, '0')}-${
            dt.getDate().toString().padStart(2, '0')} ${
            dt.getHours().toString().padStart(2, '0')}:${
            dt.getMinutes().toString().padStart(2, '0')}:${
            dt.getSeconds().toString().padStart(2, '0')}`;
    }
     


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

 

    convertUTCDateToLocalDate =(date)=> {
        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }


    memberEventCompleteMark=()=>{
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })

            const postData = {                
                member_id: this.state.user_id,
                event_id: this.state.event.event_id,                 
                complete_status: '1',                
            };



            if (!postData.member_id) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "invalid member id" });
                return false;
            }
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/event/complete",
                method: 'post',
                data: postData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                             
                        this.getEventDetails();                                                                
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
    }

    memberEventCompleteUnmark=()=>{
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
            })

            const postData = {                
                member_id: memberSession.user_id,
                event_id: this.state.event.event_id,                 
                complete_status: '0',                
            };



            if (!postData.member_id) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "invalid member id" });
                return false;
            }
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/event/complete",
                method: 'post',
                data: postData,
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                             
                        this.getEventDetails();                                                                
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
    }
      
                
    render() {
        
        const styles = reactCSS({
            'default': {
              color: {
                width: '36px',
                height: '14px',
                borderRadius: '2px',                
                background:`${this.state.event.color}`, 
              },
              swatch: {
                padding: '5px',
                background: '#fff',
                borderRadius: '1px',
                boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
                display: 'inline-block',
                cursor: 'pointer',
              },
              popover: {
                position: 'absolute',
                zIndex: '2',
              },
              cover: {
                position: 'fixed',
                top: '0px',
                right: '0px',
                bottom: '0px',
                left: '0px',
              },
            },
          });

          const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Event Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li> 
                                    {this.state.selected_date ?
                                    (<li className="breadcrumb-item"><NavLink to={"/event/list/"+this.state.selected_date}>Event List</NavLink></li>) : ''
                                    }
                                    <li className="breadcrumb-item active" aria-current="page">Event Details</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-xl-6 col-md-12">
                        <form className='form-horizontal' onSubmit={this.EventDetailsBtn}>
                         

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Category</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <span className="form-control">
                                                {this.state.event.category_name}
                                                &nbsp;
                                            </span>   
                                        </div>
                                    </div>                                
                                </div>
                            </div>    
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Title</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <span className="form-control">
                                                {this.state.event.title}
                                                &nbsp;
                                            </span>                                            
                                        </div>
                                    </div>                                
                                </div>
                            </div>    

                            <div className="card">
                                
                                <div className="row">
                                <div className="col-md-6">
                                <div className="card-header pb-0">
        <h3 className="card-title">Start Date {this.state.selectedDate}</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <span className="form-control">
                                                {this.state.event.start_date}
                                                &nbsp;
                                            </span>                                             
                                        </div>
                                    </div>                                
                                </div>
                                </div>
                                <div className="col-md-6">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">End Date</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <span className="form-control">
                                                {this.state.event.end_date}
                                                &nbsp;
                                            </span>                                             
                                        </div>
                                    </div>                                
                                </div>
                                </div>
                                </div>

                                
                            </div>    

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <span className="form-control">
                                        
                                        <div dangerouslySetInnerHTML={{ __html: this.state.event.description}} />
                                        
                                        &nbsp;
                                    </span>                                
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Color </h3>
                                </div>
                                <div className="card-body">
                                    <span className="form-control text-center">                                        
                                        <div style={ styles.color } />
                                    </span>                                
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Repeat </h3>
                                </div>
                                <div className="card-body">
                                    <span className="form-control text-center">                                        
                                    {this.state.event.recurring ? this.state.event.recurring_value : 'No' }
                                    </span>                                
                                </div>
                            </div>
 
                        </form>

                         


                    </div>

                    <div className="col-xl-6 col-md-12">
                        <div className='float-right'>
                                
                                {this.state.event_members.complete_status == '0' ? (<button className='btn btn-brand' title="Mark Complete"  onClick={this.memberEventCompleteMark}>Mark Complete</button>) : ''}                                                                       

                                {this.state.event_members.complete_status == '1' ? (<button className='btn btn-brandColor btn-fixed' title="Mark Incomplete"  onClick={this.memberEventCompleteUnmark}>Completed</button>) : ''}                                                                       
                        </div>
                    </div>
                     

                </div>
            </Fragment>

        )
    }
}

export default EventDetails
