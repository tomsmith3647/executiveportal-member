import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import { plus, getUserDetails } from '../../config/functions'
// Variable
const $ = window.$

export class Header extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        showAlert: false,
        showAlertMessage: '',
        memberPermission: [],
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showLoader: false,
        amount: '0.00',
        baseApiURL: '',
        member: '',
        confirm_amount: '0',
        notifications:[],
        notificationbadge:{},
        notification_id:'',
    }

    componentDidMount() {

        // $(document).bind("contextmenu",function(e){
        //     return false;
        // });

        // const notification_id = this.props.match.params.notification_id;            
        // this.setState({notification_id:notification_id});

        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));

        const resp = getUserDetails(memberSession.user_id);
        if (resp) {
            this.setState({ showAlert: true, showAlertMessage: resp.error_message });
        }


        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                memberPermission: memberSession.permissions,
            })

            axios({
                url: API_URL + "member/detail/" + memberSession.user_id,
                method: 'get',
                data: {},
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    if (error_code == '200') {
                        this.setState({ member: response.data.member });
                    }
                    else if (error_code == '301') {
                        //auth expired
                        this.handleLogout();

                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });

                this.getNotificationsGobal();
                this.getNotificationsBadge();

        }
        else {
            this.handleLogout();
        }
    }


    getNotificationsGobal = () => {
         
         
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));                 
        axios({
            url: API_URL + "member/notification/list",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ notifications: response.data.notifications });                
            }
            else {
                //fail
                this.setState({showAlertIcon: 'warning', showAlert: false, showAlertMessage: error_message, showProgaramTableMessage: error_message });
            }

            
            if(window.location.pathname == '/notifications')
            {
                this.setState({ notifications:[]});
            }

            if(window.location.pathname == '/notifications/'+this.state.notification_id)
            {
                this.setState({ notifications:[]});
            }
            
        })
        .catch(err => {
            console.log(err);
        });
         

       
    }


    getNotificationsBadge = () => {                  
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));                 
        axios({
            url: API_URL + "member/notification/badge",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ notificationbadge: response.data.notificationbadge });                
            }
            else {
                //fail
                this.setState({showAlertIcon: 'warning', showAlert: false, showAlertMessage: error_message, showProgaramTableMessage: error_message });
            }


            var url = window.location.pathname;
            var parts = url.split('/');

            

            if(parts[1] == 'notifications')
            {
                this.setState({ notificationbadge:{}});
            }
        })
        .catch(err => {
            console.log(err);
        }); 
    }



    handleLogout = () => {

        //this.setState({showAlert:true,showAlertMessage:'Logout Successfully'});

        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        console.log(memberSession);
        if (memberSession.user_id) {

            axios({
                url: API_URL + "member/logout",
                method: 'post',
                data: {},
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    window.localStorage.removeItem('memberSession');
                    window.location.href = '/auth/signin'
                    if (error_code == '200') {
                        //success                        

                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
    }



    handleBoomChange = (e) => {
        this.setState({ amount: e.target.value })
    }

    //handleConfirmAmountCheck
    handleConfirmAmountCheck = (e) => {
        const isChecked = e.target.checked ? 1 : 0;
        this.setState({ confirm_amount: isChecked });
    }

    BoomPostBtn = (e) => {
        e.preventDefault()
        const userSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (userSession.user_id) {

            var ex = /^[0-9]+\.?[0-9]*$/;
            if (ex.test(this.state.amount) == false) {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please enter valid amount', showAlertActionURL: '' });
                return false;
            }

            if (this.state.amount < 1) {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please enter valid amount', showAlertActionURL: '' });
                return false;
            }

            if (this.state.confirm_amount != '1') {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please confirm your enter amount', showAlertActionURL: '' });
                return false;
            }


            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/boom/post",
                method: 'post',
                data: {
                    amount: this.state.amount,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: 'refresh' });

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });

        }
        //Get user permissions

    }

    sweetalertok = () => {
        this.setState({ showAlert: false });


        if (this.state.showAlertActionURL == 'refresh') {
            window.location.reload();
        } else if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    checkValidAmount = (evt) => {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            evt.preventDefault();
            return false;
        }
        else {
            return true;
        }
    }

    boomForm = () => {
        this.textInput.focus();
    }


    handleToggle = () => {
        $('body').toggleClass('offcanvas-active');
    }



    render() {


        const getPermissionOject = (value) => {
            const permissiondata = this.state.memberPermission.filter(function (e) {
                return e.permission_id == value;
            });

            if (permissiondata) {
                return permissiondata[0];
            }
        }



        //boom - 38080107
        const boomdata = getPermissionOject('38080107');
        var boom_r = 0;
        var boom_w = 0;
        var boom_u = 0;
        if (boomdata) {
            boom_r = boomdata.r;
            boom_w = boomdata.w;
            boom_u = boomdata.u;

        }

        //Love - 22334025
        const lovedata = getPermissionOject('22334025');
        var love_r = 0;
        var love_w = 0;
        var love_u = 0;
        if (lovedata) {
            love_r = lovedata.r;
            love_w = lovedata.w;
            love_u = lovedata.u;
        }

        //Refer A Friend -35982552
        const referAFriendData = getPermissionOject('35982552');
        var referAFriend_r = 0;
        var referAFriend_w = 0;
        var referAFriend_u = 0;
        if (referAFriendData) {
            referAFriend_r = referAFriendData.r;
            referAFriend_w = referAFriendData.w;
            referAFriend_u = referAFriendData.u;
        }

        //Notification - 70459612
        const notificationData = getPermissionOject('70459612');
        var notification_r = 0;
        var notification_w = 0;
        var notification_u = 0;
        if (referAFriendData) {
            notification_r = notificationData.r;
            notification_w = notificationData.w;
            notification_u = notificationData.u;
        }

        //User Dashboard // 55640434
        // const userDashboardData = getPermissionOject('55640434');
        var userdashboard_r = 1;
        var userdashboard_w = 1;
        var userdashboard_u = 1;
        // if (referAFriendData) {
        //     userdashboard_r = userDashboardData.r;
        //     userdashboard_w = userDashboardData.w;
        //     userdashboard_u = userDashboardData.u;
        // }


        //User Advanced onboarding // 27534076 
        const advancedOnboardingData = getPermissionOject('27534076');
        var advancedOnboarding_r = 0;
        var advancedOnboarding_w = 0;
        var advancedOnboarding_u = 0;
        if (advancedOnboardingData) {
            advancedOnboarding_r = advancedOnboardingData.r;
            advancedOnboarding_w = advancedOnboardingData.w;
            advancedOnboarding_u = advancedOnboardingData.u;
        }

        if (!userdashboard_r) {
            //window.location.href="/profile";
        }


        //Program // 17345795
        const programData = "";
        //const programData = getPermissionOject('17345795');
        var program_r = 1;
        var program_w = 1;
        var program_u = 1;
        if (programData) {
            program_r = programData.r;
            program_w = programData.w;
            program_u = programData.u;
        }

        //Link // 80555725
        const linkData = getPermissionOject('80555725');
        var link_r = 0;
        var link_w = 0;
        var link_u = 0;
        if (linkData) {
            link_r = linkData.r;
            link_w = linkData.w;
            link_u = linkData.u;
        }

        //Support // 56519713
        const supportData = getPermissionOject('56519713');
        var support_r = 0;
        var support_w = 0;
        var support_u = 0;
        if (supportData) {
            support_r = supportData.r;
            support_w = supportData.w;
            support_u = supportData.u;
        }

        //FAQ // 68389135
        const faqData = getPermissionOject('68389135');
        var faq_r = 0;
        var faq_w = 0;
        var faq_u = 0;
        if (faqData) {
            faq_r = faqData.r;
            faq_w = faqData.w;
            faq_u = faqData.u;
        }

        //Email Support // 32508229
        const emailSupportData = getPermissionOject('32508229');
        var emailSupport_r = 0;
        var emailSupport_w = 0;
        var emailSupport_u = 0;
        if (referAFriendData) {
            emailSupport_r = emailSupportData.r;
            emailSupport_w = emailSupportData.w;
            emailSupport_u = emailSupportData.u;
        }













        return (
            // Start Page header
            <Fragment>


                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />


                <div className="page-header">

                    <div className="left">
                        <div className="input-group">
                            {/* <a href="javascript:void(0)" className="d-none d-md-flex nav-link icon menu_toggle" onClick={this.handleToggle}><i className="fe fe-align-center"></i></a> */}
                            <input type="text" className="form-control" placeholder="What you want to find" />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary btn-search" type="button">Search</button>
                            </div>
                            <a href="javascript:void(0)" className="d-sm-block d-md-none nav-link icon menu_toggle" onClick={this.handleToggle}><i className="fe fe-align-center"></i></a>
                        </div>
                    </div>
                    <div className="right">
                        <div className="notification d-flex align-items-center">

                            {boom_r ?

                                <div className="dropdown d-flex">
                                    {
                                        boom_r ? (<a className="nav-link icon btn btn-default btn-icon ml-1" data-toggle="dropdown" ><i className="fa fa-bomb brandColor" style={{ fontSize: '25px' }} onClick={this.boomForm}></i></a>) : ''
                                    }

                                    <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">


                                        <div className="col-sm-12">
                                            <form className='form-horizontal' onSubmit={this.BoomPostBtn}>

                                                <h6 className='mt-1 mt-2'>Boom <i className="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="Use this to celebrate every time you make a sale! Simply enter the sale amount and click BOOM!"></i></h6>
                                                <div className="form-group">

                                                    {
                                                        boom_w ?
                                                            (
                                                                <input type="text" ref={elem => (this.textInput = elem)} autofocus="true" name="boom" className="form-control" id="" aria-describedby="emailHelp" onChange={this.handleBoomChange} placeholder="Amount ($000.00)" onKeyPress={this.checkValidAmount} required />
                                                            )
                                                            :
                                                            (
                                                                <input type="text" ref={elem => (this.textInput = elem)} name="boom" readonly="readonly" className="form-control" id="" aria-describedby="emailHelp" placeholder="Amount ($000.00)" />
                                                            )
                                                    }

                                                </div>


                                                {
                                                    boom_w ?
                                                        (
                                                            <div class="form-check cm-check">
                                                                <input type="checkbox" class="form-check-input" id="confirm_amount" name='confirm_amount' onChange={this.handleConfirmAmountCheck} required />
                                                                <label class="form-check-label" for="confirm_amount" >I verify this transaction is legitimate.</label>
                                                            </div>

                                                        ) : ''
                                                }


                                                {
                                                    boom_w ?
                                                        (
                                                            <div className="form-group text-center">
                                                                <button type="submit" className="btn btn-brand filled btn-block">Boom!</button>
                                                            </div>
                                                        ) : ''
                                                }

                                            </form>
                                        </div>

                                    </div>
                                </div>

                                : ''
                            }

                            {/* <div className="dropdown d-flex">
                                <a className="nav-link icon d-none d-md-flex btn btn-default btn-icon ml-1" data-toggle="dropdown"><i className="fa fa-heart text-danger" style={{ fontSize: '25px' }} ></i></a>
                                <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <ul className="list-unstyled feeds_widget">
                                        {referAFriend_r ?
                                            <li className="">
                                                <a href="javascript:void(0);" className="media">
                                                    <div className="media-body">
                                                        <span className="name">Refer A friend</span>
                                                    </div>
                                                </a>
                                            </li> : ''
                                        }

                                        <div className="dropdown-divider"></div>

                                        {referAFriend_r ?
                                            <li className="">
                                                <a href="javascript:void(0);" className="media">
                                                    <div className="media-body">
                                                        <span className="name">Leav A Review</span>
                                                    </div>
                                                </a>
                                            </li> : ''
                                        }



                                    </ul>
                                </div>
                            </div> */}

                            <div className="dropdown d-flex">
                                {
                                    love_r || referAFriend_r ? (<a className="nav-link icon btn btn-default btn-icon ml-1" data-toggle="dropdown"><i className="fa fa-heart text-danger" style={{ fontSize: '25px' }} ></i></a>) : ''
                                }

                                <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    {referAFriend_r ?

                                        <NavLink to="/refer-a-friend" className="dropdown-item" href="javascript:void(0);">Refer A Friend</NavLink>
                                        : ''
                                    }

                                    { referAFriend_r && love_r ?
                                        (<div className="dropdown-divider"></div>) : ''
                                    }

                                    {love_r ?
                                    (
                                        <Fragment>
                                        
                                        <NavLink to="/review" className="dropdown-item" href="javascript:void(0);">Leave A Review</NavLink>
                                        </Fragment>)
                                        : ''
                                    }

                                </div>
                            </div>

                            <div className="dropdown d-flex">
                                {this.state.notificationbadge.notification_counter > 0 ? (<span className="notification-counter">{this.state.notificationbadge.notification_counter}</span>) : ''}
                                <a href="javascript:void(0)" className="chip ml-3" data-toggle="dropdown">
                                    {/* <span className="avatar" style={{ backgroundImage: 'url(/assets/images/placeholder-2.png)' }}></span>{this.state.full_name}</a> */}
                                    <span className="avatar" style={{ backgroundImage: `url(${this.state.member.profile_image ? this.state.baseApiURL + this.state.member.profile_image : '/assets/images/placeholder-2.png'}` }}></span>{this.state.full_name}</a>
                                <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">

                                    <NavLink className="dropdown-item" to='/profile'><i className="dropdown-icon fe fe-user"></i> Profile</NavLink>
                                    <NavLink className="dropdown-item" to='/password/change'><i className="dropdown-icon fe fe-user"></i> Password</NavLink>


                                    {notification_r ? <a className="dropdown-item" href="/notifications">
                                        <i className="dropdown-icon fa fa-bell"></i> Notifications ({this.state.notificationbadge.notification_counter})
                                </a> : ''}

                                    <div className="dropdown-divider"></div>



                                    <a className="dropdown-item" onClick={this.handleLogout}><i className="dropdown-icon fe fe-log-out"></i> Log Out</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
                <div className="row">
                    <div className="col-md-12 notification-badge">

                    { 
                    
                        
                        this.state.notificationbadge.notification_counter > 0 && notification_r && this.state.notificationbadge.notification.notification_title ? 
                            
                            (
                                <div class="alert alert-warning alert-dismissible fade show text-center border" role="alert">
                                    <strong  ><a href={"/notifications/"+this.state.notificationbadge.notification.notification_id}  >{this.state.notificationbadge.notification.notification_title}</a></strong> 
                                    
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">     
                                        {/* <i className='fa fa-times'></i>                        */}
                                    </button>
                                </div>
                        ) : ''
                        
                        
                        
                        
                        
                        }
                    </div>
                </div>
            </Fragment>

        )
    }
}

export default Header
