import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import { NavLink } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid'
import '@fullcalendar/core/main.css'
import interactionPlugin from "@fullcalendar/interaction";
import timeGridPlugin from "@fullcalendar/timegrid";
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import { Pie } from 'react-chartjs-2';
const $ = window.$

export class DashBoard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            baseApiURL: '',
            showLoader: false,
            showAlertIcon: '',
            showAlert: false,
            showConfirmationAlert: false,
            showLoader: false,
            categories: [],
            reviews: [],
            booms: [],
            refers: [],
            programs: [],
            total_boom: '0.00',
            total_income: '0.00',
            total_boom_amount: '0.00',
            total_referral_amount: '0.00',
            member_permission: {},
            member_permission: { r: '1' },
            memberPermission: [],
            counters: [],
            counter_data: {},
            history:{},
            history_list:[],
            events:[],
            calenderevents:[],
            currentDate: new Date().toISOString().slice(0,10),

        }
    }


    componentDidMount() {

        
        //get user detail from memeber id
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        if (memberSession.user_id) {
            this.setState({
                user_id: memberSession.user_id,
                email: memberSession.email,
                full_name: memberSession.full_name,
                auth_token: memberSession.auth_token,
                memberPermission: memberSession.permissions,
                role_id: memberSession.role_id
            })

            //get review List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/permission/validate/55640434", //user dashboard
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        member_permission 

                        if (response.data.member_permission.r == '0') {
                            window.location.href = '/profile'
                        }


                        this.setState({ member_permission: response.data.member_permission });
                    }
                    else {
                        //fail
                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


            //get review List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/review/list",
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ reviews: response.data.reviews });
                        this.setState({ member: response.data.member });
                    }
                    else {
                        //fail
                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


            //get boom List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/boom/list",
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ booms: response.data.booms });
                        this.setState({
                            total_income: response.data.total_income,
                            total_boom_amount: response.data.total_boom_amount,
                            total_referral_amount: response.data.total_referral_amount
                        });

                    }
                    else {
                        //fail
                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


            //get refer List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/refer/list",
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ refers: response.data.refers });
                    }
                    else {
                        //fail
                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });



            //get program List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/list",
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ programs: response.data.member_programs });
                    }
                    else {
                        //fail
                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


            //get Member program List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/list/menu/" + memberSession.role_id,
                method: 'get',
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ image_base_url: response.data.api_url });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ programs: response.data.programs });
                    }
                })
                .catch(err => {
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });

                //counter
                this.getProgramCounter();
                this.getProgramHisotyList();
                this.getCalenderEvents();
        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }
    }
    handleDateClick = (arg) => { // bind with an arrow function
        //alert(arg.dateStr)
        const selected_date = arg.dateStr;
        if(selected_date)
        {
            const selecteddate = new Date(selected_date).toISOString().slice(0,10);
            window.location.href = '/event/list/'+selecteddate;
        }
        
    }

    handleDateEventClick = (calEvent, jsEvent, view, resourceObj, info) => { // bind with an arrow function
        //alert(arg.dateStr)
        //alert(calEvent.event.title)
        const selected_event_id = calEvent.event.id;
        if(selected_event_id)
        {
            window.location.href = '/event/details/'+selected_event_id;
        }
        
    }

    


    getCalenderEvents = () => {
        //get review List
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "member/event/calender/list",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message; //
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ events: response.data.events});
                this.setState({ calenderevents: response.data.calenderevents});
            }
            else {
                //fail
                //this.setState({ showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
            }
        })
        .catch(err => {
            this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
        });

    }


    getProgramHisotyList = () => {
        //get review List
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "member/history/list/",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message; //
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ history_list: response.data.history_list});

            }
            else {
                //fail
                //this.setState({ showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
            }
        })
        .catch(err => {
            this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
        });

    }

    
    getProgramHisoty = (program_id) => {
        //get review List
        program_id = program_id ? program_id : ''
        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "member/history/details/"+program_id,
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message; //
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ history: response.data.history});
                    if (response.data.history.url) {
                        window.location.href = response.data.history.url; // '/dashboard';
                    }

                }
                else {
                    //fail
                    //this.setState({ showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
                }
            })
            .catch(err => {
                this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
            });

    }

    checkProgramHisoty = (e) => {
        //get review List
        const program_id = e.currentTarget.dataset.id;
        this.getProgramHisoty(program_id);
        
        

    }


    getProgramCounter = (str) => {
        //get review List
        str = str ? str : 0;

        const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
        this.setState({ showLoader: true });



        axios({
            url: API_URL + "member/dashboard/program/counter",
            method: 'get',
            headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message; //
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ counters: response.data.programs, counter_data: response.data });

                }
                else {
                    //fail
                    this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
                }
            })
            .catch(err => {
                this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
            });

    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if(this.state.showAlertActionURL == 'refresh') 
        {
            window.location.reload();  
        } 
        else if(this.state.showAlertActionURL) 
        {
            window.location.href = this.state.showAlertActionURL;  
        }
    }


    render() {
        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        //Current Date
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1; //January is 0!
        let yyyy = today.getFullYear();

        if (dd < 10) { dd = '0' + dd }
        if (mm < 10) { mm = '0' + mm }

        let current = yyyy + '-' + mm + '-';

        console.log(current)

        const getPermissionOject = (value) => {
            const permissiondata = this.state.memberPermission.filter(function (e) {
                return e.permission_id == value;
            });

            if (permissiondata) {
                return permissiondata[0];
            }
        }



        //boom - 38080107
        const boomdata = getPermissionOject('38080107');
        var boom_r = 0;
        var boom_w = 0;
        var boom_u = 0;
        if (boomdata) {
            boom_r = boomdata.r;
            boom_w = boomdata.w;
            boom_u = boomdata.u;

        }

        //Love - 22334025
        const lovedata = getPermissionOject('22334025');
        var love_r = 0;
        var love_w = 0;
        var love_u = 0;
        if (lovedata) {
            love_r = lovedata.r;
            love_w = lovedata.w;
            love_u = lovedata.u;
        }

        //Refer A Friend -35982552
        const referAFriendData = getPermissionOject('35982552');
        var referAFriend_r = 0;
        var referAFriend_w = 0;
        var referAFriend_u = 0;
        if (referAFriendData) {
            referAFriend_r = referAFriendData.r;
            referAFriend_w = referAFriendData.w;
            referAFriend_u = referAFriendData.u;
        }

        //Notification - 70459612
        const notificationData = getPermissionOject('70459612');
        var notification_r = 0;
        var notification_w = 0;
        var notification_u = 0;
        if (referAFriendData) {
            notification_r = notificationData.r;
            notification_w = notificationData.w;
            notification_u = notificationData.u;
        }

        //User Dashboard // 55640434
        // const userDashboardData = getPermissionOject('55640434');
        var userdashboard_r = 1;
        var userdashboard_w = 1;
        var userdashboard_u = 1;
        // if (referAFriendData) {
        //     userdashboard_r = userDashboardData.r;
        //     userdashboard_w = userDashboardData.w;
        //     userdashboard_u = userDashboardData.u;
        // }


        //User Advanced onboarding // 27534076 
        const advancedOnboardingData = getPermissionOject('27534076');
        var advancedOnboarding_r = 0;
        var advancedOnboarding_w = 0;
        var advancedOnboarding_u = 0;
        if (advancedOnboardingData) {
            advancedOnboarding_r = advancedOnboardingData.r;
            advancedOnboarding_w = advancedOnboardingData.w;
            advancedOnboarding_u = advancedOnboardingData.u;
        }


        //Program // 17345795
        const programData = "";
        //const programData = getPermissionOject('17345795');
        var program_r = 1;
        var program_w = 1;
        var program_u = 1;
        if (programData) {
            program_r = programData.r;
            program_w = programData.w;
            program_u = programData.u;
        }

        //Link // 80555725
        const linkData = getPermissionOject('80555725');
        var link_r = 0;
        var link_w = 0;
        var link_u = 0;
        if (referAFriendData) {
            link_r = linkData.r;
            link_w = linkData.w;
            link_u = linkData.u;
        }

        //Support // 56519713
        const supportData = getPermissionOject('56519713');
        var support_r = 0;
        var support_w = 0;
        var support_u = 0;
        if (referAFriendData) {
            support_r = supportData.r;
            support_w = supportData.w;
            support_u = supportData.u;
        }

        //FAQ // 68389135
        const faqData = getPermissionOject('68389135');
        var faq_r = 0;
        var faq_w = 0;
        var faq_u = 0;
        if (referAFriendData) {
            faq_r = faqData.r;
            faq_w = faqData.w;
            faq_u = faqData.u;
        }

        //Email Support // 32508229
        const emailSupportData = getPermissionOject('32508229');
        var emailSupport_r = 0;
        var emailSupport_w = 0;
        var emailSupport_u = 0;
        if (referAFriendData) {
            emailSupport_r = emailSupportData.r;
            emailSupport_w = emailSupportData.w;
            emailSupport_u = emailSupportData.u;
        }

        const data = {
            labels: [
                'Boom Sales',
                'Refer a Friend',
                // 'Others'
            ],
            datasets: [{
                data: [this.state.counter_data.total_boom_amount_chart, this.state.counter_data.total_referral_amount_chart],
                backgroundColor: [
                    '#e8be28',
                    '#000000',
                    // '#FFCE56'
                ],
                hoverBackgroundColor: [
                    '#e8be28',
                    '#000000',
                    // '#FFCE56'
                ]
            }]
        };

        return (
            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Dashboard</h1>
                         
                    </div>
                </div>
                <br />

                <div className="program-progress">
                    <div className="row">
                        <div className="col-sm-8">
                            <h5 className='text-center mb-3'>Program Progress</h5>
                            
                            <div class="accordion dashboardCounterDiv" id="accordionExample" style={{ display: program_r == '1' ? 'block' : 'none' }}>
                            <div class="row clearfix row-deck" >
                                

                                {
                                    this.state.counters.map((pg, pgindex) => (
                                    <div class="col-6 col-md-6 col-xl-6">
                                        <div class="card">
                                            <div className="row">
                                                <div class="col-md-9" style={{display:'flex'}}>

                                                {/* <div class="col-md-9"> */}
                                                    <div class="card-body top_counter">
                                                        <a href={`/program/details/${pg.program_id}`} className="my_sort_cut text-muted" title="View Program">
                                                            <div class="icon">
                                                                
                                                                {pg.program_icon ?                                                           
                                                                (<span className='program-icon'><img src={this.state.image_base_url + pg.program_icon} data-toggle="tooltip" data-placement="right" title={pg.program_title} /></span>) : (<div class="icon" style={{backgroundColor: pg.program_color ? pg.program_color : '#e8be28'}}><i class="fa fa-book"></i> </div>)
                                                                }
                                                            </div>
                                                        </a>
                                                        <div class="content">
                                                            <a href={`/program/details/${pg.program_id}`} className="my_sort_cut text-muted" title="View Program">
                                                                    
                                                                
                                                                <h5>{pg.program_title}</h5>
                                                                
                                                            </a>
                                                            {/* <span>Total Student</span>
                                                            <h5 class="number mb-0">2,051</h5> */}
                                                        </div>


                                                        
                                                        
                                                        
                                                    </div>

                                                    
                                                    
                                                    {/* <div class="card-body top_counter" style={{borderTop:'none'}}>
                                                         
                                                            <div class="icon">
                                                                
                                                            </div>
                                                         
                                                        <div class="content">
                                                            <a href={`/program/details/${pg.program_id}`} className="my_sort_cut text-muted" title="View Program">
                                                                    
                                                                <span class="tag tag-warning" style={{backgroundColor: pg.program_color ? pg.program_color : '#e8be28'}}>Details</span>
                                                                
                                                            </a>
                                                             
                                                        </div>
                                                        
                                                        
                                                    </div> */}

                                                     
                                                        
                                                </div>
                                                <div className="col-md-3">
                                                    <div class="icon"  style={{float:'right', padding:'15px'}}>
                                                        {/* <i class="fa fa-users"></i>  */}
                                                        <CircularProgressbar
                                                            value={pg.completeProgramItemsPercent}
                                                            text={pg.completeProgramItemsPercent+'%'}
                                                            strokeWidth={3}
                                                            styles={buildStyles({
                                                                        
                                                                textColor: pg.program_color ? pg.program_color : '#e8be28',
                                                                pathColor: pg.program_color ? pg.program_color : '#e8be28',
                                                                    
                                                                })}
                                                        />
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div className="row" style={{marginTop: '-21px'}}>
                                                <div className="col-md-12">
                                                    {/* history  history_url */}
                                                        <p>
                                                    {/* {(pg.program_continue == '1' && pg.completeProgramItemsPercent != '100') ?
                                                    (<a href="javascript:void(0);" data-id={pg.program_id} onClick={this.checkProgramHisoty}  className="text-info brandColor" style={{marginLeft: '10px'}} title="View Detail">
                                                        Continue >>
                                                    </a>) : ''
                                                    } */}
                                                    {(pg.program_continue == '1' && pg.completeProgramItemsPercent != '100') ?
                                                    (<a href={pg.program_history_url} data-id={pg.program_id}  className="text-info brandColor" style={{marginLeft: '10px'}} title="View Detail">
                                                        Continue >>
                                                    </a>) : ''
                                                    }
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    ))
                                }
                                
                            </div>
                            </div>

                        </div>
                        <div className="col-sm-4">
                            <h5 className='text-center mb-3'>Earnings</h5>
                            <div className="card">

                                <div className='dashboardChart'>

                                    <Pie data={data} />

                                    <div className='earning '>
                                        <div className='text-center earning-amount'>${this.state.counter_data.total_income}</div>
                                        <div className='earning-list-wrapper '>
                                            <div className='earning-list'>
                                                <div>Boom Sales ({this.state.counter_data.boom_count})</div>
                                                {/* <div></div> */}
                                                <div>${this.state.counter_data.total_boom_amount}</div>
                                            </div>
                                            <div className='earning-list'>
                                                <div>Refer a Friend ({this.state.counter_data.referral_friend_count})</div>
                                                {/* <div></div> */}
                                                <div>${this.state.counter_data.total_referral_amount} </div>
                                            </div>
                                            {/* <div className='earning-list'>
                                            <div>Other</div>
                                            <div>22</div>
                                            <div>75%</div>
                                        </div> */}
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div className="row" style={{ display: this.state.member_permission.r == '1' ? 'block' : 'none' }}>
                    
                    <div className="col-md-12"  >

                    

                        {/* <div style={{ display: program_r == '1' ? 'block' : 'none' }}>
                            <div class="row clearfix row-deck" >
                                {
                                    this.state.programs.map((program, bindex) => (

                                        <div class="col-6 col-md-6 col-xl-6">
                                            <div class="card">

                                            <div className="row">
                                                <div className="col-md-8">

                                                    <div class="card-body ribbon">
                                                    <div class="icon">
                                                        <div class="icon bg-yellow"><i class="fa fa-users"></i> </div>                                                        
                                                    </div>
                                                        
                                                        <a href={`/program/details/${program.program_id}`} className="my_sort_cut text-muted" title="View Program">
                                                            
                                                            <i class="fa fa-book"></i>
                                                            <span>{program.program_title}</span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    
                                                    <div class="card-body ribbon"  >
                                                        
                                                        <CircularProgressbar
                                                            value='70'
                                                            text='70%'
                                                            strokeWidth={5}
                                                            styles={buildStyles({
                                                                        
                                                                textColor: '#e8be28',
                                                                pathColor: '#e8be28',
                                                                    
                                                                })}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div> */}

                        {/* <div class="row clearfix row-deck">
                            

                            <div class="col-6 col-md-4 col-xl-6" style={{display:program_r == '1' ? 'block' : 'none'}}>
                                <div class="card">
                                    <div class="card-body ribbon">
                                        <a href="/programs" class="my_sort_cut text-muted">
                                            <i class="fa fa-tasks"></i>
                                            <span>Total Programs</span>
                                        </a>
                                        <div class="py-3 m-0 text-center h5">{this.state.programs.length}</div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-6 col-md-4 col-xl-6">
                                <div class="card">
                                    <div class="card-body ribbon">

                                        <a href="#" class="my_sort_cut text-muted">
                                            <i class="fa fa-dollar"></i>
                                            <span>Your Earnings</span>
                                        </a>
                                        <div class="py-3 m-0 text-center h5 text-success">${this.state.total_income}</div>
                                    </div>
                                </div>
                            </div>

                        </div> */}
                        <div className="row">
                            {/* <div class="col-4 col-md-4 col-xl-4">
                                <div class="card">
                                    <div class="card-body ribbon">

                                        <a href="#" class="my_sort_cut text-muted">
                                            <i class="fa fa-dollar"></i>
                                            <span>Your Earnings</span>
                                        </a>
                                        <div class="py-3 m-0 text-center h5">${this.state.total_income}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4 col-md-4 col-xl-4">
                                <div class="card">
                                    <div class="card-body ribbon">

                                        <a href="#" class="my_sort_cut text-muted">
                                            <i class="fa fa-user-circle-o"></i>
                                            <span>Refer A Friend</span>
                                        </a>
                                        <div class="py-3 m-0 text-center h5">${this.state.total_referral_amount}</div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-4 col-md-4 col-xl-4">
                                <div class="card">
                                    <div class="card-body ribbon">
                                        <a href="#" class="my_sort_cut text-muted">
                                            <i class="fa fa-address-book"></i>
                                            <span>Boom Sales</span>
                                        </a>
                                        <div class="py-3 m-0 text-center h5">${this.state.total_boom_amount}</div>
                                    </div>
                                </div>
                            </div> */}

                            <div className="col-md-12">
                                <div className="card-footer text-right">                    
                                    <a href={"/event/add/"+this.state.currentDate} className="btn btn-brand">Add Event</a>
                                </div>
                                <div className="card">
                                    <div className="card-body">
                                        

                                        <FullCalendar
                                       
                                            header={{
                                                left: 'title',
                                                center: '',
                                                right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek, prev,next Today'
                                            }}
                                            dateClick={this.handleDateClick}
                                            defaultView="timeGridWeek"
                                            default={true}
                                            plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                                            // weekends={false}
                                            //events={[
                                            //     { title: 'event 1', date: `${current}10`, color:'#7ed321' },
                                            //     { title: 'event 2', date: `${current}25` }
                                            // ]}
                                           events={this.state.calenderevents}
                                        //    events={[
                                        //        {
                                        //         color: "#F1CA29",
                                        //         daysOfWeek: [3],
                                        //         endTime: "22:06",
                                        //         id: "gwdnG1BQRlKqv8W6vwmmKX8LYu5xxDhHaE8RJ",
                                        //         startTime: "14:59",
                                        //         title: "Event 101 Recurring (June 17, 2020, 2:59 pm - 3:14 pm)",
                                        //        }
                                        //     // {
                                        //     //   groupId: 'blueEvents', // recurrent events in this group move together
                                        //     //   daysOfWeek: [ '1' ],
                                        //     //   startTime: '10:45:00',
                                        //     //   endTime: '12:45:00'
                                        //     // },
                                        //     // {
                                        //     //   daysOfWeek: [ '3' ], // these recurrent events move separately
                                        //     //   startTime: '11:00:00',
                                        //     //   endTime: '11:30:00',
                                        //     //   color: 'red'
                                        //     // },
                                        //     // {
                                        //     //     daysOfMonth: [ '1' ], // these recurrent events move separately
                                        //     //     startTime: '15:00:00',
                                        //     //     endTime: '16:30:00',
                                        //     //     color: 'red'
                                        //     //   }
                                        //   ]}
                                          
                                            navLinks= {true} // can click day/week names to navigate views
                                            editable= {true}
                                            eventLimit= {true} // allow "more" link when too many events
                                    
                                            eventClick={this.handleDateEventClick}
                                            //eventClick = {function(calEvent, jsEvent, view, resourceObj) {alert(calEvent.title)}}
                                            //eventColor='#7ed321'
                                             
                                           
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row clearfix row-deck">
                            {/* Review */}
                            {/* <div class="col-xl-12 col-lg-12 col-md-12">
                                <div className="card">
                                    <div className="row ">
                                        <div className="col">
                                            <div className="card-header pb-0">
                                                <h3 className="card-title">Reviews</h3>
                                            </div>
                                        </div>                                         
                                    </div>
                                    <div className="card-body">
                                        <div className="table-responsive">
                                                        
                                            <table id='categoriestbl' className="table table-fixed table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                                <thead>
                                                    <tr>
                                                        <th>Sno.</th>
                                                        <th>Programs</th>
                                                        <th>Description</th>
                                                        <th class='text-center'>Rating</th>
                                                        <th class='text-center'>Date</th>
                                                    </tr>
                                                </thead>                                        
                                                <tbody>                                                
                                                    {
                                                        this.state.reviews.map((review, rindex) => (
                                                            <tr key={rindex}>
                                                                <td>{rindex + 1}</td>
                                                                <td className="text-capitalize">
                                                                    {review.program_title}
                                                                </td>
                                                                <td className="text-capitalize">
                                                                <div dangerouslySetInnerHTML={{ __html: review.description }} />
                                                                </td>                                                                
                                                                <td class='text-center brandColor'>
                                                                    <span className={review.rating == '1.00' ? '' : 'hidden'}>
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;                                                                                         
                                                                    </span>

                                                                    <span className={review.rating == '2.00' ? '' : 'hidden'}>
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    </span>

                                                                    <span className={review.rating == '3.00' ? '' : 'hidden'}>
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    </span>

                                                                    <span className={review.rating == '4.00' ? '' : 'hidden'}>
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <br/>
                                                                        
                                                                    </span>

                                                                    <span className={review.rating == '5.00' ? '' : 'hidden'}>
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    </span>
                                                                </td>  
                                                                <td class='text-center'>
                                                                    {(new Date(review.created_date)).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                                </td>                                                                
                                                            </tr>
                                                        ))
                                                    }
                                                </tbody>
                                                {this.state.reviews.length == 0 && (<tfoot><tr><td colspan="4"  className="text-center text-capitalize">{this.state.showCategoriesTableMessage}</td></tr></tfoot>)}
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>                              */}
                        </div>



                        <div class="row clearfix row-deck">
                            {/* sales */}
                            {/* <div class="col-xl-6 col-lg-6 col-md-12">
                                <div className="card">
                                    <div class="card-header">
                                            <h3 class="card-title">Sales</h3>
                                    </div>
                                    <div className="card-body">
                                    <div className="table-responsive">                                                    
                                        <table id='categoriestbl' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                            <thead>
                                                <tr>
                                                    <th>Sno.</th>
                                                    <th>Amount($)</th>
                                                    <th class='text-center'>Date</th>                                                     
                                                </tr>
                                            </thead>                                        
                                            <tbody>
                                                {
                                                    this.state.booms.map((boom, bindex) => (
                                                        <tr>
                                                            <td>{bindex + 1}</td>
                                                            <td className="text-capitalize">
                                                                ${boom.amount}
                                                            </td>

                                                            <td class='text-center'>
                                                                {(new Date(boom.created_date)).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                            </td>                                                              
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                            {this.state.booms.length == 0 && (<tfoot><tr><td colspan="4"  className="text-center text-capitalize">{this.state.showCategoriesTableMessage}</td></tr></tfoot>)}
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div> */}

                            {/* refer */}

                            {/* <div class="col-xl-6 col-lg-6 col-md-12">
                                <div className="card">
                                    <div class="card-header">
                                            <h3 class="card-title">Refer Friends</h3>
                                    </div>
                                    <div className="card-body">
                                    <div className="table-responsive">                                                    
                                        <table id='categoriestbl' className="table table-fixed table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                            <thead>
                                                <tr>
                                                    <th>Sno.</th>
                                                    <th className="text-left">Email</th>
                                                    <th class='text-center'>Staus</th>
                                                    <th class='text-center'>Date</th>                                                     
                                                </tr>
                                            </thead>                                        
                                            <tbody>
                                                {
                                                    this.state.refers.map((refer, refindex) => (
                                                        <tr>
                                                            <td>{refindex + 1}</td>
                                                            <td className="text-left">
                                                                {refer.refer_email}
                                                            </td>
                                                            <td class='text-center'>
                                                                {refer.refer_status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Pending</span>)}
                                                            </td>
                                                            <td class='text-center'>
                                                                {(new Date(refer.created_date)).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                            </td>                                                              
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                            {this.state.refers.length == 0 && (<tfoot><tr><td colspan="4"  className="text-center text-capitalize">{this.state.showCategoriesTableMessage}</td></tr></tfoot>)}
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div> */}

                        </div>
                    </div>
                </div>
            </Fragment>

        )
    }
}


export default DashBoard
