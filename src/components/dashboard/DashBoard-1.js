import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import { NavLink } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';


export class DashBoard extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            baseApiURL: '',
            showLoader: false,            
            showAlertIcon: '',
            showAlert: false,
            showConfirmationAlert: false,
            showLoader:false,
            categories:[],
            reviews:[],
            booms:[],
            refers:[],
            programs:[],
            total_boom:'0.00'
        }
    }


    componentDidMount() {
          //get user detail from memeber id
          const memberSession = JSON.parse(window.localStorage.getItem('memberSession'));
          if (memberSession.user_id) 
          {
              this.setState({
                  user_id: memberSession.user_id,
                  email: memberSession.email,
                  full_name: memberSession.full_name,
                  auth_token: memberSession.auth_token,
              })
            //get review List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/review/list",
                method: 'get',    
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }        
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;                       
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ reviews: response.data.reviews }); 
                    this.setState({ member: response.data.member });               
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });


            //get boom List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/boom/list",
                method: 'get',    
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }        
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;                       
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ booms: response.data.booms });  
                    this.setState({ total_boom: response.data.total_boom });  
                                                    
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });


            //get refer List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/refer/list",
                method: 'get',    
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }        
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;                       
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ refers: response.data.refers });                                                                           
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });



            //get program List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "member/program/list",
                method: 'get',    
                headers: { 'user_id': memberSession.user_id, 'auth_token': memberSession.auth_token }        
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;                       
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ programs: response.data.member_programs });                
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });
        }
        else {
            window.localStorage.removeItem('memberSession');
            window.location.href = '/auth/signin'
        }
    }


    render() { 
        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };   
        return (
            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                   type={this.state.showAlertIcon}                    
                   title={this.state.showAlertMessage}
                   onConfirm={this.sweetalertok}
                   onCancel={() => this.setState({ showAlert: false })}
                   show={this.state.showAlert}
                />
                <div className="row">                     
                    <div className="col-md-12">
                        <div class="row clearfix row-deck">
                            <div class="col-6 col-md-4 col-xl-3">
                                <div class="card">
                                    <div class="card-body ribbon">
                                        <div class="ribbon-box green" data-toggle="tooltip" title="New Professors">{this.state.programs.length}</div>
                                        <a href="#" class="my_sort_cut text-muted">
                                            <i class="fa fa-book"></i>
                                            <span>Programs</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-xl-3">
                                <div class="card">
                                    <div class="card-body ribbon">
                                    <div class="ribbon-box orange" data-toggle="tooltip" title="New Professors">{this.state.reviews.length}</div>
                                        <a href="#" class="my_sort_cut text-muted">
                                            <i class="fa fa-address-book"></i>
                                            <span>Review</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-xl-3">
                                <div class="card">
                                    <div class="card-body ribbon">
                                        <div class="ribbon-box green" data-toggle="tooltip" title="New Staff">{this.state.refers.length}</div>
                                        <a href="staff.html" class="my_sort_cut text-muted">
                                            <i class="fa fa-user-circle-o"></i>
                                            <span>Refer</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                              
                            <div class="col-6 col-md-4 col-xl-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div>Total Revenue</div>
                                            <div class="py-3 m-0 text-center h1 text-success">${this.state.total_boom}</div>
                                        <div class="d-flex">
                                            <span class="text-muted">Income</span>
                                            {/* <div class="ml-auto"><i class="fa fa-caret-up text-success"></i>4%</div> */}
                                        </div>
                                    </div>                                     
                                </div>
                            </div>
                        </div>

                        
                        <div class="row clearfix row-deck">
                            {/* Review */}
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div className="card">
                                    <div className="row ">
                                        <div className="col">
                                            <div className="card-header pb-0">
                                                <h3 className="card-title">Reviews</h3>
                                            </div>
                                        </div>                                         
                                    </div>
                                    <div className="card-body">
                                        <div className="table-responsive">
                                                        
                                            <table id='categoriestbl' className="table table-fixed table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                                <thead>
                                                    <tr>
                                                        <th>Sno.</th>
                                                        <th>Programs</th>
                                                        <th>Description</th>
                                                        <th class='text-center'>Rating</th>
                                                        <th class='text-center'>Date</th>
                                                    </tr>
                                                </thead>                                        
                                                <tbody>                                                
                                                    {
                                                        this.state.reviews.map((review, rindex) => (
                                                            <tr key={rindex}>
                                                                <td>{rindex + 1}</td>
                                                                <td className="text-capitalize">
                                                                    {review.program_title}
                                                                </td>
                                                                <td className="text-capitalize">
                                                                <div dangerouslySetInnerHTML={{ __html: review.description }} />
                                                                </td>                                                                
                                                                <td class='text-center brandColor'>
                                                                    <span className={review.rating == '1.00' ? '' : 'hidden'}>
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;                                                                                         
                                                                    </span>

                                                                    <span className={review.rating == '2.00' ? '' : 'hidden'}>
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    </span>

                                                                    <span className={review.rating == '3.00' ? '' : 'hidden'}>
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    </span>

                                                                    <span className={review.rating == '4.00' ? '' : 'hidden'}>
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        <br/>
                                                                        
                                                                    </span>

                                                                    <span className={review.rating == '5.00' ? '' : 'hidden'}>
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                    </span>
                                                                </td>  
                                                                <td class='text-center'>
                                                                    {(new Date(review.created_date)).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                                </td>                                                                
                                                            </tr>
                                                        ))
                                                    }
                                                </tbody>
                                                {this.state.reviews.length == 0 && (<tfoot><tr><td colspan="4"  className="text-center text-capitalize">{this.state.showCategoriesTableMessage}</td></tr></tfoot>)}
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>                             
                        </div>


                       
                        <div class="row clearfix row-deck">
                             {/* sales */}
                            <div class="col-xl-6 col-lg-6 col-md-12">
                                <div className="card">
                                    <div class="card-header">
                                            <h3 class="card-title">Sales</h3>
                                    </div>
                                    <div className="card-body">
                                    <div className="table-responsive">                                                    
                                        <table id='categoriestbl' className="table table-fixed table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                            <thead>
                                                <tr>
                                                    <th>Sno.</th>
                                                    <th>Amount($)</th>
                                                    <th class='text-center'>Date</th>                                                     
                                                </tr>
                                            </thead>                                        
                                            <tbody>
                                                {
                                                    this.state.booms.map((boom, bindex) => (
                                                        <tr>
                                                            <td>{bindex + 1}</td>
                                                            <td className="text-capitalize">
                                                                ${boom.amount}
                                                            </td>

                                                            <td class='text-center'>
                                                                {(new Date(boom.created_date)).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                            </td>                                                              
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                            {this.state.booms.length == 0 && (<tfoot><tr><td colspan="4"  className="text-center text-capitalize">{this.state.showCategoriesTableMessage}</td></tr></tfoot>)}
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>

                            {/* refer */}

                            <div class="col-xl-6 col-lg-6 col-md-12">
                                <div className="card">
                                    <div class="card-header">
                                            <h3 class="card-title">Refer Friends</h3>
                                    </div>
                                    <div className="card-body">
                                    <div className="table-responsive">                                                    
                                        <table id='categoriestbl' className="table table-fixed table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                            <thead>
                                                <tr>
                                                    <th>Sno.</th>
                                                    <th className="text-left">Email</th>
                                                    <th class='text-center'>Staus</th>
                                                    <th class='text-center'>Date</th>                                                     
                                                </tr>
                                            </thead>                                        
                                            <tbody>
                                                {
                                                    this.state.refers.map((refer, refindex) => (
                                                        <tr>
                                                            <td>{refindex + 1}</td>
                                                            <td className="text-left">
                                                                {refer.refer_email}
                                                            </td>
                                                            <td class='text-center'>
                                                                {refer.refer_status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Pending</span>)}
                                                            </td>
                                                            <td class='text-center'>
                                                                {(new Date(refer.created_date)).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                            </td>                                                              
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                            {this.state.refers.length == 0 && (<tfoot><tr><td colspan="4"  className="text-center text-capitalize">{this.state.showCategoriesTableMessage}</td></tr></tfoot>)}
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </Fragment>

        )
    }
}


export default DashBoard
