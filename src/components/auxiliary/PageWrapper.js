import React from 'react';

const PageWrapper = (props) => {
    return (
        <div className={`page ${props.classes ? props.classes : ''}`}>
            {props.children}
        </div>
    );
}

export default PageWrapper;
